///This class represents all the constants methods and variables
import 'package:flutter/material.dart' show Color, Colors;
import 'package:flutter/material.dart';

class Constants {
  static const String ABOUT_EVAN =
      "An evangelist, author, teacher and lyricist with uncompromising faithfulness to the Holy Scriptures, Evans brings clarity and a message of necessity to Christians to uncover the unseen riches in God’s word." +
          "Born in a village called Mukerian, Punjab, India in 1988, Evans began his evangelistic career at a very young age of nineteen. In spite of many hurdles, challenges and obstacles Evans did not deviate from his work but continued to follow the path, which God gave him through vision and calling." +
          "\nOver the years, \nEvans has established churches, taught, preached the word of God and brought many souls to the kingdom of God. Evans has written and composed a number of Biblical songs and authored a few books, which are yet to be published and he provides daily devotion for a solid spiritual foundation to many people as well." +
          "\nEvans uncompromisingly stresses on correct interpretation, understanding and application of the Word of God as it should be applied by each individual being a New Testament believer.";
//  static const String BASE_URL = 'http://192.168.0.103:3000/v1/';
  static const String BASE_URL =
      'http://dds.christianappdevelopers.com:3000/v1/';
  static const String phpApi =
      'http://dds.christianappdevelopers.com/api/api.php/records/';
  static const String BLOG_API = BASE_URL + 'blogs/blogs-by-month/';
  static const String UPCOMING_EVENT_API = BASE_URL + 'events';
  static const String MUSIC_ALBUM_API = BASE_URL + 'music-albums';
  static const String ABOUT_API = BASE_URL + 'about_us.php';
  static const String REGISTRATION = BASE_URL + 'deviceId.php?deviceId=';
  static const String LETTER_OF_CEO = BASE_URL + 'letter_of_ceo.php';
  static const String MISSION = BASE_URL + 'missions';
  static const String VISION = BASE_URL + 'our_vision.php';
//  static const String STATEMENT_OF_FAITH = BASE_URL + 'statement_of_faith.php';
  static const String NO_DATA = 'No Data Found!';
  static const String MISSIONS_API = BASE_URL + 'mission.php';
  static const String GALLERY_LIST_API = BASE_URL + "gallery_images.php?id=";
  static const String MUSIC_LIST_API =
      BASE_URL + 'audio_album_track_list.php?id=';
  static const String VIDEO_API = BASE_URL + 'videos/videos/';
  static const String VIDEO_DATES_API = BASE_URL + '/videos/archive-dates/';
  static const String BLOGS_DATES_API = BASE_URL + 'blogs/archive-dates/';
  static const String TRACK_URL =
      "http://rbn1.christianappdevelopers.com/rapture/uploaded_files/track_list/";
  static const String GALLERY_ALBUM_API = BASE_URL + 'gallery-albums/';
  static const String FAITH_API = BASE_URL + 'faith.php';
  static const String INVITE_API = BASE_URL + 'invite';
  static const String PILLAR_CLOUD_API = BASE_URL + 'pillar-of-cloud';
  static const String PILLAR_FIRE_API = BASE_URL + 'pillar-of-fire';
  static const String MESSAGE_API = BASE_URL + 'messages';
  static const String PRAYER_API = BASE_URL + 'prayer';
  static const String NO_INTERNET_FOUND = 'No internet connection found';
  static const String SOMETHING_WENT = 'Something went wrong';
  static const String EMPTY_TEXT_ERROR = "This field can't be empty";
  static const String PLEASE_WAIT = 'Please Wait...';
  static const String MESSAGE_THUMBNAIL = BASE_THUMBNAIL + 'message/';
  static const String BASE_THUMBNAIL =
      'https://dds.christianappdevelopers.com/evans_laravel_admin/storage/app/public/';
  static const String VIDEO_THUMBNAIL =
      'http://rbn1.christianappdevelopers.com/rapture/uploaded_files/youtube_video/thumb/';
  static const String MUSIC_TRACK =
      'http://rbn1.christianappdevelopers.com/rapture/uploaded_files/track_list/';
  static const String BLOG_THUMBNAIL = BASE_THUMBNAIL;
  static const String UPCOMING_THUMBNAIL = BASE_THUMBNAIL + 'event/';
  static const String MUSIC_ALBUM_THUMBNAIL = BASE_THUMBNAIL + 'Audio/';
  static const String MUSIC_LIST_THUMBNAIL =
      'http://rbn1.christianappdevelopers.com/rapture/uploaded_files/track_list/thumb/';
  static const String MISSION_THUMBNAIL = BASE_THUMBNAIL + 'mission/';
  static const String FAITH_THUMBNAIL = BASE_THUMBNAIL + 'faith/';
  static const String YOUTUBE_APP_KEY =
      "AIzaSyD7ioDAOXF3XeU3MZIYYBMllaanH2WqQ4E";
  static const String GALLERY_ALBUM_THUMBNAIL = BASE_THUMBNAIL + 'Gallery/';
  static const String THANK_YOU_TEXT =
      "Thank you for taking the time to fill out the form. We deeply appericiate the invitation to co-labor with you in your area for a great spiritual breakthrough!\n\nWe will prayfully consider your request and respond as quickly as possible.\n\nThank you and God bless";
  static const String TESTIMONY_API =
      "http://webinnovativetechnology.com/bible/api/testimony.php";
  static const String TESTIMONY_Thumb =
      "http://rbn1.christianappdevelopers.com/rapture/uploaded_files/testimony/thumb/";
  static const String PDF_Api =
      "http://webinnovativetechnology.com/rapture/api/pdf_notes.php";

  static const String FIRE =
      """<p data-mce-style=\"text-align: justify;\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; text-align: justify;\">It\u2019s a very common term&nbsp;<strong>\u201CPartners\u201D<\/strong>&nbsp;and I always wanted something new, something prophetic. I asked God what you would like to call those who will support our small but significant ministry financially.<\/p><p data-mce-style=\"text-align: justify;\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; text-align: justify;\">And the Holy Spirit inspired \u201C<strong>Pillar of Fire<\/strong>\u201D.<\/p><p data-mce-style=\"text-align: justify;\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; text-align: justify;\">The pillar of fire by night guided the Israelites during their exodus from Egyptian bondage. This allowed them to travel by night.&nbsp;<strong>Exodus 13:21\u201322<\/strong>&nbsp;explains that God gave them the pillar of fire by night to give light and showed them the way to go.<\/p><p data-mce-style=\"text-align: justify;\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; text-align: justify;\">In ministry, two things are very essential, finance and prayers. I am not here to make my kingdom or here to make money. God told me to choose 40 people from every state who will support financially every month.<\/p><p data-mce-style=\"text-align: justify;\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; text-align: justify;\">I won\u2019t be choosing who will become a \u201C<strong>Pillar of Fire<\/strong>\u201D of our ministry, God will. My wife and I will prayerfully take this decision.<\/p><p data-mce-style=\"text-align: justify;\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; text-align: justify;\">The pillar was a testimony to other nations concerning God\u2019s involvement with and protection of His people Israel same way you will be a testimony to the people that you are involved with our ministry and standing with us in fulfilling the great commission God gave us.<\/p><p data-mce-style=\"text-align: justify;\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; text-align: justify;\"><strong>Exodus 13:22<\/strong>&nbsp;says, \u201CNeither the pillar of cloud by day nor the pillar of fire by night left its place in front of the people.\u201D The pillar is a picture of God\u2019s faithfulness to us that God never leaves nor forsakes His people the same way we are looking for people who will faithfully stand with our ministry which God gave us and become a strong pillar of fire.<\/p><p data-mce-style=\"text-align: justify;\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; text-align: justify;\">Kindly fill the form carefully and after praying we will get back to you.<\/p><p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px;\">In Christ,<\/p><p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px;\">Neha &amp; Evans Francis<\/p>""";
  static const String CLOUD =
      "<p data-mce-style=\"text-align: justify;\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; text-align: justify;\">In ministry it\u2019s a very common term\u00A0<strong>\u201Cintercessors\u201D<\/strong>\u00A0and I always wanted something new, something prophetic. I asked God what you would like to call those who will support our small but significant ministry with their prayers.<\/p><p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px;\">And the Holy Spirit inspired \u201C<strong>Pillar of Cloud<\/strong>\u201D.<\/p><p data-mce-style=\"text-align: justify;\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; text-align: justify;\">The pillar of cloud by day guided the Israelites during their exodus from Egyptian bondage. This allowed them to travel by day.\u00A0<strong>Exodus 13:21\u201322<\/strong>\u00A0explains that God gave them the pillar of cloud by day to lead them in the way He wanted them to go.\u00A0<\/p><p data-mce-style=\"text-align: justify;\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; text-align: justify;\">In ministry, two things are very essential, finance and prayers. I am not here to make my kingdom or here to make my kingdom, it\u2019s all about God\u2019s kingdom. God told me to choose 40 people from every state who will support our ministry with their prayers.<\/p><p data-mce-style=\"text-align: justify;\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; text-align: justify;\">I won\u2019t be choosing who will become a \u201C<strong>Pillar of Cloud<\/strong>\u201D of our ministry, God will. My wife and I will prayerfully take this decision.<\/p><p data-mce-style=\"text-align: justify;\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; text-align: justify;\">The pillar was a testimony to other nations concerning God\u2019s involvement with and protection of His people Israel same way you will be a testimony to the people that you are involved with our ministry and standing with us in fulfilling the great commission God gave us.<\/p><p data-mce-style=\"text-align: justify;\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; text-align: justify;\"><strong>Exodus 13:22<\/strong>\u00A0says, \u201CNeither the pillar of cloud by day nor the pillar of fire by night left its place in front of the people.\u201D The pillar is a picture of God\u2019s faithfulness to us that God never leaves nor forsakes His people, same way we are looking for people who will faithfully stand with our ministry which God gave us and become a strong pillar of cloud.<\/p><p data-mce-style=\"text-align: justify;\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; text-align: justify;\">Kindly fill the below form carefully and after praying we will get back to you.<\/p><p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px;\">In Christ,<\/p><p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px;\">Neha & Evans Francis<\/p>";

  static const String CLOUD_TITLE =
      "I would Like to become a Pillar of Cloud of Your Ministry!";

  static const String FIRE_TITLE =
      "I would Like to become a Pillar of Fire of Your Ministry!";

  static const String INVITE = "Invite Evans Francis";

  static const String Prayer_TITLE = "Send Prayer Request";
  static const String STATEMENT_OF_FAITH =
      "<p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px;\"><strong>THE AUTHORITY OF THE BIBLE<\/strong><\/p><p data-mce-style=\"text-align: justify;\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; text-align: justify;\">We believe the Bible is God-inspired, nd is to be the authoritative rule of life and practice for all Christians.We believe the Scriptures can be fulfilled and must not be broken. We believe that the Old Testament was the type and shadow of better things to come, and that the New Testament is the perfect expression of God\u2019s will and Character as expressed through Jesus Christ. The Epistles are the expression of what Christ has done for us and desires to do through us. (2 Tim. 3:16-17; Heb. 1:1-3; John 10:35; Matt. 5:19, 19:17; 1 John 2:3-4 )<\/p><p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px;\"><strong>THE ETERNAL GODHEAD<\/strong><\/p><p data-mce-style=\"text-align: justify;\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; text-align: justify;\">We believe God is Triune: Therefore Father, Son, and Holy Spirit are equally Divine and therefore equally deity.(1 John 5:7-8; Matt. 1:20-25; 2 Corinthians 13:14)<\/p><p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px;\"><strong>MAN & ORIGINAL PURPOSE<\/strong><\/p><p data-mce-style=\"text-align: justify;\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; text-align: justify;\">We believe man was created in the image of God and that his purpose was to have dominion over all the works of God\u2019s hands. (Gen. 1:26)<\/p><p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px;\"><strong>SPIRIT BEINGS: SATAN, DEMONS, AND ANGELS<\/strong><\/p><p data-mce-style=\"text-align: justify;\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; text-align: justify;\">We believe that there is a literal Devil (satan) and that he is the adversary of God and man and that there are spirit beings known as demons and angels. (1 Peter 5:8; Matt. 8:16; Mark 16:17; Heb. 1:7, 13-14)<\/p><p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px;\"><strong>FALL OF MAN<\/strong><\/p><p data-mce-style=\"text-align: justify;\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; text-align: justify;\">We believe that by voluntary disobedience man fell from perfection. (Romans 5:12)<\/p><p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px;\"><strong>SIN<\/strong><\/p><p data-mce-style=\"text-align: justify;\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; text-align: justify;\">We believe that whatsoever is not of faith is sin and that our sin has been forgiven and not imputed unto us. We believe that if we sin, we have an advocate with the Father, Jesus Christ, which will cleanse us from all unrighteousness. (Rom. 4:8; 6:14; 14:23; 1 John 1:9-2:1)<\/p><p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px;\"><strong>SALVATION<\/strong><\/p><p data-mce-style=\"text-align: justify;\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; text-align: justify;\">We believe that while we were yet sinners Christ died for us, signing the pardon for all who believe in Him, and that we have no righteousness (of our own) and must come to God pleading the righteousness of Christ as our only justification. (John 3:16; Rom. 5:8; Eph. 2:8)<\/p><p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px;\"><strong>SANCTIFICATION<\/strong><\/p><p data-mce-style=\"text-align: justify;\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; text-align: justify;\">We believe that Christ is our sanctification and we are therefore sanctified upon our receiving Him, and that we grow in grace as we grow in Christ therefore ever growing in holiness of life. (1 Co. 1:30; 1 Thess. 4:3; 2 Thess. 2:13; 1 Peter 1:2; 2 Peter 3:18)<\/p><p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px;\"><strong>HOLY SPIRIT EMPOWERMENT<\/strong><\/p><p data-mce-style=\"text-align: justify;\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; text-align: justify;\">We believe that the Holy Spirit empowers believers to live a life pleasing unto God, to do the works of Christ, and to meet the needs of all humanity. (John 14:12; 16:7-16; Acts 1:8)<\/p><p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px;\"><strong>HEALING IN ATONEMENT<\/strong><\/p><p data-mce-style=\"text-align: justify;\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; text-align: justify;\">We believe Divine Healing is the power of God to heal the sick (in mind or body), that healing for every person was included in Christ\u2019s atoning sacrifice, and that it is God\u2019s will for every Christian to minister healing to anyone at any time. (Psalm 103:2; Isaiah 53:4; Matt. 8:16-17; 1 Peter 2:24; John 14:12)<\/p><p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px;\"><strong>THE CHURCH \u2013 WHICH IS HIS BODY<\/strong><\/p><p data-mce-style=\"text-align: justify;\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; text-align: justify;\">We believe the church (on earth) is comprised of all in whom the Spirit of Christ dwells, and that the church is the body of Christ upon the earth and is to grow up into Him in all things. (Rom.8:9; Eph. 1:23; 4:12-16)<\/p><p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px;\"><strong>THE MINISTRY<\/strong><\/p><p data-mce-style=\"text-align: justify;\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; text-align: justify;\">We believe that God has designated and instituted duly called and equipped persons to function as the overseers of the Body of Christ(the church) upon the earth, and that these persons are charged with the equipping and edification of the saints until such time as the church has grown up into the image of Christ. (Eph. 4:11-15; Acts 20:28; 1 Peter 5:1-3)<\/p><p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px;\"><strong>THE JUDGMENT<\/strong><\/p><p data-mce-style=\"text-align: justify;\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; text-align: justify;\">We believe that the judgment of God has been passed unto Jesus. That God is not judging anyone at this time. There is A day of Judgment but it is not today. John 5:22; 12:47-48; Acts 17:30-31; 1 CO. 6:2)<\/p><p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px;\"><strong>FAITH TOWARDS GOD<\/strong><\/p><p data-mce-style=\"text-align: justify;\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; text-align: justify;\">We believe that we are to have faith towards God based upon the promises of God and that the fulfillment of those promises are based upon God\u2019s faithfulness. We believe that we are to have faith in God and not in our faith. Our faith is that God has and will keep His Word. (Mark 11:22-23; Heb. 6:1; 11:11; 2 Tim. 2:13)<\/p><p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px;\"><strong>GRACE & REPENTANCE FROM DEAD WORKS<\/strong><\/p><p data-mce-style=\"text-align: justify;\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; text-align: justify;\">We believe that the grace which came by Jesus Christ is extended to all to the end that they might believe in Him. We believe this grace is both the opportunity and the ability given to men to repent and turn to Christ for the remission of sin. We believe repentance from dead works includes forsaking any works that might be thought to merit favor or salvation from God. (John 1:16, 17; Rom. 11:6; Heb. 6:1; 9:14)<\/p><p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px;\"><strong>THE KINGDOM OF GOD<\/strong><\/p><p data-mce-style=\"text-align: justify;\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; text-align: justify;\">We believe the Kingdom of God is what Jesus preached and demonstrated and that we are to continue His ministry in the same manner and with the same results. We also believe that we are to advance the Kingdom of God upon the earth until such time as Christ shall entreat us to enter into the joy of our Lord. (Mark 1:15; Luke 4:43; Luke 9:60-10:11; Acts 8:12; 19:8; 20:25; 1 Co. 4:20; 2 Thess. 1:5; Matt. 25:19-23)<\/p><p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px;\"><strong>CHRISTIAN MISSION<\/strong><\/p><p data-mce-style=\"text-align: justify;\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; text-align: justify;\">We believe that the Christian Mission is to: Love God in thought, word, and deed & to love your neighbor as yourself, which is practiced by doing unto them as you would have done unto you. Your love for God and man will be demonstrated by reproducing disciples of the Lord Jesus Christ, both domestically and in foreign lands by every Biblical method. (Matt. 7:11-29; Matt. 28:18-20)<\/p>";

  static const String payTm = "9960877313";
  static const String payPal = "https://www.paypal.com/paypalme2/evansfrancis";
  static const String gPay = "9960877313";
  static const String gPayUPI = "evansfrancis333@oksbi";
  static const String SBIName = "Evans Francis Albert";
  static const String SBINumber = "32783806272";
  static const String SBIBank = "State Bank of India";
  static const String SBIIFSC = "SBIN0014726";
  static const String SBIBranch = "Jaripatka";
  static const String SBIATM = "5196 1901 3351 6452";

  static const String CreatedBy = "https://christianappdevelopers.com";

  static const String RateUs =
      "https://play.google.com/store/apps/details?id=com.christianappdevelopers.evansfrancis&hl=en";
  static const String RateUsIOS =
      "https://apps.apple.com/us/app/evans-francis/id1275222206?ls=1";

  static const String HOME_IMAGES = BASE_THUMBNAIL + "home/";
  static const String HOME_IMAGES_API = BASE_URL + "home-images";

  static const String REGISTER_DEVICE_API = BASE_URL + "add-device/";

  static const String ReachUs_API = BASE_URL + "reach-us/";
  static GlobalKey<ScaffoldState> scaffoldKey =
      GlobalKey(debugLabel: 'ScaffoldKey');

  static const String payPalId = "alanalex138@gmail.com";

//  static const String RateUs =
//      "https://play.google.com/store/apps/details?id=dr.legend.evans&hl=en";
}

class ChurchAppColors {
  static Color darkColor = Color.fromRGBO(18, 32, 48, 1);
  static Color darkAccent = Colors.grey[800];
  static Color lightBlue = Colors.cyan;
  static Color golden = Color(0xFFEAC131);
//  static Color golden = Color(0xFFD4AF37);
  static Color lightYellow = Color(0xFFFBDC73);
  static Color pinkRed = Color(0xFFF55564);
  static Color lightPinkRed = Color(0xFFF8A2A4);
  static Color navyBlue = Color(0xFF5A6571);
  static Color scaffoldBackground = Color(0xFFFFFFFF);
  static Color brandBackground = Color(0xFF19222B);
  static Color brandBackgroundLight = Color(0xFF495764);
  static Color prayerBackground = Color(0xFF302E30);
}
