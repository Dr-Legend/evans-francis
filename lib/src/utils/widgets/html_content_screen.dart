import 'package:evans/src/utils/widgets/html_view.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';

class HtmlContentScreenBasic extends StatelessWidget {
  final String name;
  final String content;
  final String date;
  final String image;
  final String title;
  final String createdAt;
  const HtmlContentScreenBasic(
      {Key key,
      this.name,
      this.date,
      this.content,
      this.image,
      this.title,
      this.createdAt})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                name,
                style: TextStyle(
                    fontSize: 25,
                    fontFamily: 'IMFellEnglishSC',
                    letterSpacing: 2),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Hero(
                tag: this.createdAt,
                child: title != 'Events'
                    ? CachedNetworkImage(
                        height: 300,
                        imageUrl: image,
                        width: double.maxFinite,
                        fit: BoxFit.fitWidth,
                      )
                    : CachedNetworkImage(
                        imageUrl: image,
                        width: double.maxFinite,
                        fit: BoxFit.fitWidth,
                      ),
              ),
            ),
            title != 'Events'
                ? Flexible(
                    child: HtmlView(
                      html: this.content ?? 'No content',
                    ),
                  )
                : Container(),
          ],
        ),
      ),
    );
  }
}
