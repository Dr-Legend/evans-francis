import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class PNetworkAssetImage extends StatelessWidget {
  final String image;
  final BoxFit fit;
  final double width, height;
  const PNetworkAssetImage(this.image,
      {Key key, this.fit, this.height, this.width})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
//    return CachedNetworkImage(
//      imageUrl: image,
//      placeholder: (context, url) => Center(child: CircularProgressIndicator()),
//      errorWidget: (context, url, error) => Center(child: Icon(Icons.error)),
//      fit: fit,
//      width: width,
//      height: height,
//    );
    return Image.asset(
      image,
      fit: BoxFit.fitHeight,
      width: width,
      height: height - 50,
      scale: 5,
    );
  }
}
