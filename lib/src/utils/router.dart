import 'package:evans/main.dart';
import 'package:evans/src/about/about_evans.dart';
import 'package:evans/src/blog/blog_screen.dart';
import 'package:evans/src/cloud/cloud_screen.dart';
import 'package:evans/src/events/events_screen.dart';
import 'package:evans/src/fire/fire_screen.dart';
import 'package:evans/src/gallery/gallery_screen.dart';
import 'package:evans/src/home/home_bloc.dart';
import 'package:evans/src/home/home_screen.dart';
import 'package:evans/src/home/test_home.dart';
import 'package:evans/src/invite/invite_screen.dart';
import 'package:evans/src/love/love_offering.dart';
import 'package:evans/src/messages/messages_screen.dart';
import 'package:evans/src/mission/mission_screen.dart';
import 'package:evans/src/prayer/prayer_screen.dart';
import 'package:evans/src/reach_us/reach_us_screen.dart';
import 'package:evans/src/settings/settings_bloc.dart';
import 'package:evans/src/statement_of_faith/statement_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Routes {
  static const String root = "/";
  static const String searchScreen = "/searchScreen";
  static const String settingsScreen = "/settingsScreen";
  static const String blog = "/blogScreen";
  static const String fire = "/fire";
  static const String cloud = "/cloud";

  static const String invite = "/invite";

  static const String Prayer = "/prayer";

  static const String statement = "/statement";

  static const String gallery = "/gallery";

  static const String events = "/events";

  static const String missions = "/missions";
  static const String loveOffering = "/loveOffering";
  static const String about = "/about";

  static const String messages = "/messages";

  static const String reachUs = "/reachUs";
}

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case Routes.root:
        // return MaterialPageRoute(builder: (_) => Login());
        return MaterialPageRoute(
            builder: (_) => BlocBuilder<HomeBloc, HomeState>(
                  builder: (BuildContext context, HomeState state) => Home(),
                ));

      case Routes.settingsScreen:
        return MaterialPageRoute(
          builder: (_) => BlocBuilder<SettingsBloc, SettingsState>(
            builder: (BuildContext context, SettingsState state) => Container(),
          ),
        );
      case Routes.searchScreen:
        return MaterialPageRoute(builder: (_) => ChurchApp());
      case Routes.blog:
        return MaterialPageRoute(builder: (_) => Blog());
      case Routes.fire:
        return MaterialPageRoute(builder: (_) => Fire());
      case Routes.cloud:
        return MaterialPageRoute(builder: (_) => Cloud());
      case Routes.invite:
        return MaterialPageRoute(builder: (_) => Invite());
      case Routes.Prayer:
        return MaterialPageRoute(builder: (_) => Prayer());
      case Routes.statement:
        return MaterialPageRoute(builder: (_) => Statement());
      case Routes.gallery:
        return MaterialPageRoute(builder: (_) => Gallery());
      case Routes.events:
        return MaterialPageRoute(builder: (_) => Events());
      case Routes.missions:
        return MaterialPageRoute(builder: (_) => Missions());
      case Routes.loveOffering:
        return MaterialPageRoute(builder: (_) => LoveOffering());
      case Routes.about:
        return MaterialPageRoute(builder: (_) => AboutEvans());
      case Routes.messages:
        return MaterialPageRoute(builder: (_) => Messages());
      case Routes.reachUs:
        return MaterialPageRoute(builder: (_) => ReachusScreen());
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                      child: Text('No route defined for ${settings.name}')),
                ));
    }
  }
}
