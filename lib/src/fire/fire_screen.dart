import 'package:easy_dialog/easy_dialog.dart';
import 'package:evans/src/fire/fire_bloc.dart';
import 'package:evans/src/models/fire_data.dart';
import 'package:evans/src/utils/constants.dart';
import 'package:evans/src/utils/widgets/html_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:intl/intl.dart';

class Fire extends StatefulWidget {
  final VoidCallback onSubmit;

  Fire({Key key, this.onSubmit}) : super(key: key);

  @override
  _FireState createState() => _FireState();
}

class _FireState extends State<Fire> {
  FireData fireData = FireData();
  final GlobalKey<FormBuilderState> _fbKey11 = GlobalKey<FormBuilderState>();
  @override
  Widget build(BuildContext context) {
    return BlocListener<FireBloc, FireState>(
      listener: (BuildContext context, FireState state) {
        if (state is SuccessFireState) {
          showDialog(
              context: context,
              builder: (_) => AlertDialog(
                    actions: <Widget>[
                      FlatButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                            widget.onSubmit();
                          },
                          child: Text('Close'))
                    ],
                    title: Row(
                      children: <Widget>[
                        Icon(
                          Icons.done,
                          color: Colors.green,
                        ),
                        SizedBox(
                          width: 12,
                        ),
                        Text(
                          'Success',
                          style: TextStyle(color: Colors.green),
                        ),
                      ],
                    ),
                    content: Text('Request Sent successfully!'),
                  ));
        }
        if (state is ErrorFireState) {
          showDialog(
              context: context,
              builder: (_) => AlertDialog(
                    actions: <Widget>[
                      FlatButton(
                          onPressed: () => Navigator.of(context).pop(),
                          child: Text('Close'))
                    ],
                    title: Row(
                      children: <Widget>[
                        Icon(
                          Icons.error_outline,
                          color: Colors.red,
                        ),
                        SizedBox(
                          width: 12,
                        ),
                        Text(
                          'Error',
                          style: TextStyle(color: Colors.red),
                        ),
                      ],
                    ),
                    content: Text('Unable to send request please try again!'),
                  ));
        }
      },
      child: BlocBuilder<FireBloc, FireState>(
          builder: (BuildContext context, FireState state) {
        return SingleChildScrollView(
          child: Container(
            color: ChurchAppColors.brandBackgroundLight,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Material(
                  color: ChurchAppColors.brandBackgroundLight,
                  elevation: 4,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Flexible(
                          child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Html(
                          data: Constants.FIRE,
                          defaultTextStyle: TextStyle(
                              color: ChurchAppColors.golden,
                              fontFamily: 'Stoke'),
                        ),
                      )),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          children: <Widget>[
                            FormBuilder(
                              key: _fbKey11,
                              initialValue: {
                                'date': DateTime.now(),
                                'accept_terms': false,
                              },
                              child: Column(
                                children: <Widget>[
                                  Text(
                                    Constants.FIRE_TITLE,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: 'Stoke',
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: FormBuilderTextField(
                                      initialValue: '',
                                      style: TextStyle(color: Colors.white),
                                      textInputAction: TextInputAction.done,
                                      attribute: "f_name",
                                      decoration: InputDecoration(
                                          labelText: "First Name"),
                                      validators: [
                                        FormBuilderValidators.required(),
                                      ],
                                      onSaved: (fname) {
                                        fireData.fName = fname;
                                      },
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: FormBuilderTextField(
                                      initialValue: '',
                                      style: TextStyle(color: Colors.white),
                                      textInputAction: TextInputAction.done,
                                      attribute: "m_name",
                                      decoration: InputDecoration(
                                          labelText: "Middle Name"),
                                      validators: [],
                                      onSaved: (mname) {
                                        fireData.mName = mname;
                                      },
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: FormBuilderTextField(
                                      initialValue: '',
                                      style: TextStyle(color: Colors.white),
                                      textInputAction: TextInputAction.done,
                                      attribute: "l_name",
                                      decoration: InputDecoration(
                                          labelText: "Last name"),
                                      validators: [
                                        FormBuilderValidators.required(),
                                      ],
                                      onSaved: (lname) {
                                        fireData.lName = lname;
                                      },
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: FormBuilderTextField(
                                      initialValue: '',
                                      style: TextStyle(color: Colors.white),
                                      textInputAction: TextInputAction.done,
                                      attribute: "email",
                                      keyboardType: TextInputType.emailAddress,
                                      decoration:
                                          InputDecoration(labelText: "Email"),
                                      validators: [
                                        FormBuilderValidators.required(),
                                        FormBuilderValidators.email(),
                                      ],
                                      onSaved: (email) {
                                        fireData.email = email;
                                      },
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: FormBuilderTextField(
                                      initialValue: '',
                                      style: TextStyle(color: Colors.white),
                                      textInputAction: TextInputAction.done,
                                      attribute: "phone",
                                      keyboardType: TextInputType.number,
                                      decoration: InputDecoration(
                                          labelText: "Contact No"),
                                      maxLength: 10,
                                      validators: [
                                        FormBuilderValidators.required(),
                                        FormBuilderValidators.numeric(),
                                        FormBuilderValidators.maxLength(10)
                                      ],
                                      onSaved: (contact) {
                                        fireData.phone = contact;
                                      },
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: FormBuilderTextField(
                                      initialValue: '',
                                      style: TextStyle(color: Colors.white),
                                      textInputAction: TextInputAction.done,
                                      attribute: "amount",
                                      keyboardType: TextInputType.number,
                                      decoration:
                                          InputDecoration(labelText: "Amount"),
                                      validators: [
                                        FormBuilderValidators.required(),
                                        FormBuilderValidators.numeric(),
                                      ],
                                      onSaved: (amount) {
                                        fireData.amount = amount;
                                      },
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: FormBuilderTextField(
                                      initialValue: '',
                                      style: TextStyle(color: Colors.white),
                                      textInputAction: TextInputAction.done,
                                      attribute: "country",
                                      keyboardType: TextInputType.text,
                                      decoration:
                                          InputDecoration(labelText: "Country"),
                                      validators: [
                                        FormBuilderValidators.required(),
                                      ],
                                      onSaved: (contact) {
                                        fireData.country = contact;
                                      },
                                    ),
                                  ),
//                                  Padding(
//                                    padding: const EdgeInsets.all(8.0),
//                                    child: FormBuilderTextField(
//                                      style: TextStyle(color: Colors.white),
//                                      textInputAction: TextInputAction.done,
//                                      attribute: "address",
//                                      keyboardType: TextInputType.text,
//                                      decoration:
//                                          InputDecoration(labelText: "Address"),
//                                      validators: [
//                                        FormBuilderValidators.required(),
//                                        FormBuilderValidators.minLength(5,
//                                            errorText:
//                                                'Address must be bigger than 5 characters!')
//                                      ],
//                                      onSaved: (address) {
//                                        fireData.address = address;
//                                      },
//                                    ),
//                                  ),
//                                  FormBuilderCheckboxList(
//                                    decoration: InputDecoration(
//                                        labelText: "The language of my people"),
//                                    attribute: "languages",
//                                    initialValue: ["Dart"],
//                                    options: [
//                                      FormBuilderFieldOption(value: "Dart"),
//                                      FormBuilderFieldOption(value: "Kotlin"),
//                                      FormBuilderFieldOption(value: "Java"),
//                                      FormBuilderFieldOption(value: "Swift"),
//                                      FormBuilderFieldOption(
//                                          value: "Objective-C"),
//                                    ],
//                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  MaterialButton(
                                    child: Text("Submit"),
                                    textColor: Colors.white,
                                    color: Colors.green,
                                    shape: StadiumBorder(
                                        side: BorderSide(color: Colors.green)),
                                    onPressed: () {
                                      if (_fbKey11.currentState
                                          .saveAndValidate()) {
                                        print(_fbKey11.currentState.value);
                                        fireData = FireData.fromJson(
                                            _fbKey11.currentState.value);
                                        BlocProvider.of<FireBloc>(context)
                                            .add(SendFireEvent(fireData));
                                        _fbKey11.currentState.reset();
                                      }
//                                      FocusScope.of(context).unfocus();
                                    },
                                  ),
//                                    MaterialButton(
//                                      elevation: 5,
//                                      color: Colors.white,
//                                      textColor: Colors.red,
//                                      child: Text("Reset"),
//                                      shape: StadiumBorder(
//                                          side: BorderSide(
//                                              color: Colors.white70)),
//                                      onPressed: () {
//                                        _fbKey.currentState.reset();
//                                      },
//                                    ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 240,
                            ),
                          ],
                        ),
                      )
                    ],
                  )),
            ),
          ),
        );
      }),
    );
  }
}
