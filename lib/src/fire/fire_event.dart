part of 'fire_bloc.dart';

@immutable
abstract class FireEvent {}

@immutable
class LoadFireEvent extends FireEvent {}

@immutable
class SendFireEvent extends FireEvent {
  final FireData data;

  SendFireEvent(this.data);
}
