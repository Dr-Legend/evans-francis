import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:evans/src/api/api.dart';
import 'package:evans/src/models/fire_data.dart';
import 'package:evans/src/utils/constants.dart';
import 'package:meta/meta.dart';

part 'fire_event.dart';

part 'fire_state.dart';

class FireBloc extends Bloc<FireEvent, FireState> {
  ApiMethods apiMethods = ApiMethods();
  @override
  FireState get initialState => InitialFireState();

  @override
  Stream<FireState> mapEventToState(FireEvent event) async* {
    if (event is SendFireEvent) {
      try {
        yield LoadingFireState();
        await apiMethods.requestInPost(
            Constants.PILLAR_FIRE_API, event.data.toJson());
        yield SuccessFireState();
      } catch (e) {
        yield ErrorFireState(e);
      }
    }
  }
}
