part of 'fire_bloc.dart';

@immutable
abstract class FireState {}

class InitialFireState extends FireState {}

class LoadingFireState extends FireState {}

class SuccessFireState extends FireState {}

class ErrorFireState extends FireState {
  final DioError error;

  ErrorFireState(this.error);
}
