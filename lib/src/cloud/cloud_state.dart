part of 'cloud_bloc.dart';

@immutable
abstract class CloudState {}

class InitialCloudState extends CloudState {}

class LoadingCloudState extends CloudState {}

class SuccessCloudState extends CloudState {}

class ErrorCloudState extends CloudState {
  final DioError error;

  ErrorCloudState(this.error);
}
