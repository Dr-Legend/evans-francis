import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:evans/src/api/api.dart';
import 'package:evans/src/models/cloud_data.dart';
import 'package:evans/src/utils/constants.dart';
import 'package:meta/meta.dart';

part 'cloud_event.dart';

part 'cloud_state.dart';

class CloudBloc extends Bloc<CloudEvent, CloudState> {
  ApiMethods apiMethods = ApiMethods();
  @override
  CloudState get initialState => InitialCloudState();

  @override
  Stream<CloudState> mapEventToState(CloudEvent event) async* {
    if (event is SendCloudEvent) {
      try {
        yield LoadingCloudState();
        await apiMethods.requestInPost(
            Constants.PILLAR_CLOUD_API, event.data.toJson());
        yield SuccessCloudState();
      } catch (e) {
        yield ErrorCloudState(e);
      }
    }
  }
}
