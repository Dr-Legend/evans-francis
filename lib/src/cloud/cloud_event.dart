part of 'cloud_bloc.dart';

@immutable
abstract class CloudEvent {}

@immutable
class LoadCloudEvent extends CloudEvent {}

@immutable
class SendCloudEvent extends CloudEvent {
  final CloudData data;

  SendCloudEvent(this.data);
}
