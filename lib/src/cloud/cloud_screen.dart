import 'package:evans/src/cloud/cloud_bloc.dart';
import 'package:evans/src/models/cloud_data.dart';
import 'package:evans/src/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_html/flutter_html.dart';

class Cloud extends StatefulWidget {
  final VoidCallback onSubmit;

  const Cloud({Key key, this.onSubmit}) : super(key: key);
  @override
  _CloudState createState() => _CloudState();
}

class _CloudState extends State<Cloud> {
  final GlobalKey<FormBuilderState> _fbKey2 = GlobalKey<FormBuilderState>();
  CloudData cloudData = CloudData();
  String time;
  @override
  Widget build(BuildContext context) {
    return BlocListener<CloudBloc, CloudState>(
      listener: (BuildContext context, CloudState state) {
        if (state is SuccessCloudState) {
          showDialog(
              context: context,
              builder: (_) => AlertDialog(
                    actions: <Widget>[
                      FlatButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                            this.widget.onSubmit();
                          },
                          child: Text('Close'))
                    ],
                    title: Row(
                      children: <Widget>[
                        Icon(
                          Icons.done,
                          color: Colors.green,
                        ),
                        SizedBox(
                          width: 12,
                        ),
                        Text(
                          'Success',
                          style: TextStyle(color: Colors.green),
                        ),
                      ],
                    ),
                    content: Text('Request Sent successfully!'),
                  ));
        }
        if (state is ErrorCloudState) {
          showDialog(
              context: context,
              builder: (_) => AlertDialog(
                    actions: <Widget>[
                      FlatButton(
                          onPressed: () => Navigator.of(context).pop(),
                          child: Text('Close'))
                    ],
                    title: Row(
                      children: <Widget>[
                        Icon(
                          Icons.error_outline,
                          color: Colors.red,
                        ),
                        SizedBox(
                          width: 12,
                        ),
                        Text(
                          'Error',
                          style: TextStyle(color: Colors.red),
                        ),
                      ],
                    ),
                    content: Text('Unable to send request please try again!'),
                  ));
        }
      },
      child: BlocBuilder<CloudBloc, CloudState>(
          builder: (BuildContext context, CloudState state) {
        return SingleChildScrollView(
          child: Container(
            color: ChurchAppColors.brandBackgroundLight,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Material(
                  color: ChurchAppColors.brandBackgroundLight,
                  elevation: 4,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Flexible(
                          child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Html(
                          data: Constants.CLOUD,
                          defaultTextStyle: TextStyle(
                            fontFamily: 'Stoke',
                            color: ChurchAppColors.golden,
                          ),
                        ),
                      )),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Material(
                          color: ChurchAppColors.brandBackgroundLight,
                          elevation: 2,
                          child: Column(
                            children: <Widget>[
                              Text(
                                Constants.CLOUD_TITLE,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: 'Stoke',
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20),
                              ),
                              FormBuilder(
                                key: _fbKey2,
                                initialValue: {
                                  'date': DateTime.now(),
                                  'accept_terms': false,
                                },
                                child: Column(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: FormBuilderTextField(
                                        initialValue: '',
                                        style: TextStyle(color: Colors.white),
                                        textInputAction: TextInputAction.done,
                                        attribute: "f_name",
                                        decoration: InputDecoration(
                                            labelText: "First Name"),
                                        validators: [
                                          FormBuilderValidators.required(),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: FormBuilderTextField(
                                        initialValue: '',
                                        style: TextStyle(color: Colors.white),
                                        textInputAction: TextInputAction.done,
                                        attribute: "m_name",
                                        decoration: InputDecoration(
                                            labelText: "Middle Name"),
                                        validators: [],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: FormBuilderTextField(
                                        initialValue: '',
                                        style: TextStyle(color: Colors.white),
                                        textInputAction: TextInputAction.done,
                                        attribute: "l_name",
                                        decoration: InputDecoration(
                                            labelText: "Last name"),
                                        validators: [
                                          FormBuilderValidators.required(),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: FormBuilderTextField(
                                        initialValue: '',
                                        style: TextStyle(color: Colors.white),
                                        textInputAction: TextInputAction.done,
                                        attribute: "email",
                                        keyboardType:
                                            TextInputType.emailAddress,
                                        decoration:
                                            InputDecoration(labelText: "Email"),
                                        validators: [
                                          FormBuilderValidators.required(),
                                          FormBuilderValidators.email(),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: FormBuilderTextField(
                                        initialValue: '',
                                        style: TextStyle(color: Colors.white),
                                        textInputAction: TextInputAction.done,
                                        attribute: "phone",
                                        keyboardType: TextInputType.number,
                                        decoration: InputDecoration(
                                            labelText: "Contact No"),
                                        maxLength: 10,
                                        validators: [
                                          FormBuilderValidators.required(),
                                          FormBuilderValidators.numeric(),
                                          FormBuilderValidators.maxLength(10)
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: FormBuilderTextField(
                                        initialValue: '',
                                        style: TextStyle(color: Colors.white),
                                        textInputAction: TextInputAction.done,
                                        attribute: "country",
                                        keyboardType: TextInputType.text,
                                        decoration: InputDecoration(
                                            labelText: "Country"),
                                        validators: [
                                          FormBuilderValidators.required(),
                                        ],
                                      ),
                                    ),
//                                    Padding(
//                                      padding: const EdgeInsets.all(8.0),
//                                      child: FormBuilderTextField(
//                                        initialValue: '',
//                                        style: TextStyle(color: Colors.white),
//                                        textInputAction: TextInputAction.done,
//                                        attribute: "address",
//                                        keyboardType: TextInputType.text,
//                                        decoration: InputDecoration(
//                                            labelText: "Address"),
//                                        validators: [
//                                          FormBuilderValidators.required(),
//                                          FormBuilderValidators.minLength(5,
//                                              errorText:
//                                                  'Address must be bigger than 5 characters!')
//                                        ],
//                                      ),
//                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: FormBuilderDropdown(
                                        initialValue: 'Select Time',
                                        style: TextStyle(
                                          color: ChurchAppColors.golden,
                                        ),
                                        attribute: "prayer_time",
                                        items: List.generate(60, (index) {
                                          return DropdownMenuItem(
                                              value: (index + 1).toString(),
                                              child:
                                                  Text((index + 1).toString()));
                                        })
                                          ..insert(
                                              0,
                                              DropdownMenuItem(
                                                  value: 'Select Time',
                                                  child: Text('Select Time'))),
                                        decoration: InputDecoration(
                                            labelText:
                                                "Prayer Time (In Hours)"),
                                        validators: [
                                          FormBuilderValidators.required(),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    MaterialButton(
                                      child: Text("Submit"),
                                      textColor: Colors.white,
                                      color: Colors.green,
                                      shape: StadiumBorder(
                                          side:
                                              BorderSide(color: Colors.green)),
                                      onPressed: () {
                                        if (_fbKey2.currentState
                                            .saveAndValidate()) {
                                          print(_fbKey2.currentState.value);
                                          cloudData = CloudData.fromJson(
                                              _fbKey2.currentState.value);
                                          BlocProvider.of<CloudBloc>(context)
                                              .add(SendCloudEvent(cloudData));
                                          _fbKey2.currentState.reset();
                                        }
//                                        FocusScope.of(context).unfocus();
                                      },
                                    ),
//                                    MaterialButton(
//                                      elevation: 5,
//                                      color: Colors.white,
//                                      textColor: Colors.red,
//                                      child: Text("Reset"),
//                                      shape: StadiumBorder(
//                                          side: BorderSide(
//                                              color: Colors.white70)),
//                                      onPressed: () {
//                                        _fbKey2.currentState.reset();
//                                      },
//                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 240,
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  )),
            ),
          ),
        );
      }),
    );
  }
}
