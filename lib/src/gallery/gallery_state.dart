part of 'gallery_bloc.dart';

@immutable
abstract class GalleryState {}

@immutable
class InitialGalleryState extends GalleryState {}

@immutable
class ErrorGalleryState extends GalleryState {
  final DioError error;

  ErrorGalleryState(this.error);
}

@immutable
class LoadedGalleryState extends GalleryState {
  final List<GalleryData> GalleryAlbums;

  LoadedGalleryState({this.GalleryAlbums});
}

@immutable
class LoadingGalleryState extends GalleryState {}
