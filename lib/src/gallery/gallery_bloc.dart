import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:evans/src/api/api.dart';
import 'package:evans/src/models/gallery_data.dart';
import 'package:evans/src/utils/constants.dart';
import 'package:meta/meta.dart';

part 'gallery_event.dart';

part 'gallery_state.dart';

class GalleryBloc extends Bloc<GalleryEvent, GalleryState> {
  final ApiMethods api = ApiMethods();
  @override
  GalleryState get initialState => InitialGalleryState();

  @override
  Stream<GalleryState> mapEventToState(GalleryEvent event) async* {
    if (event is LoadGalleryEvent) {
      try {
        yield LoadingGalleryState();
        var response = await api.requestInGet(Constants.GALLERY_ALBUM_API);
        List<GalleryData> galleryAlbums = galleryDataFromJson(response);
        yield LoadedGalleryState(GalleryAlbums: galleryAlbums);
      } catch (e) {
        yield ErrorGalleryState(e);
      }
    }
  }
}
