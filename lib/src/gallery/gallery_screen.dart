import 'package:cached_network_image/cached_network_image.dart';
import 'package:evans/src/api/api.dart';
import 'package:evans/src/gallery/gallery_bloc.dart';
import 'package:evans/src/models/gallery_data.dart';
import 'package:evans/src/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_scroll_gallery/flutter_scroll_gallery.dart';

class Gallery extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).canvasColor,
        title: Text("Gallery"),
      ),
      body: BlocBuilder<GalleryBloc, GalleryState>(
        builder: (BuildContext context, GalleryState state) {
          if (state is LoadingGalleryState) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          if (state is ErrorGalleryState) {
            return Center(
              child: Text("Unable to fetch Gallery please try again later.."),
            );
          }
          if (state is LoadedGalleryState) {
            return GridView.builder(
              shrinkWrap: true,
              scrollDirection: Axis.vertical,
              gridDelegate:
                  SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
              itemBuilder: (BuildContext context, int index) {
                return InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (_) => Images(
                                  images:
                                      state.GalleryAlbums[index].galleryImages,
                                )));
                  },
                  child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Hero(
                        tag: state.GalleryAlbums[index].id,
                        child: Material(
                          elevation: 5,
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                          child: Container(
                            decoration: BoxDecoration(
                                gradient: LinearGradient(
                                    begin: Alignment.bottomLeft,
                                    end: Alignment.topRight,
                                    stops: [
                                      0,
                                      1,
                                      0
                                    ],
                                    colors: [
                                      Colors.redAccent,
                                      Colors.white,
                                      Colors.red
                                    ]),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8)),
                                image: DecorationImage(
                                    fit: BoxFit.fill,
                                    image: CachedNetworkImageProvider(
                                        Constants.BASE_THUMBNAIL +
                                            state.GalleryAlbums[index].image
                                                .replaceAll(" ", "%20")))),
                            child: Center(
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.black.withOpacity(0.5),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(8)),
                                ),
                                width: double.maxFinite,
                                height: double.maxFinite,
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
//                                    Padding(
//                                      padding: const EdgeInsets.only(
//                                          left: 8.0, right: 8.0),
//                                      child: Text(
//                                        state.GalleryAlbums[index].title,
//                                        maxLines: 1,
//                                        overflow: TextOverflow.ellipsis,
//                                        style: TextStyle(
//                                          color: Colors.white,
//                                        ),
//                                      ),
//                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      )),
                );
              },
              itemCount: state.GalleryAlbums.length,
            );
          }
          return Container(
            child: Center(
              child: Text(
                  "Servers are under maintainance. Please try again later.."),
            ),
          );
        },
      ),
    );
  }
}

class Images extends StatefulWidget {
  final List<GalleryImages> images;

  const Images({Key key, this.images}) : super(key: key);
  @override
  _ImagesState createState() => _ImagesState();
}

class _ImagesState extends State<Images> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Gallery Images'),
        backgroundColor: Colors.black,
      ),
      backgroundColor: Colors.black,
      body: ScrollGallery(
        widget.images
            .map((image) => NetworkImage(
                Constants.BASE_THUMBNAIL + image.image.replaceAll(" ", "%20")))
            .toList()
            .cast<NetworkImage>(),
        interval: Duration(hours: 5),
        zoomable: true,
      ),
    );
  }
}
