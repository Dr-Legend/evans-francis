class CloudData {
  int _id;
  String _fName;
  String _mName;
  String _lName;
  String _phone;
  String _email;
  String _address;
  String _country;
  String _prayerTime;
  String _createdAt;
  String _updatedAt;

  CloudData(
      {int id,
      String fName,
      String mName,
      String lName,
      String phone,
      String email,
      String address,
      String country,
      String prayerTime,
      String createdAt,
      String updatedAt}) {
    this._id = id;
    this._fName = fName;
    this._mName = mName;
    this._lName = lName;
    this._phone = phone;
    this._email = email;
    this._address = address;
    this._country = country;
    this._prayerTime = prayerTime;
    this._createdAt = createdAt;
    this._updatedAt = updatedAt;
  }

  int get id => _id;
  set id(int id) => _id = id;
  String get fName => _fName;
  set fName(String fName) => _fName = fName;
  String get mName => _mName;
  set mName(String mName) => _mName = mName;
  String get lName => _lName;
  set lName(String lName) => _lName = lName;
  String get phone => _phone;
  set phone(String phone) => _phone = phone;
  String get email => _email;
  set email(String email) => _email = email;
  String get address => _address;
  set address(String address) => _address = address;
  String get country => _country;
  set country(String country) => _country = country;
  String get prayerTime => _prayerTime;
  set prayerTime(String prayerTime) => _prayerTime = prayerTime;
  String get createdAt => _createdAt;
  set createdAt(String createdAt) => _createdAt = createdAt;
  String get updatedAt => _updatedAt;
  set updatedAt(String updatedAt) => _updatedAt = updatedAt;

  CloudData.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _fName = json['f_name'];
    _mName = json['m_name'];
    _lName = json['l_name'];
    _phone = json['phone'];
    _email = json['email'];
    _address = json['address'];
    _country = json['country'];
    _prayerTime = json['prayer_time'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['f_name'] = this._fName;
    data['m_name'] = this._mName;
    data['l_name'] = this._lName;
    data['phone'] = this._phone;
    data['email'] = this._email;
    data['address'] = this._address;
    data['country'] = this._country;
    data['prayer_time'] = this._prayerTime;
    data['created_at'] = this._createdAt;
    data['updated_at'] = this._updatedAt;
    return data;
  }
}
