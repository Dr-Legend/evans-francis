// To parse this JSON data, do
//
//     final blogData = blogDataFromJson(jsonString);

import 'dart:convert';

List<BlogData> blogDataFromJson(dynamic str) {
//  final jsonData = json.decode(str);
  return new List<BlogData>.from(str.map((x) => BlogData.fromJson(x)));
}

String blogDataToJson(List<BlogData> data) {
  final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
  return json.encode(dyn);
}

class BlogData {
  int _id;
  String _title;
  String _image;
  String _description;
  String _date;
  String _substrDate;
  String _createdAt;
  String _updatedAt;

  BlogData(
      {int id,
      String title,
      String image,
      String description,
      String date,
      String substrDate,
      String createdAt,
      String updatedAt}) {
    this._id = id;
    this._title = title;
    this._image = image;
    this._description = description;
    this._date = date;
    this._substrDate = substrDate;
    this._createdAt = createdAt;
    this._updatedAt = updatedAt;
  }

  int get id => _id;
  set id(int id) => _id = id;
  String get title => _title;
  set title(String title) => _title = title;
  String get image => _image;
  set image(String image) => _image = image;
  String get description => _description;
  set description(String description) => _description = description;
  String get date => _date;
  set date(String date) => _date = date;
  String get substrDate => _substrDate;
  set substrDate(String substrDate) => _substrDate = substrDate;
  String get createdAt => _createdAt;
  set createdAt(String createdAt) => _createdAt = createdAt;
  String get updatedAt => _updatedAt;
  set updatedAt(String updatedAt) => _updatedAt = updatedAt;

  BlogData.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _title = json['title'];
    _image = json['image'];
    _description = json['description'];
    _date = json['date'];
    _substrDate = json['substr_date'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['title'] = this._title;
    data['image'] = this._image;
    data['description'] = this._description;
    data['date'] = this._date;
    data['substr_date'] = this._substrDate;
    data['created_at'] = this._createdAt;
    data['updated_at'] = this._updatedAt;
    return data;
  }
}
