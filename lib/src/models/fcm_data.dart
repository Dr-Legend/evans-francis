import 'package:flutter/material.dart';

class FCMData {
  final clickAction;
  final sound;
  final status;
  final screen;
  final extradata;

  FCMData(
      {Key key,
      this.clickAction,
      this.sound,
      this.status,
      this.screen,
      this.extradata})
      : super();
}
