// To parse this JSON data, do
//
//     final missionData = missionDataFromJson(jsonString);

import 'dart:convert';

List<MissionData> missionDataFromJson(dynamic str) {
//  final jsonData = json.decode(str);
  return new List<MissionData>.from(str.map((x) => MissionData.fromJson(x)));
}

String missionDataToJson(List<MissionData> data) {
  final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
  return json.encode(dyn);
}

class MissionData {
  int _id;
  String _title;
  String _description;
  String _image;
  String _date;
  String _time;
  String _createdAt;
  String _updatedAt;

  MissionData(
      {int id,
      String title,
      String description,
      String image,
      String date,
      String time,
      String createdAt,
      String updatedAt}) {
    this._id = id;
    this._title = title;
    this._description = description;
    this._image = image;
    this._date = date;
    this._time = time;
    this._createdAt = createdAt;
    this._updatedAt = updatedAt;
  }

  int get id => _id;
  set id(int id) => _id = id;
  String get title => _title;
  set title(String title) => _title = title;
  String get description => _description;
  set description(String description) => _description = description;
  String get image => _image;
  set image(String image) => _image = image;
  String get date => _date;
  set date(String date) => _date = date;
  String get time => _time;
  set time(String time) => _time = time;
  String get createdAt => _createdAt;
  set createdAt(String createdAt) => _createdAt = createdAt;
  String get updatedAt => _updatedAt;
  set updatedAt(String updatedAt) => _updatedAt = updatedAt;

  MissionData.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _title = json['title'];
    _description = json['description'];
    _image = json['image'];
    _date = json['date'];
    _time = json['time'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['title'] = this._title;
    data['description'] = this._description;
    data['image'] = this._image;
    data['date'] = this._date;
    data['time'] = this._time;
    data['created_at'] = this._createdAt;
    data['updated_at'] = this._updatedAt;
    return data;
  }
}
