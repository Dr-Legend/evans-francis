// To parse this JSON data, do
//
//     final aboutUsData = aboutUsDataFromJson(jsonString);

import 'dart:convert';

List<AboutUsData> aboutUsDataFromJson(String str) {
  final jsonData = json.decode(str);
  return new List<AboutUsData>.from(jsonData.map((x) => AboutUsData.fromJson(x)));
}

String aboutUsDataToJson(List<AboutUsData> data) {
  final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
  return json.encode(dyn);
}

class AboutUsData {
  String status;
  String id;
  String name;
  String content;

  AboutUsData({
    this.status,
    this.id,
    this.name,
    this.content,
  });

  factory AboutUsData.fromJson(Map<String, dynamic> json) => new AboutUsData(
    status: json["status"] == null ? null : json["status"],
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    content: json["content"] == null ? null : json["content"],
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "content": content == null ? null : content,
  };
}
