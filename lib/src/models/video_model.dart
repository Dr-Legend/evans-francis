// To parse this JSON data, do
//
//     final videoData = videoDataFromJson(jsonString);

import 'dart:convert';

List<VideoData> videoDataFromJson(dynamic str) =>
    new List<VideoData>.from(str.map((x) => VideoData.fromJson(x)));

String videoDataToJson(List<VideoData> data) =>
    json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

class VideoData {
  int _id;
  String _title;
  String _youtubeVideoId;
  String _date;
  String _substrDate;
  String _createdAt;
  String _updatedAt;

  VideoData(
      {int id,
      String title,
      String youtubeVideoId,
      String date,
      String substrDate,
      String createdAt,
      String updatedAt}) {
    this._id = id;
    this._title = title;
    this._youtubeVideoId = youtubeVideoId;
    this._date = date;
    this._substrDate = substrDate;
    this._createdAt = createdAt;
    this._updatedAt = updatedAt;
  }

  int get id => _id;
  set id(int id) => _id = id;
  String get title => _title;
  set title(String title) => _title = title;
  String get youtubeVideoId => _youtubeVideoId;
  set youtubeVideoId(String youtubeVideoId) => _youtubeVideoId = youtubeVideoId;
  String get date => _date;
  set date(String date) => _date = date;
  String get substrDate => _substrDate;
  set substrDate(String substrDate) => _substrDate = substrDate;
  String get createdAt => _createdAt;
  set createdAt(String createdAt) => _createdAt = createdAt;
  String get updatedAt => _updatedAt;
  set updatedAt(String updatedAt) => _updatedAt = updatedAt;

  VideoData.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _title = json['title'];
    _youtubeVideoId = json['youtube_video_id'];
    _date = json['date'];
    _substrDate = json['substr_date'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['title'] = this._title;
    data['youtube_video_id'] = this._youtubeVideoId;
    data['date'] = this._date;
    data['substr_date'] = this._substrDate;
    data['created_at'] = this._createdAt;
    data['updated_at'] = this._updatedAt;
    return data;
  }
}

enum CategoryName { ROCK_OF_LIFE }

final categoryNameValues =
    new EnumValues({"Rock of Life": CategoryName.ROCK_OF_LIFE});

enum Status { SUCCESS }

final statusValues = new EnumValues({"success": Status.SUCCESS});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
