// To parse this JSON data, do
//
//     final inviteData = inviteDataFromJson(jsonString);

import 'dart:convert';

InviteData inviteDataFromJson(String str) {
  final jsonData = json.decode(str);
  return InviteData.fromJson(jsonData);
}

String inviteDataToJson(InviteData data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class InviteData {
  String status;

  InviteData({
    this.status,
  });

  factory InviteData.fromJson(Map<String, dynamic> json) => new InviteData(
    status: json["status"] == null ? null : json["status"],
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
  };
}
