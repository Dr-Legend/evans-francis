class ReachUsData {
  int _id;
  String _name;
  int _phone;
  String _email;
  String _message;
  String _createdAt;
  String _updatedAt;

  ReachUsData(
      {int id,
      String name,
      int phone,
      String email,
      String message,
      String createdAt,
      String updatedAt}) {
    this._id = id;
    this._name = name;
    this._phone = phone;
    this._email = email;
    this._message = message;
    this._createdAt = createdAt;
    this._updatedAt = updatedAt;
  }

  int get id => _id;
  set id(int id) => _id = id;
  String get name => _name;
  set name(String name) => _name = name;
  int get phone => _phone;
  set phone(int phone) => _phone = phone;
  String get email => _email;
  set email(String email) => _email = email;
  String get message => _message;
  set message(String message) => _message = message;
  String get createdAt => _createdAt;
  set createdAt(String createdAt) => _createdAt = createdAt;
  String get updatedAt => _updatedAt;
  set updatedAt(String updatedAt) => _updatedAt = updatedAt;

  ReachUsData.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _name = json['name'];
    _phone = int.parse(json['phone']);
    _email = json['email'];
    _message = json['message'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['name'] = this._name;
    data['phone'] = this._phone;
    data['email'] = this._email;
    data['message'] = this._message;
    data['created_at'] = this._createdAt;
    data['updated_at'] = this._updatedAt;
    return data;
  }
}
