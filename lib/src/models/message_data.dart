// To parse this JSON data, do
//
//     final messageData = messageDataFromJson(jsonString);

import 'dart:convert';

List<MessageData> messageDataFromJson(dynamic str) =>
    new List<MessageData>.from(str.map((x) => MessageData.fromJson(x)));

String messageDataToJson(List<MessageData> data) =>
    json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

class MessageData {
  int _id;
  String _time;
  String _createdAt;
  String _updatedAt;
  String _date;
  String _image;
  String _description;
  String _title;

  MessageData(
      {int id,
      String time,
      String createdAt,
      String updatedAt,
      String date,
      String image,
      String description,
      String title}) {
    this._id = id;
    this._time = time;
    this._createdAt = createdAt;
    this._updatedAt = updatedAt;
    this._date = date;
    this._image = image;
    this._description = description;
    this._title = title;
  }

  int get id => _id;
  set id(int id) => _id = id;
  String get time => _time;
  set time(String time) => _time = time;
  String get createdAt => _createdAt;
  set createdAt(String createdAt) => _createdAt = createdAt;
  String get updatedAt => _updatedAt;
  set updatedAt(String updatedAt) => _updatedAt = updatedAt;
  String get date => _date;
  set date(String date) => _date = date;
  String get image => _image;
  set image(String image) => _image = image;
  String get description => _description;
  set description(String description) => _description = description;
  String get title => _title;
  set title(String title) => _title = title;

  MessageData.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _time = json['time'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
    _date = json['date'];
    _image = json['image'];
    _description = json['description'];
    _title = json['title'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['time'] = this._time;
    data['created_at'] = this._createdAt;
    data['updated_at'] = this._updatedAt;
    data['date'] = this._date;
    data['image'] = this._image;
    data['description'] = this._description;
    data['title'] = this._title;
    return data;
  }
}
