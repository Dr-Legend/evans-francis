// To parse this JSON data, do
//
//     final galleryData = galleryDataFromJson(jsonString);

import 'dart:convert';

List<GalleryData> galleryDataFromJson(dynamic str) => new List<GalleryData>.from(
    str.map((x) => GalleryData.fromJson(x)));

String galleryDataToJson(List<GalleryData> data) =>
    json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

class GalleryData {
  int _id;
  String _title;
  String _image;
  String _createdAt;
  String _updatedAt;
  List<GalleryImages> _galleryImages;

  GalleryData(
      {int id,
      String title,
      String image,
      String createdAt,
      String updatedAt,
      List<GalleryImages> galleryImages}) {
    this._id = id;
    this._title = title;
    this._image = image;
    this._createdAt = createdAt;
    this._updatedAt = updatedAt;
    this._galleryImages = galleryImages;
  }

  int get id => _id;
  set id(int id) => _id = id;
  String get title => _title;
  set title(String title) => _title = title;
  String get image => _image;
  set image(String image) => _image = image;
  String get createdAt => _createdAt;
  set createdAt(String createdAt) => _createdAt = createdAt;
  String get updatedAt => _updatedAt;
  set updatedAt(String updatedAt) => _updatedAt = updatedAt;
  List<GalleryImages> get galleryImages => _galleryImages;
  set galleryImages(List<GalleryImages> galleryImages) =>
      _galleryImages = galleryImages;

  GalleryData.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _title = json['title'];
    _image = json['image'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
    if (json['galleryImages'] != null) {
      _galleryImages = new List<GalleryImages>();
      json['galleryImages'].forEach((v) {
        _galleryImages.add(new GalleryImages.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['title'] = this._title;
    data['image'] = this._image;
    data['created_at'] = this._createdAt;
    data['updated_at'] = this._updatedAt;
    if (this._galleryImages != null) {
      data['galleryImages'] =
          this._galleryImages.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class GalleryImages {
  int _id;
  String _image;
  String _createdAt;
  String _updatedAt;

  GalleryImages({int id, String image, String createdAt, String updatedAt}) {
    this._id = id;
    this._image = image;
    this._createdAt = createdAt;
    this._updatedAt = updatedAt;
  }

  int get id => _id;
  set id(int id) => _id = id;
  String get image => _image;
  set image(String image) => _image = image;
  String get createdAt => _createdAt;
  set createdAt(String createdAt) => _createdAt = createdAt;
  String get updatedAt => _updatedAt;
  set updatedAt(String updatedAt) => _updatedAt = updatedAt;

  GalleryImages.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _image = json['image'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['image'] = this._image;
    data['created_at'] = this._createdAt;
    data['updated_at'] = this._updatedAt;
    return data;
  }
}
