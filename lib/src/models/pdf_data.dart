import 'dart:convert';

import 'package:flutter/cupertino.dart';

List<PDF> pdfDataFromJson(String str) {
  final jsonData = json.decode(str);
  return new List<PDF>.from(jsonData.map((x) => PDF.fromJson(x)));
}

String pdfDataToJson(List<PDF> data) {
  final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
  return json.encode(dyn);
}

class PDF {
  final String id;
  final String name;
  final String path;

  PDF({this.id, this.name, this.path});
  factory PDF.fromJson(Map<String, dynamic> json) => new PDF(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        path: json["pdf"] == null ? null : json["pdf"],
      );

  Map<String, dynamic> toJson() => {
        "pdf": path == null ? null : path,
        "id": id == null ? null : id,
        "name": name == null ? null : name,
      };
}
