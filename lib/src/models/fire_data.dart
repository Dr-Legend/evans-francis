class FireData {
  int _id;
  String _fName;
  String _mName;
  String _lName;
  String _email;
  String _phone;
  String _address;
  String _country;
  String _amount;
  String _createdAt;
  String _updatedAt;

  FireData(
      {int id,
      String fName,
      String mName,
      String lName,
      String email,
      String phone,
      String address,
      String country,
      String amount,
      String createdAt,
      String updatedAt}) {
    this._id = id;
    this._fName = fName;
    this._mName = mName;
    this._lName = lName;
    this._email = email;
    this._phone = phone;
    this._address = address;
    this._country = country;
    this._amount = amount;
    this._createdAt = createdAt;
    this._updatedAt = updatedAt;
  }

  int get id => _id;
  set id(int id) => _id = id;
  String get fName => _fName;
  set fName(String fName) => _fName = fName;
  String get mName => _mName;
  set mName(String mName) => _mName = mName;
  String get lName => _lName;
  set lName(String lName) => _lName = lName;
  String get email => _email;
  set email(String email) => _email = email;
  String get phone => _phone;
  set phone(String phone) => _phone = phone;
  String get address => _address;
  set address(String address) => _address = address;
  String get country => _country;
  set country(String country) => _country = country;
  String get amount => _amount;
  set amount(String amount) => _amount = amount;
  String get createdAt => _createdAt;
  set createdAt(String createdAt) => _createdAt = createdAt;
  String get updatedAt => _updatedAt;
  set updatedAt(String updatedAt) => _updatedAt = updatedAt;

  FireData.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _fName = json['f_name'];
    _mName = json['m_name'];
    _lName = json['l_name'];
    _email = json['email'];
    _phone = json['phone'];
    _address = json['address'];
    _country = json['country'];
    _amount = json['amount'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['f_name'] = this._fName;
    data['m_name'] = this._mName;
    data['l_name'] = this._lName;
    data['email'] = this._email;
    data['phone'] = this._phone;
    data['address'] = this._address;
    data['country'] = this._country;
    data['amount'] = this._amount;
    data['created_at'] = this._createdAt;
    data['updated_at'] = this._updatedAt;
    return data;
  }
}
