import 'dart:convert';

List<HomeData> homeDataFromJson(dynamic str) {
//  final jsonData = json.decode(str);
  return new List<HomeData>.from(str.map((x) => HomeData.fromJson(x)));
}

String homeDataToJson(List<HomeData> data) {
  final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
  return json.encode(dyn);
}

class HomeData {
  int _id;
  String _images;
  String _createdAt;
  String _updatedAt;

  HomeData({int id, String images, String createdAt, String updatedAt}) {
    this._id = id;
    this._images = images;
    this._createdAt = createdAt;
    this._updatedAt = updatedAt;
  }

  int get id => _id;
  set id(int id) => _id = id;
  String get images => _images;
  set images(String images) => _images = images;
  String get createdAt => _createdAt;
  set createdAt(String createdAt) => _createdAt = createdAt;
  String get updatedAt => _updatedAt;
  set updatedAt(String updatedAt) => _updatedAt = updatedAt;

  HomeData.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _images = json['images'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['images'] = this._images;
    data['created_at'] = this._createdAt;
    data['updated_at'] = this._updatedAt;
    return data;
  }
}
