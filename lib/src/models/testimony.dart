import 'package:meta/meta.dart';
import 'dart:convert';

List<TestimonyData> testimonyDataFromJson(String str) {
  final jsonData = json.decode(str);
  return new List<TestimonyData>.from(
      jsonData.map((x) => TestimonyData.fromJson(x)));
}

String testimonyDataToJson(List<TestimonyData> data) {
  final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
  return json.encode(dyn);
}

class TestimonyData {
  String status;
  String id;
  String name;
  String image;
  String testimonyDate;
  String content;

  TestimonyData({
    this.status,
    this.id,
    this.name,
    this.image,
    this.testimonyDate,
    this.content,
  });

  factory TestimonyData.fromJson(Map<String, dynamic> json) => TestimonyData(
        status: json["status"],
        id: json["id"],
        name: json["name"],
        image: json["image"],
        testimonyDate: json["testimonyDate"],
        content: json["content"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "id": id,
        "name": name,
        "image": image,
        "testimonyDate": testimonyDate,
        "content": content,
      };
}
