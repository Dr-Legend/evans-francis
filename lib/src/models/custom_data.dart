// To parse this JSON data, do
//
//     final customData = customDataFromJson(jsonString);

import 'dart:convert';

CustomData customDataFromJson(String str) => CustomData.fromJson(json.decode(str));

String customDataToJson(CustomData data) => json.encode(data.toJson());

class CustomData {
  String status;

  CustomData({
    this.status,
  });

  factory CustomData.fromJson(Map<String, dynamic> json) => new CustomData(
    status: json["status"] == null ? null : json["status"],
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
  };
}
