// To parse this JSON data, do
//
//     final aboutData = aboutDataFromJson(jsonString);

import 'dart:convert';

List<AboutData> aboutDataFromJson(String str) {
  final jsonData = json.decode(str);
  return new List<AboutData>.from(jsonData.map((x) => AboutData.fromJson(x)));
}

String aboutDataToJson(List<AboutData> data) {
  final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
  return json.encode(dyn);
}

class AboutData {
  String status;
  int id;
  String name;
  String content;

  AboutData({
    this.status,
    this.id,
    this.name,
    this.content,
  });

  factory AboutData.fromJson(Map<String, dynamic> json) => new AboutData(
    status: json["status"] == null ? null : json["status"],
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    content: json["content"] == null ? null : json["content"],
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "content": content == null ? null : content,
  };
}
