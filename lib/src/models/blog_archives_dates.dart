import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:queries/collections.dart';

List<ArchiveDates> archiveDatesFromJson(dynamic str) {
//  final jsonData = json.decode(str);
  var lists =
      new List<ArchiveDates>.from(str.map((x) => ArchiveDates.fromJson(x)))
          .toSet()
          .toList();
  var result = new Collection(lists).distinct();
  return result.reverse().toList();
}

String archiveDatesToJson(List<ArchiveDates> data) {
  final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
  return json.encode(dyn);
}

class ArchiveDates extends Equatable {
  String _substrDate;

  BlogArchiveDates({String substrDate}) {
    this._substrDate = substrDate;
  }

  String get substrDate => _substrDate;
  set substrDate(String substrDate) => _substrDate = substrDate;

  ArchiveDates.fromJson(Map<String, dynamic> json) {
    _substrDate = json['substr_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['substr_date'] = this._substrDate;
    return data;
  }

  @override
  // TODO: implement props
  List<Object> get props => [_substrDate];
}
