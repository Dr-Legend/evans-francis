// To parse this JSON data, do
//
//     final galleryListData = galleryListDataFromJson(jsonString);

import 'dart:convert';

List<GalleryListData> galleryListDataFromJson(String str) {
  final jsonData = json.decode(str);
  return new List<GalleryListData>.from(jsonData.map((x) => GalleryListData.fromJson(x)));
}

String galleryListDataToJson(List<GalleryListData> data) {
  final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
  return json.encode(dyn);
}

class GalleryListData {
  String status;
  int id;
  String name;
  String image;

  GalleryListData({
    this.status,
    this.id,
    this.name,
    this.image,
  });

  factory GalleryListData.fromJson(Map<String, dynamic> json) => new GalleryListData(
    status: json["status"] == null ? null : json["status"],
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    image: json["image"] == null ? null : json["image"],
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "image": image == null ? null : image,
  };
}
