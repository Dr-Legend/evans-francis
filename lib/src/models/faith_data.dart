// To parse this JSON data, do
//
//     final faithData = faithDataFromJson(jsonString);

import 'dart:convert';

List<FaithData> faithDataFromJson(String str) {
  final jsonData = json.decode(str);
  return new List<FaithData>.from(jsonData.map((x) => FaithData.fromJson(x)));
}

String faithDataToJson(List<FaithData> data) {
  final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
  return json.encode(dyn);
}

class FaithData {
  String status;
  String id;
  String name;
  String image;
  String content;

  FaithData({
    this.status,
    this.id,
    this.name,
    this.image,
    this.content,
  });

  factory FaithData.fromJson(Map<String, dynamic> json) => new FaithData(
    status: json["status"] == null ? null : json["status"],
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    image: json["image"] == null ? null : json["image"],
    content: json["content"] == null ? null : json["content"],
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "image": image == null ? null : image,
    "content": content == null ? null : content,
  };
}
