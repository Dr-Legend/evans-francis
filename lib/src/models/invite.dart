class InviteData {
  int _id;
  String _organization;
  String _title;
  String _email;
  int _phone;
  String _address;
  String _country;
  String _state;
  String _city;
  int _zipCode;
  String _website;
  String _contactPerson;
  int _contactPersonPhone;
  String _pastor;
  String _eventTitle;
  String _eventTheme;
  String _eventDate;
  String _attendance;
  String _createdAt;
  String _updatedAt;

  InviteData(
      {int id,
      String organization,
      String title,
      String email,
      int phone,
      String address,
      String country,
      String state,
      String city,
      int zipCode,
      String website,
      String contactPerson,
      int contactPersonPhone,
      String pastor,
      String eventTitle,
      String eventTheme,
      String eventDate,
      String attendance,
      String createdAt,
      String updatedAt}) {
    this._id = id;
    this._organization = organization;
    this._title = title;
    this._email = email;
    this._phone = phone;
    this._address = address;
    this._country = country;
    this._state = state;
    this._city = city;
    this._zipCode = zipCode;
    this._website = website;
    this._contactPerson = contactPerson;
    this._contactPersonPhone = contactPersonPhone;
    this._pastor = pastor;
    this._eventTitle = eventTitle;
    this._eventTheme = eventTheme;
    this._eventDate = eventDate;
    this._attendance = attendance;
    this._createdAt = createdAt;
    this._updatedAt = updatedAt;
  }

  int get id => _id;
  set id(int id) => _id = id;
  String get organization => _organization;
  set organization(String organization) => _organization = organization;
  String get title => _title;
  set title(String title) => _title = title;
  String get email => _email;
  set email(String email) => _email = email;
  int get phone => _phone;
  set phone(int phone) => _phone = phone;
  String get address => _address;
  set address(String address) => _address = address;
  String get country => _country;
  set country(String country) => _country = country;
  String get state => _state;
  set state(String state) => _state = state;
  String get city => _city;
  set city(String city) => _city = city;
  int get zipCode => _zipCode;
  set zipCode(int zipCode) => _zipCode = zipCode;
  String get website => _website;
  set website(String website) => _website = website;
  String get contactPerson => _contactPerson;
  set contactPerson(String contactPerson) => _contactPerson = contactPerson;
  int get contactPersonPhone => _contactPersonPhone;
  set contactPersonPhone(int contactPersonPhone) =>
      _contactPersonPhone = contactPersonPhone;
  String get pastor => _pastor;
  set pastor(String pastor) => _pastor = pastor;
  String get eventTitle => _eventTitle;
  set eventTitle(String eventTitle) => _eventTitle = eventTitle;
  String get eventTheme => _eventTheme;
  set eventTheme(String eventTheme) => _eventTheme = eventTheme;
  String get eventDate => _eventDate;
  set eventDate(String eventDate) => _eventDate = eventDate;
  String get attendance => _attendance;
  set attendance(String attendance) => _attendance = attendance;
  String get createdAt => _createdAt;
  set createdAt(String createdAt) => _createdAt = createdAt;
  String get updatedAt => _updatedAt;
  set updatedAt(String updatedAt) => _updatedAt = updatedAt;

  InviteData.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _organization = json['organization'];
    _title = json['title'];
    _email = json['email'];
    _phone = int.parse(json['phone']);
    _address = json['address'];
    _country = json['country'];
    _state = json['state'];
    _city = json['city'];
    _zipCode = int.parse(json['zip_code']);
    _website = json['website'];
    _contactPerson = json['contact_person'];
    _contactPersonPhone = int.parse(json['contact_person_phone']);
    _pastor = json['pastor'];
    _eventTitle = json['event_title'];
    _eventTheme = json['event_theme'];
    _eventDate = json['event_date'];
    _attendance = json['attendance'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['organization'] = this._organization;
    data['title'] = this._title;
    data['email'] = this._email;
    data['phone'] = this._phone;
    data['address'] = this._address;
    data['country'] = this._country;
    data['state'] = this._state;
    data['city'] = this._city;
    data['zip_code'] = this._zipCode;
    data['website'] = this._website;
    data['contact_person'] = this._contactPerson;
    data['contact_person_phone'] = this._contactPersonPhone;
    data['pastor'] = this._pastor;
    data['event_title'] = this._eventTitle;
    data['event_theme'] = this._eventTheme;
    data['event_date'] = this._eventDate;
    data['attendance'] = this._attendance;
    data['created_at'] = this._createdAt;
    data['updated_at'] = this._updatedAt;
    return data;
  }
}
