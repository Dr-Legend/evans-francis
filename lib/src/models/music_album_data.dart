// To parse this JSON data, do
//
//     final musicAlbumData = musicAlbumDataFromJson(jsonString);

import 'dart:convert';

List<MusicAlbumData> musicAlbumDataFromJson(dynamic str) =>
    new List<MusicAlbumData>.from(str.map((x) => MusicAlbumData.fromJson(x)));

String musicAlbumDataToJson(List<MusicAlbumData> data) =>
    json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

class MusicAlbumData {
  int _id;
  String _albumTitle;
  String _albumImage;
  String _createdAt;
  String _updatedAt;
  List<Audios> _audios;

  MusicAlbumData(
      {int id,
      String albumTitle,
      String albumImage,
      String createdAt,
      String updatedAt,
      List<Audios> audios}) {
    this._id = id;
    this._albumTitle = albumTitle;
    this._albumImage = albumImage;
    this._createdAt = createdAt;
    this._updatedAt = updatedAt;
    this._audios = audios;
  }

  int get id => _id;
  set id(int id) => _id = id;
  String get albumTitle => _albumTitle;
  set albumTitle(String albumTitle) => _albumTitle = albumTitle;
  String get albumImage => _albumImage;
  set albumImage(String albumImage) => _albumImage = albumImage;
  String get createdAt => _createdAt;
  set createdAt(String createdAt) => _createdAt = createdAt;
  String get updatedAt => _updatedAt;
  set updatedAt(String updatedAt) => _updatedAt = updatedAt;
  List<Audios> get audios => _audios;
  set audios(List<Audios> audios) => _audios = audios;

  MusicAlbumData.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _albumTitle = json['album_title'];
    _albumImage = json['album_image'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
    if (json['audios'] != null) {
      _audios = new List<Audios>();
      json['audios'].forEach((v) {
        _audios.add(new Audios.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['album_title'] = this._albumTitle;
    data['album_image'] = this._albumImage;
    data['created_at'] = this._createdAt;
    data['updated_at'] = this._updatedAt;
    if (this._audios != null) {
      data['audios'] = this._audios.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Audios {
  int _id;
  String _audioTitle;
  String _image;
  String _audioFile;
  String _createdAt;
  String _updatedAt;

  Audios(
      {int id,
      String audioTitle,
      String image,
      String audioFile,
      String createdAt,
      String updatedAt}) {
    this._id = id;
    this._audioTitle = audioTitle;
    this._image = image;
    this._audioFile = audioFile;
    this._createdAt = createdAt;
    this._updatedAt = updatedAt;
  }

  int get id => _id;
  set id(int id) => _id = id;
  String get audioTitle => _audioTitle;
  set audioTitle(String audioTitle) => _audioTitle = audioTitle;
  String get image => _image;
  set image(String image) => _image = image;
  String get audioFile => _audioFile;
  set audioFile(String audioFile) => _audioFile = audioFile;
  String get createdAt => _createdAt;
  set createdAt(String createdAt) => _createdAt = createdAt;
  String get updatedAt => _updatedAt;
  set updatedAt(String updatedAt) => _updatedAt = updatedAt;

  Audios.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _audioTitle = json['audio_title'];
    _image = json['image'];
    _audioFile = json['audio_file'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['audio_title'] = this._audioTitle;
    data['image'] = this._image;
    data['audio_file'] = this._audioFile;
    data['created_at'] = this._createdAt;
    data['updated_at'] = this._updatedAt;
    return data;
  }
}
