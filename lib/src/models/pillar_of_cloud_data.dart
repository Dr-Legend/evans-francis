// To parse this JSON data, do
//
//     final pillarOfCloudData = pillarOfCloudDataFromJson(jsonString);

import 'dart:convert';

List<PillarOfCloudData> pillarOfCloudDataFromJson(String str) {
  final jsonData = json.decode(str);
  return new List<PillarOfCloudData>.from(jsonData.map((x) => PillarOfCloudData.fromJson(x)));
}

String pillarOfCloudDataToJson(List<PillarOfCloudData> data) {
  final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
  return json.encode(dyn);
}

class PillarOfCloudData {
  String status;
  int id;
  String name;
  String content;

  PillarOfCloudData({
    this.status,
    this.id,
    this.name,
    this.content,
  });

  factory PillarOfCloudData.fromJson(Map<String, dynamic> json) => new PillarOfCloudData(
    status: json["status"] == null ? null : json["status"],
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    content: json["content"] == null ? null : json["content"],
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "content": content == null ? null : content,
  };
}
