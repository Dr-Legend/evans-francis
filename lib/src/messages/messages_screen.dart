import 'dart:io';
import 'dart:typed_data';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:evans/src/messages/messages_bloc.dart';
import 'package:evans/src/utils/constants.dart';
import 'package:evans/src/utils/widgets/network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
//import 'package:share/share.dart';

class Messages extends StatelessWidget {
  static final String path = "lib/src/pages/travel/thome.dart";
  DateFormat dateFormat = DateFormat('dd/MM/yyyy hh:mm aa');
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).canvasColor,
        elevation: 0,
        title: Text('Messages'),
      ),
      body: SafeArea(
        child: BlocBuilder<MessagesBloc, MessagesState>(
            builder: (BuildContext context, MessagesState state) {
          if (state is LoadingMessagesState) {
            return Center(child: CircularProgressIndicator());
          }
          if (state is LoadedMessagesState) {
            return Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Neha & Evans Francis",
                              style: TextStyle(
                                  fontSize: 18.0, fontWeight: FontWeight.bold)),
                          Text(
                            "The Prophetic Words",
                            style: TextStyle(color: Colors.grey.shade700),
                          )
                        ],
                      ),
                      CircleAvatar(
                        backgroundColor: Colors.transparent,
                        backgroundImage:
                            AssetImage('assets/images/about-t.png'),
                        radius: 40,
                      )
                    ],
                  ),
                ),
                Flexible(
                  child: ListView.builder(
                    shrinkWrap: true,
                    itemBuilder: (BuildContext context, int index) {
                      if (state.messageData.isNotEmpty) {
                        return _buildFeaturedItem(context,
                            image: Constants.BASE_THUMBNAIL +
                                state.messageData[index].image,
                            title: state.messageData[index].title,
                            subtitle: dateFormat.format(DateTime.parse(
                                state.messageData[index].createdAt)));
                      } else {
                        return Container(
                          child: Center(
                            child: Text('No Messages Found!'),
                          ),
                        );
                      }
                    },
                    itemCount: state.messageData.length,
                  ),
                ),
              ],
            );
          }
          return Container(
            child: Center(
              child: Text(
                  "Servers are under maintainance. Please try again later.."),
            ),
          );
        }),
      ),
    );
  }

  Widget _buildItem({String title}) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
      child: Material(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
        elevation: 5.0,
        child: Container(
          padding: EdgeInsets.all(16.0),
          child: Text(title,
              style: TextStyle(
                fontSize: 20.0,
              )),
        ),
      ),
    );
  }

  Container _buildFeaturedItem(BuildContext context,
      {String image, String title, String subtitle}) {
    return Container(
      padding: EdgeInsets.only(left: 16.0, top: 8.0, right: 16.0, bottom: 16.0),
      child: Material(
        elevation: 5.0,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
        child: Stack(
          children: <Widget>[
            ClipRRect(
                borderRadius: BorderRadius.circular(5.0),
                child: PNetworkImage(
                  image,
                  fit: BoxFit.cover,
                )),
            Positioned(
              right: 10.0,
              top: 10.0,
              child: FloatingActionButton(
                heroTag: subtitle,
                mini: true,
                backgroundColor: Colors.white,
                elevation: 5,
                onPressed: () async {
                  //Share.share("$title \n ${Constants.RateUs}");
                  Scaffold.of(context).showSnackBar(SnackBar(
                      content: Row(
                    children: <Widget>[
                      CircularProgressIndicator(),
                      SizedBox(
                        width: 12,
                      ),
                      Text('Preparing image to send...')
                    ],
                  )));
                  var request = await HttpClient().getUrl(Uri.parse(image));
                  var response = await request.close();
                  Uint8List bytes =
                      await consolidateHttpClientResponseBytes(response);
                  Scaffold.of(context).hideCurrentSnackBar();
                  await Share.file(
                      'Todays Message', 'message.jpg', bytes, 'image/jpg',
                      text:
                          'Download Evans Francis App now! \nPlayStore: ${Constants.RateUs} \n Appstore: ${Constants.RateUsIOS}');
                },
                child: Icon(
                  Icons.share,
                ),
              ),
            ),
            title.isNotEmpty
                ? Positioned(
                    bottom: 20.0,
                    child: Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                      color: Colors.black.withOpacity(0.7),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Text(title,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20.0,
                                  fontWeight: FontWeight.bold)),
//                          Text(subtitle, style: TextStyle(color: Colors.white))
                        ],
                      ),
                    ),
                  )
                : Container(),
          ],
        ),
      ),
    );
  }
}
//_buildFeaturedItem(
//image: kathmandu1,
//title: "Kathmandu",
//subtitle: "90 places worth to visit"),
