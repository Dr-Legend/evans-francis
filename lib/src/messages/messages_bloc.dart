import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:evans/src/api/api.dart';
import 'package:evans/src/models/message_data.dart';
import 'package:evans/src/utils/constants.dart';
import 'package:meta/meta.dart';

part 'messages_event.dart';

part 'messages_state.dart';

class MessagesBloc extends Bloc<MessagesEvent, MessagesState> {
  ApiMethods api = ApiMethods();
  @override
  MessagesState get initialState => InitialMessagesState();

  @override
  Stream<MessagesState> mapEventToState(MessagesEvent event) async* {
    if (event is LoadMessagesEvent) {
      yield LoadingMessagesState();
      try {
        var response = await api.requestInGet(Constants.MESSAGE_API);
//        var dateRespose = await api.requestInGet(Constants.BLOGS_DATES_API);
        List<MessageData> messages = messageDataFromJson(response);
//        List<ArchiveDates> dates = archiveDatesFromJson(dateRespose[0]);
        yield LoadedMessagesState(messageData: messages);
      } catch (e) {
        yield ErrorMessagesState(e);
      }
    }
  }
}
