part of 'messages_bloc.dart';

@immutable
abstract class MessagesState {}

class InitialMessagesState extends MessagesState {}

class LoadingMessagesState extends MessagesState {}

class LoadedMessagesState extends MessagesState {
  final List<MessageData> messageData;
//  final List<ArchiveDates> dates;

  LoadedMessagesState({this.messageData});
}

class ErrorMessagesState extends MessagesState {
  final DioError error;

  ErrorMessagesState(this.error);
}
