import 'package:evans/src/models/reach_us.dart';
import 'package:evans/src/reach_us/reach_us_bloc.dart';
import 'package:evans/src/utils/constants.dart';
import 'package:evans/src/utils/widgets/asset_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class ReachusScreen extends StatelessWidget {
  final GlobalKey<FormBuilderState> _fbKey1 = GlobalKey<FormBuilderState>();
  ReachUsData data = ReachUsData();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: ChurchAppColors.brandBackgroundLight,
          elevation: 0,
          title: Text('Reach us'),
        ),
        backgroundColor: ChurchAppColors.brandBackgroundLight,
        body: SafeArea(
          child: BlocListener<ReachUsBloc, ReachUsState>(
            listener: (BuildContext context, ReachUsState state) {
              if (state is SuccessReachUsState) {
                showDialog(
                    context: context,
                    builder: (_) => AlertDialog(
                          actions: <Widget>[
                            FlatButton(
                                onPressed: () => Navigator.of(context).pop(),
                                child: Text('Close'))
                          ],
                          title: Row(
                            children: <Widget>[
                              Icon(
                                Icons.done,
                                color: Colors.green,
                              ),
                              SizedBox(
                                width: 12,
                              ),
                              Text(
                                'Success',
                                style: TextStyle(color: Colors.green),
                              ),
                            ],
                          ),
                          content: Text('Request Sent successfully!'),
                        ));
              }
              if (state is ErrorReachUsState) {
                showDialog(
                    context: context,
                    builder: (_) => AlertDialog(
                          actions: <Widget>[
                            FlatButton(
                                onPressed: () => Navigator.of(context).pop(),
                                child: Text('Close'))
                          ],
                          title: Row(
                            children: <Widget>[
                              Icon(
                                Icons.error_outline,
                                color: Colors.red,
                              ),
                              SizedBox(
                                width: 12,
                              ),
                              Text(
                                'Error',
                                style: TextStyle(color: Colors.red),
                              ),
                            ],
                          ),
                          content:
                              Text('Unable to send request please try again!'),
                        ));
              }
            },
            child: BlocBuilder<ReachUsBloc, ReachUsState>(
                builder: (BuildContext context, ReachUsState state) {
              return SingleChildScrollView(
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image.asset('assets/images/brand-t.png'),
                      Padding(
                        padding: const EdgeInsets.all(0.0),
                        child: Container(
                          padding: EdgeInsets.all(20.0),
                          child: Material(
                            elevation: 0,
                            type: MaterialType.card,
                            borderRadius: BorderRadius.circular(20),
                            color: ChurchAppColors.brandBackgroundLight,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                SizedBox(
                                  height: 20.0,
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    'Send us a Message',
                                    style: TextStyle(
                                        fontSize: 20,
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                FormBuilder(
                                  key: _fbKey1,
                                  initialValue: {
                                    'date': DateTime.now(),
                                    'accept_terms': false,
                                  },
                                  child: Column(
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: FormBuilderTextField(
                                          textInputAction: TextInputAction.next,
                                          style: TextStyle(color: Colors.white),
                                          attribute: "name",
                                          decoration: InputDecoration(
                                              labelText: "Full Name",
                                              border: UnderlineInputBorder()),
                                          validators: [
                                            FormBuilderValidators.required(),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: FormBuilderTextField(
                                          textInputAction: TextInputAction.next,
                                          attribute: "email",
                                          keyboardType:
                                              TextInputType.emailAddress,
                                          style: TextStyle(color: Colors.white),
                                          decoration: InputDecoration(
                                              labelText: "Email",
                                              border: UnderlineInputBorder()),
                                          validators: [
                                            FormBuilderValidators.email(),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: FormBuilderTextField(
                                          textInputAction: TextInputAction.next,
                                          attribute: "phone",
                                          keyboardType: TextInputType.number,
                                          style: TextStyle(color: Colors.white),
                                          decoration: InputDecoration(
                                              labelText: "Contact No",
                                              border: UnderlineInputBorder()),
                                          maxLength: 10,
                                          validators: [
                                            FormBuilderValidators.numeric(),
                                            FormBuilderValidators.maxLength(10)
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: FormBuilderTextField(
                                          textInputAction:
                                              TextInputAction.newline,
                                          attribute: "message",
                                          style: TextStyle(color: Colors.white),
                                          keyboardType: TextInputType.text,
                                          decoration: InputDecoration(
                                              labelText: "Message",
                                              border: UnderlineInputBorder()),
                                          validators: [
                                            FormBuilderValidators.required(),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: <Widget>[
                                      Flexible(
                                        child: SizedBox(
                                          child: MaterialButton(
                                            child: Text("Submit"),
                                            textColor: Colors.white,
                                            color: Colors.green,
                                            shape: RoundedRectangleBorder(
                                                side: BorderSide(
                                                    color: Colors.green),
                                                borderRadius:
                                                    BorderRadius.circular(10)),
                                            onPressed: () {
                                              if (_fbKey1.currentState
                                                  .saveAndValidate()) {
                                                print(
                                                    _fbKey1.currentState.value);
                                                data = ReachUsData.fromJson(
                                                    _fbKey1.currentState.value);
                                                BlocProvider.of<ReachUsBloc>(
                                                        context)
                                                    .add(
                                                        SendReachUsEvent(data));
                                                _fbKey1.currentState.reset();
                                              }
                                            },
                                          ),
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width -
                                              100,
                                          height: 40,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }),
          ),
        ));
  }
}
