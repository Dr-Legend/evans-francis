import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:evans/src/api/api.dart';
import 'package:evans/src/models/reach_us.dart';
import 'package:evans/src/utils/constants.dart';
import 'package:meta/meta.dart';

part 'reach_us_event.dart';

part 'reach_us_state.dart';

class ReachUsBloc extends Bloc<ReachUsEvent, ReachUsState> {
  ApiMethods apiMethods = ApiMethods();
  @override
  ReachUsState get initialState => InitialReachUsState();

  @override
  Stream<ReachUsState> mapEventToState(ReachUsEvent event) async* {
    if (event is SendReachUsEvent) {
      try {
        yield LoadingReachUsState();
        await apiMethods.requestInPost(
            Constants.ReachUs_API, event.data.toJson());
        yield SuccessReachUsState();
      } catch (e) {
        yield ErrorReachUsState(e);
      }
    }
  }
}
