part of 'reach_us_bloc.dart';

@immutable
abstract class ReachUsEvent {}

@immutable
class SendReachUsEvent extends ReachUsEvent {
  final ReachUsData data;

  SendReachUsEvent(this.data);
}
