part of 'reach_us_bloc.dart';

@immutable
abstract class ReachUsState {}

class InitialReachUsState extends ReachUsState {}

class LoadingReachUsState extends ReachUsState {}

class SuccessReachUsState extends ReachUsState {}

class ErrorReachUsState extends ReachUsState {
  final DioError error;

  ErrorReachUsState(this.error);
}
