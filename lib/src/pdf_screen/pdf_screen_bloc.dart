import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:evans/src/models/pdf_data.dart';
import 'package:meta/meta.dart';

part 'pdf_screen_event.dart';

part 'pdf_screen_state.dart';

class PdfScreenBloc extends Bloc<PdfScreenEvent, PdfScreenState> {
  @override
  PdfScreenState get initialState => InitialPdfScreenState();

  @override
  Stream<PdfScreenState> mapEventToState(PdfScreenEvent event) async* {
    // TODO: Add your event logic
  }
}
