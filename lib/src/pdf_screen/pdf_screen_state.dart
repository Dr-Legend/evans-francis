part of 'pdf_screen_bloc.dart';

@immutable
abstract class PdfScreenState {}

class InitialPdfScreenState extends PdfScreenState {}

class LoadingPdfScreenState extends PdfScreenState {}

class ErrorPdfScreenState extends PdfScreenState {
  final String error;

  ErrorPdfScreenState(this.error);
}

class PdfReadyScreenState extends PdfScreenState {
  final List<PDF> pdfNotes;

  PdfReadyScreenState({this.pdfNotes});
}
