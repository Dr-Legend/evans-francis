part of 'pdf_screen_bloc.dart';

@immutable
abstract class PdfScreenEvent {}

class LoadPdfScreenEvent extends PdfScreenEvent {}
