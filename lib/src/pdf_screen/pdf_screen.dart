import 'dart:io';
import 'dart:typed_data';

import 'package:evans/src/old_dev_non_bloc/utils/constants.dart';
import 'package:evans/src/pdf_screen/pdf_screen_bloc.dart';
import 'package:evans/src/utils/widgets/list_tile_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pdf_viewer_plugin/pdf_viewer_plugin.dart';
import 'package:http/http.dart' as http;

class PdfScreen extends StatefulWidget {
  @override
  _PdfScreenState createState() => _PdfScreenState();
}

class _PdfScreenState extends State<PdfScreen> {
  String path;

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/teste.pdf');
  }

  @override
  void initState() {
    super.initState();
  }

  Future<File> writeCounter(Uint8List stream) async {
    final file = await _localFile;

    // Write the file
    return file.writeAsBytes(stream);
  }

  Future<Uint8List> fetchPost(String pdfFilePath) async {
    final response = await http.get(Constants.PDF_File_BASE_URL + pdfFilePath);
    final responseJson = response.bodyBytes;

    return responseJson;
  }

  loadPdf(String pdfFilePath) async {
    writeCounter(await fetchPost(pdfFilePath));
    path = (await _localFile).path;

    if (!mounted) return;

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: BlocBuilder<PdfScreenBloc, PdfScreenState>(
          builder: (BuildContext context, PdfScreenState state) {
        if (state is LoadingPdfScreenState) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else if (state is PdfReadyScreenState) {
          return Container(
            child: ListView.builder(
                itemCount: state.pdfNotes.isEmpty ? 0 : state.pdfNotes.length,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () async {
                      await loadPdf(state.pdfNotes[index].path);
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => PdfContentScreen(
                                    path: path,
                                    name: state.pdfNotes[index].name,
                                  )));
                    },
                    child: ListTileCard(
                      key: Key(state.pdfNotes[index].name),
                      name: state.pdfNotes[index].name,
                      assetUrl: 'images/icon_pdf.png',
                      useAssetImage: true,
                    ),
                  );
                }),
          );
        } else if (state is ErrorPdfScreenState) {
          return Center(
            child: Text(state.error),
          );
        }
        return Center(
          child: Container(
            child: Text("you shouldn't see this"),
          ),
        );
      }),
    );
  }
}

class PdfContentScreen extends StatelessWidget {
  final String path;
  final String name;
  const PdfContentScreen({Key key, this.path, this.name}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          name,
        ),
      ),
      body: PdfViewer(
        filePath: path,
      ),
    );
  }
}
