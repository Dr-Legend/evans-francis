import 'package:cached_network_image/cached_network_image.dart';
import 'package:evans/src/api/api.dart';
import 'package:evans/src/models/music_album_data.dart';
import 'package:evans/src/music/audio_songs.dart';
import 'package:evans/src/music/music_bloc.dart';
import 'package:evans/src/utils/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_plugin_playlist/flutter_plugin_playlist.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'dart:convert';

class AudioAlbums extends StatefulWidget {
  final RmxAudioPlayer rmxAudioPlayer;

  const AudioAlbums({
    Key key,
    @required this.rmxAudioPlayer,
  }) : super(key: key);
  @override
  _AudioAlbumsState createState() => _AudioAlbumsState();
}

class _AudioAlbumsState extends State<AudioAlbums> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Audio Albums'),
      ),
      body: BlocBuilder<MusicBloc, MusicState>(
          builder: (BuildContext context, MusicState state) {
        if (state is LoadingMusicState) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        if (state is LoadedMusicState && state.musicAlbums.length != 0) {
          var albums = state.musicAlbums;
          return GridView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.vertical,
            gridDelegate:
                SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
            itemBuilder: (BuildContext context, int index) {
              return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Material(
                    elevation: 5,
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    child: Container(
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.bottomLeft,
                              end: Alignment.topRight,
                              stops: [
                                0,
                                1,
                                0
                              ],
                              colors: [
                                Colors.redAccent,
                                Colors.white,
                                Colors.red
                              ]),
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                          image: DecorationImage(
                              fit: BoxFit.fill,
                              image: CachedNetworkImageProvider(
                                  Constants.BASE_THUMBNAIL +
                                      albums[index]
                                          .albumImage
                                          .replaceAll(" ", "%20")))),
                      child: Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.black.withOpacity(0.5),
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                          ),
                          width: double.maxFinite,
                          height: double.maxFinite,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Material(
                                color: Colors.transparent,
                                child: IconButton(
                                    icon: Icon(
                                      FontAwesomeIcons.play,
                                      size: 30,
                                      color: Colors.white.withOpacity(0.7),
                                    ),
                                    onPressed: () {
                                      Navigator.of(context).push(
                                          MaterialPageRoute(
                                              builder: (_) => AudioSongs(
                                                  rmxAudioPlayer: this
                                                      .widget
                                                      .rmxAudioPlayer,
                                                  songs: albums[index].audios,
                                                  albumName: albums[index]
                                                      .albumTitle)));
//                                            NowPlaying(
//                                              musicListData:
//                                              audioTracks[
//                                              index],
//                                            )
                                    }),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 8.0, right: 8.0),
                                child: Text(
                                  albums[index].albumTitle,
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ));
            },
            itemCount: albums.length,
          );
        }
        return Container(
          child: Center(
            child: Text(
                "Servers are under maintainance. Please try again later.."),
          ),
        );
      }),
    );
  }
}
