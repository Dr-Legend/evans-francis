part of 'music_bloc.dart';

@immutable
abstract class MusicState {}

class InitialMusicState extends MusicState {}

@immutable
class ErrorMusicState extends MusicState {
  final DioError error;

  ErrorMusicState(this.error);
}

@immutable
class LoadedMusicState extends MusicState {
  final List<MusicAlbumData> musicAlbums;

  LoadedMusicState({this.musicAlbums});
}

@immutable
class LoadingMusicState extends MusicState {}
