import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:evans/src/api/api.dart';
import 'package:evans/src/models/music_album_data.dart';
import 'package:evans/src/utils/constants.dart';
import 'package:meta/meta.dart';

part 'music_event.dart';

part 'music_state.dart';

class MusicBloc extends Bloc<MusicEvent, MusicState> {
  ApiMethods apiMethods = new ApiMethods();
  @override
  MusicState get initialState => InitialMusicState();

  @override
  Stream<MusicState> mapEventToState(MusicEvent event) async* {
    if (event is LoadMusicEvent) {
      try {
        yield LoadingMusicState();
        var response = await apiMethods.requestInGet(Constants.MUSIC_ALBUM_API);
        List<MusicAlbumData> musicAlbums = musicAlbumDataFromJson(response);
        yield LoadedMusicState(musicAlbums: musicAlbums);
      } catch (e) {
        yield ErrorMusicState(e);
        print(e);
      }
    }
  }
}
