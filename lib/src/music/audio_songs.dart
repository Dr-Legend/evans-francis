import 'package:cached_network_image/cached_network_image.dart';
import 'package:evans/src/models/music_album_data.dart';
import 'package:evans/src/music/now_playing_screen.dart';
import 'package:evans/src/utils/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_plugin_playlist/flutter_plugin_playlist.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'dart:convert';

class AudioSongs extends StatefulWidget {
  final List<Audios> songs;
  final String albumName;
  final RmxAudioPlayer rmxAudioPlayer;
  const AudioSongs({
    Key key,
    @required this.songs,
    @required this.albumName,
    @required this.rmxAudioPlayer,
  }) : super(key: key);
  @override
  _AudioSongsState createState() => _AudioSongsState();
}

class _AudioSongsState extends State<AudioSongs> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.albumName),
      ),
      body: widget.songs.length != 0
          ? GridView.builder(
              shrinkWrap: true,
              scrollDirection: Axis.vertical,
              gridDelegate:
                  SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
              itemBuilder: (BuildContext context, int index) {
                return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Hero(
                      tag: widget.songs[index].audioTitle,
                      child: Material(
                        elevation: 5,
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        child: Container(
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                  begin: Alignment.bottomLeft,
                                  end: Alignment.topRight,
                                  stops: [
                                    0,
                                    1,
                                    0
                                  ],
                                  colors: [
                                    Colors.redAccent,
                                    Colors.white,
                                    Colors.red
                                  ]),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8)),
                              image: DecorationImage(
                                  fit: BoxFit.fill,
                                  image: CachedNetworkImageProvider(
                                      Constants.BASE_THUMBNAIL +
                                          "/" +
                                          widget.songs[index].image
                                              .replaceAll(" ", "%20")))),
                          child: Center(
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.black.withOpacity(0.5),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8)),
                              ),
                              width: double.maxFinite,
                              height: double.maxFinite,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Material(
                                    color: Colors.transparent,
                                    child: IconButton(
                                        icon: Icon(
                                          FontAwesomeIcons.play,
                                          size: 30,
                                          color: Colors.white.withOpacity(0.7),
                                        ),
                                        onPressed: () {
                                          Navigator.of(context).push(
                                              MaterialPageRoute(
                                                  builder: (_) =>
                                                      NowPlayingScreen(
                                                        rmxAudioPlayer: this
                                                            .widget
                                                            .rmxAudioPlayer,
                                                        selectedAlbum: this
                                                            .widget
                                                            .albumName,
                                                        selectedSongIndex:
                                                            '${this.widget.songs[index].id}',
                                                        songs: preparePlaylist(
                                                            index),
                                                      )));
                                        }),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 8.0, right: 8.0),
                                    child: Text(
                                      widget.songs[index].audioTitle,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ));
              },
              itemCount: widget.songs.length,
            )
          : Container(
              child: Center(
                child: Text("No Data.."),
              ),
            ),
    );
  }

  List<Audios> preparePlaylist(int index) {
    List<Audios> playList = [];
    var selectedSong = widget.songs[index];
    playList.add(widget.songs[index]);
    widget.songs.forEach((song) {
      if (selectedSong.audioTitle != song.audioTitle) {
        playList.add(song);
      }
    });
    return playList;
  }
}
