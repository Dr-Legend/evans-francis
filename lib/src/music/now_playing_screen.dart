import 'package:cached_network_image/cached_network_image.dart';
import 'package:evans/src/models/music_album_data.dart';
import 'package:evans/src/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_plugin_playlist/flutter_plugin_playlist.dart';

class NowPlayingScreen extends StatefulWidget {
  final List<Audios> songs;
  final String selectedAlbum;
  final String selectedSongIndex;
  final RmxAudioPlayer rmxAudioPlayer;
  const NowPlayingScreen(
      {Key key,
      @required this.songs,
      @required this.selectedAlbum,
      @required this.rmxAudioPlayer,
      @required this.selectedSongIndex})
      : super(key: key);
  @override
  _NowPlayingScreenState createState() => _NowPlayingScreenState();
}

class _NowPlayingScreenState extends State<NowPlayingScreen> {
  double _seeking;
  double _position = 0;

  int _current = 0;
  int _total = 0;

  String _status = 'none';

  @override
  void initState() {
    super.initState();

    this.widget.rmxAudioPlayer.on('status', (eventName, {dynamic args}) {
      print(eventName + (args ?? "").toString());

      if ((args as OnStatusCallbackData).value != null) {
        if (mounted) {
          setState(() {
            if ((args as OnStatusCallbackData).value['currentPosition'] !=
                null) {
              _current = (args as OnStatusCallbackData)
                  .value['currentPosition']
                  .toInt();
              _total = (((args as OnStatusCallbackData).value['duration']) ?? 0)
                  .toInt();
              _status = (args as OnStatusCallbackData).value['status'];

              if (_current > 0 && _total > 0) {
                _position = _current / _total;
              } else if (!this.widget.rmxAudioPlayer.isLoading &&
                  !this.widget.rmxAudioPlayer.isSeeking) {
                _position = 0;
              }

              if (_seeking != null &&
                  !this.widget.rmxAudioPlayer.isSeeking &&
                  !this.widget.rmxAudioPlayer.isLoading) {
                _seeking = null;
              }
            }
          });
        }
      }
    });
//    if (this.widget.rmxAudioPlayer.currentState != 'playing') {
    _prepare();
//    }
  }

  _prepare() async {
//    List<AudioTrack> playList = [];
//    this.widget.songs.forEach((track) {
//      playList.add(AudioTrack(
//          trackId: '${track.id}',
//          album: this.widget.selectedAlbum,
//          artist: '',
//          albumArt: Constants.WORSHIP_SONGS_THUMBNAILS + track.image,
//          assetUrl: Constants.WORSHIP_SONGS_TRACK + track.song,
//          title: track.name));
//    });
//    await this.widget.rmxAudioPlayer.setPlaylistItems(playList,
//        options: PlaylistItemOptions(
//            retainPosition: false,
//            playFromId: this.widget.selectedSongIndex !=
//                    this.widget.songs[0].id.toString()
//                ? this.widget.selectedSongIndex
//                : this.widget.songs[0].id));
//
//    await _play();
    if (this.widget.rmxAudioPlayer.currentTrack == null) {
      List<AudioTrack> playList = [];
      this.widget.songs.forEach((track) {
        playList.add(AudioTrack(
            trackId: '${track.id}',
            album: this.widget.selectedAlbum,
            artist: '',
            albumArt: Constants.BASE_THUMBNAIL + track.image,
            assetUrl: Constants.BASE_THUMBNAIL +
                track.audioFile.replaceAll(' ', '%20'),
            title: track.audioTitle));
      });
      await this.widget.rmxAudioPlayer.setPlaylistItems(playList,
          options: PlaylistItemOptions(retainPosition: false));

      await _play();
    } else if (this.widget.rmxAudioPlayer.currentTrack.album !=
        this.widget.selectedAlbum) {
      await _pause();
      this.widget.rmxAudioPlayer.clearAllItems();
      List<AudioTrack> playList = [];
      this.widget.songs.forEach((track) {
        playList.add(AudioTrack(
            trackId: '${track.id}',
            album: this.widget.selectedAlbum,
            artist: '',
            albumArt: Constants.BASE_THUMBNAIL + track.image,
            assetUrl: Constants.BASE_THUMBNAIL +
                track.audioFile.replaceAll(' ', '%20'),
            title: track.audioTitle));
      });
      await this.widget.rmxAudioPlayer.setPlaylistItems(playList,
          options: PlaylistItemOptions(retainPosition: false));
      await _play();
    } else {
      if (this.widget.selectedSongIndex.toString() !=
          this.widget.rmxAudioPlayer.currentTrack.trackId) {
        _playFromId(this.widget.selectedSongIndex);
      }
    }
  }

  _playFromId(String id) async {
    await this.widget.rmxAudioPlayer.playTrackById(id);
  }

  _addMore() async {
    await this.widget.rmxAudioPlayer.addItem(
          new AudioTrack(
              album: "Friends",
              artist: "Friends",
              albumArt:
                  "https://pjspaul.christianappdevelopers.com/api/uploads/admin/logo.jpg",
              assetUrl:
                  "https://pjspaul.christianappdevelopers.com/api/uploads/worship_songs/Main%20Yehu%20Se%20Pyaar%20Karta%20Hoon%204.mp3",
              title: "F.R.I.E.N.D.S"),
          index: 1,
        );
  }

  _play() async {
    await this.widget.rmxAudioPlayer.play().then((_) {
      setState(() {});
    });
  }

  _pause() {
    this.widget.rmxAudioPlayer.pause().then((_) {
      print(_);
      setState(() {});
    }).catchError(print);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ChurchAppColors.brandBackground,
      appBar: AppBar(
        backgroundColor: ChurchAppColors.brandBackground,
        title: Text(this.widget.selectedAlbum),
      ),
      body: Center(
        child: _body(),
      ),
    );
  }

  Widget _body() {
    return _player();
  }

  Widget _player() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 15, horizontal: 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Container(
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                  // Box decoration takes a gradient

                  ),
              child: Center(
                child: Container(
                  height: 250,
                  width: 250,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(50),
                          bottomRight: Radius.circular(50)),
                      boxShadow: [
                        BoxShadow(
                          color: ChurchAppColors.golden.withOpacity(0.6),
                          blurRadius: 250,
                        )
                      ],
                      image: DecorationImage(
                          fit: BoxFit.fill,
                          image: this
                                      .widget
                                      .rmxAudioPlayer
                                      .currentTrack
                                      ?.albumArt !=
                                  null
                              ? CachedNetworkImageProvider(this
                                  .widget
                                  .rmxAudioPlayer
                                  .currentTrack
                                  .albumArt)
                              : AssetImage('assets/images/brand.png'))),
                ),
              ),
            ),
          ),
          Center(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                this.widget.rmxAudioPlayer.currentTrack?.title ?? '...',
                style: TextStyle(
                    color: ChurchAppColors.golden,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 2,
                    fontSize: 18),
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              new Text(
                _format(_current),
                style: TextStyle(color: ChurchAppColors.golden),
              ),
              new Text(
                _format(_total),
                style: TextStyle(color: ChurchAppColors.golden),
              )
            ],
          ),
          Slider(
            activeColor: ChurchAppColors.golden,
            inactiveColor: Colors.yellow[50],
            value: _seeking ?? _position,
            onChangeEnd: (val) async {
              if (_total > 0) {
                await this.widget.rmxAudioPlayer.seekTo(val * _total);
              }
            },
            onChanged: (val) {
              if (_total > 0) {
                setState(() {
                  _seeking = val;
                });
              }
            },
          ),
          Material(
            color: ChurchAppColors.brandBackground,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                IconButton(
                  onPressed: this.widget.rmxAudioPlayer.skipBack,
                  icon: Icon(
                    Icons.skip_previous,
                    color: ChurchAppColors.golden,
                  ),
                ),
                FloatingActionButton(
                  heroTag: "Play-pause",
                  onPressed: _onPressed(),
                  child: _icon(),
                ),
                IconButton(
                  onPressed: this.widget.rmxAudioPlayer.skipForward,
                  icon: Icon(
                    Icons.skip_next,
                    color: ChurchAppColors.golden,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  String _format(int secs) {
    int sec = secs;

    int min = 0;
    if (secs > 60) {
      min = (sec / 60).floor();
      sec = sec % 60;
    }

    return (min >= 10 ? min.toString() : '0' + min.toString()) +
        ":" +
        (sec >= 10 ? sec.toString() : '0' + sec.toString());
  }

  _onPressed() {
    if (this.widget.rmxAudioPlayer.isLoading ||
        this.widget.rmxAudioPlayer.isSeeking) return null;

    if (this.widget.rmxAudioPlayer.isPlaying) return _pause;

    return _play;
  }

  Widget _icon() {
    if (this.widget.rmxAudioPlayer.isLoading ||
        this.widget.rmxAudioPlayer.isSeeking) {
      return CircularProgressIndicator(
        backgroundColor: Colors.white,
      );
    }

    if (this.widget.rmxAudioPlayer.isPlaying) {
      return const Icon(
        Icons.pause,
        size: 40,
      );
    }
    if (this.widget.rmxAudioPlayer.isStopped) {
      return const Icon(
        Icons.stop,
        size: 40,
      );
    }

    return const Icon(
      Icons.play_arrow,
      size: 40,
    );
  }
}
