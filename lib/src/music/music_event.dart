part of 'music_bloc.dart';

@immutable
abstract class MusicEvent {}

class LoadMusicEvent extends MusicEvent {}
