import 'dart:async';

import 'package:evans/src/models/pillar_of_cloud_data.dart';
import 'package:evans/src/old_dev_non_bloc/presenters/common_presenter.dart';
import 'package:evans/src/old_dev_non_bloc/utils/constants.dart';
import 'package:evans/src/old_dev_non_bloc/utils/enum_pages.dart';
import 'package:evans/src/old_dev_non_bloc/utils/utility.dart';
import 'package:evans/src/old_dev_non_bloc/views/common_view.dart';
import 'package:flutter/material.dart';

class CustomDataLoadPage extends StatefulWidget {
  final pageName;

  const CustomDataLoadPage({Key key, this.pageName}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _PageState(pageName);
  }
}

class _PageState extends State<CustomDataLoadPage> implements View {
  Presenter _presenter;
  List<PillarOfCloudData> _listData;
  String text = '';
  String apiUrl;
  PageName _pageName;

  _PageState(pageName) {
    _presenter = new Presenter(this);
    _listData = new List<PillarOfCloudData>();
    _pageName = pageName;
  }

  ///Call the API here
  @override
  void initState() {
    super.initState();
    setState(() {
      Utility.check().then((internet) {
        if (internet != null && internet) {
          Future.delayed(Duration.zero,
              () => Utility.showProgressDialog(Constants.PLEASE_WAIT, context));
          _presenter.makeGetCall(apiUrl, _pageName);
        } else {
          Utility.showToast(Constants.NO_INTERNET_FOUND, context);
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(getTitle()),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Expanded(
              child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: ListView(
              children: <Widget>[Text(text)],
            ),
          )),
          /*ButtonWidget(
            text: Text(
              'Become a Pillar',
              style: TextStyle(color: Colors.white),
            ),
            onPressed: () {},
            padding: EdgeInsets.all(12.0),
          ),*/
        ],
      ),
    );
  }

  @override
  onFailed(Exception onError) {
    Utility.dismissProgress(context);
    Utility.showToast(Constants.SOMETHING_WENT, context);
  }

  @override
  onSuccess(String data, PageName pageName) {
    Utility.dismissProgress(context);
    setState(() {
      _listData = pillarOfCloudDataFromJson(data);
      if (_listData.length > 0) {
        text = _listData[0].content.replaceAll('&nbsp;', '');
        text = text.replaceAll('&ldquo;', '');
        text = text.replaceAll('&rdquo;', '');
        text = text.replaceAll('&rsquo;', '');
        text = text.replaceAll('&#39;', '');
      }
    });
  }

  String getTitle() {
    if (_pageName == PageName.LETTER_FROM_CEO) {
      apiUrl = Constants.LETTER_OF_CEO;
      return 'Letter Of CEO';
    } else if (_pageName == PageName.MISSION) {
      apiUrl = Constants.MISSION;
      return 'Our Mission';
    } else if (_pageName == PageName.VISION) {
      apiUrl = Constants.VISION;
      return 'Our Vision';
    } else {
      apiUrl = Constants.STATEMENT_OF_FAITH;
      return 'Statement of Faith';
    }
  }
}
