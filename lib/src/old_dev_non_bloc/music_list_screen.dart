//import 'dart:async';
//
//import 'package:cached_network_image/cached_network_image.dart';
//import 'package:evans/src/models/music_album_data.dart';
//import 'package:evans/src/models/music_list_data.dart';
//import 'package:evans/src/old_dev_non_bloc/music_play_screen.dart';
//import 'package:flutter/material.dart';
//import 'package:evans/src/old_dev_non_bloc/presenters/common_presenter.dart';
//import 'package:evans/src/old_dev_non_bloc/utils/constants.dart';
//import 'package:evans/src/old_dev_non_bloc/utils/enum_pages.dart';
//import 'package:evans/src/old_dev_non_bloc/utils/utility.dart';
//import 'package:evans/src/old_dev_non_bloc/views/common_view.dart';
//
//class MusicList extends StatefulWidget {
//  final MusicAlbumData data;
//
//  const MusicList({Key key, this.data}) : super(key: key);
//
//  @override
//  State<StatefulWidget> createState() {
//    // TODO: implement createState
//    return _MusicStateList(data);
//  }
//}
//
//class _MusicStateList extends State<MusicList> implements View {
//  MusicAlbumData _musicAlbumData;
//  List<MusicListData> _listData;
//  Presenter _presenter;
//
//  _MusicStateList(MusicAlbumData data) {
//    _musicAlbumData = data;
//    _presenter = new Presenter(this);
//    _listData = new List<MusicListData>();
//  }
//
//  ///Call the API here
//  @override
//  void initState() {
//    super.initState();
//    setState(() {
//      Utility.check().then((internet) {
//        if (internet != null && internet) {
//          Future.delayed(Duration.zero,
//              () => Utility.showProgressDialog(Constants.PLEASE_WAIT, context));
//          _presenter.makeGetCall(
//              Constants.MUSIC_LIST_API + _musicAlbumData.id.toString(),
//              PageName.MUSIC);
//        } else {
//          Utility.showToast(Constants.NO_INTERNET_FOUND, context);
//        }
//      });
//    });
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    // TODO: implement build
//    return Scaffold(
//        appBar: AppBar(
//          title: Text(_musicAlbumData.name),
//        ),
//        body: GridView.count(
//          crossAxisCount: 3,
//          children: List.generate(_listData.length, (index) {
//            return _gridItems(index);
//          }),
//        )
//
//        /* ListView.builder(
//        itemCount: _listData.length,
//        itemBuilder: (BuildContext context, int index) {
//          return _listViewItem(index);
//        },*/
//        );
//  }
//
//  Widget _gridItems(int index) {
//    var size = MediaQuery.of(context).size;
//    final double itemHeight = (size.height - kToolbarHeight - 24) / 3;
//    final double itemWidth = size.width / 3;
//    return Padding(
//      padding: const EdgeInsets.only(left: 4.0, right: 4.0),
//      child: Center(
//        child: Card(
//          elevation: 5,
//          child: Column(
//            crossAxisAlignment: CrossAxisAlignment.end,
//            mainAxisAlignment: MainAxisAlignment.end,
//            children: <Widget>[
//              Expanded(
//                  child: Stack(
//                children: <Widget>[
//                  Padding(
//                    padding: const EdgeInsets.only(top: 4.0),
//                    child: Container(
//                      height: itemHeight,
//                      width: itemWidth,
//                      child: CachedNetworkImage(
//                        fit: BoxFit.fill,
//                        imageUrl: Constants.MUSIC_LIST_THUMBNAIL +
//                            _listData[index].poster,
//                        placeholder: (context, url) => Image.asset(
//                          'images/ic_placeholder.png',
//                          fit: BoxFit.fill,
//                        ),
//                        errorWidget: (context, url, error) => Image.asset(
//                            'images/ic_placeholder.png',
//                            fit: BoxFit.fill,
//                            width: itemWidth),
//                      ),
//                    ),
//                  ),
//                  InkWell(
//                    child: Container(
//                      height: itemHeight,
//                      width: itemWidth,
//                      padding: EdgeInsets.all(20.0),
//                      child: Column(
//                        crossAxisAlignment: CrossAxisAlignment.center,
//                        mainAxisAlignment: MainAxisAlignment.center,
//                        children: <Widget>[
//                          Icon(
//                            Icons.play_arrow,
//                            size: 45,
//                            color: Colors.white,
//                          )
//                        ],
//                      ),
//                    ),
//                    onTap: () {
//                      Navigator.push(
//                          context,
//                          MaterialPageRoute(
//                              builder: (context) => MusicPlayScreen(
//                                    musicListData: _listData[index],
//                                  )));
//                    },
//                  ),
//                ],
//              )),
//              Padding(
//                padding: const EdgeInsets.all(4.0),
//                child: Center(
//                    child: Text(
//                  _listData[index].name,
//                  maxLines: 1,
//                )),
//              ),
//            ],
//          ),
//        ),
//      ),
//    );
//  }
//
//  @override
//  onFailed(Exception onError) {
//    Utility.dismissProgress(context);
//    Utility.showToast(Constants.SOMETHING_WENT, context);
//  }
//
//  @override
//  onSuccess(String data, PageName pageName) {
//    Utility.dismissProgress(context);
//    setState(() {
//      try {
//        _listData = musicListDataFromJson(data);
//      } on TypeError {
//        Utility.showToast(Constants.NO_DATA, context);
//      }
//    });
//  }
//}
