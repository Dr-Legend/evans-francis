import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class WebViewScreen extends StatelessWidget {
  final String title, url;

  const WebViewScreen({Key key, this.title, this.url}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new WebviewScaffold(
      url: url,
      withZoom: true,
      appBar: AppBar(
        title: Text(title),
      ),
      withLocalStorage: true,
      initialChild: Container(
        child: Center(child: new CircularProgressIndicator()),
      ),
    );
  }
}
