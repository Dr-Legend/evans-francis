import 'dart:async';

import 'package:evans/src/models/video_model.dart';
import 'package:evans/src/old_dev_non_bloc/presenters/common_presenter.dart';
import 'package:evans/src/old_dev_non_bloc/utils/constants.dart';
import 'package:evans/src/old_dev_non_bloc/utils/enum_pages.dart';
import 'package:evans/src/old_dev_non_bloc/utils/utility.dart';
import 'package:evans/src/old_dev_non_bloc/views/common_view.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_youtube/flutter_youtube.dart';

class VideoShowCase extends StatefulWidget {
  final PageName pageName;

  const VideoShowCase({Key key, this.pageName}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _VideoState(pageName);
  }
}

///State for VideoShowCase Class

class _VideoState extends State<VideoShowCase> implements View {
  Presenter _presenter;
  List<VideoData> _videoDataList;
  var youtube = new FlutterYoutube();
  PageName _pageName;
  int pageNumber = 1;

  _VideoState(PageName pageName) {
    _presenter = new Presenter(this);
    _videoDataList = new List<VideoData>();
    _pageName = pageName;
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: _videoDataList.length,
      itemBuilder: (BuildContext context, int index) {
        return _listViewItem(index);
      },
    );
  }

  ///Call the API here
  @override
  void initState() {
    super.initState();
    setState(() {
      Utility.check().then((internet) {
        if (internet != null && internet) {
          Future.delayed(Duration.zero,
              () => Utility.showProgressDialog(Constants.PLEASE_WAIT, context));
          _presenter.makeGetCall(
              Constants.VIDEO_API + pageNumber.toString(), PageName.LIVE_VIDEO);
        } else {
          Utility.showToast(Constants.NO_INTERNET_FOUND, context);
        }
      });
    });
  }

  ///ListView Item layout
  Widget _listViewItem(int index) {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: InkWell(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              height: 220,
              width: MediaQuery.of(context).size.width,
              child: CachedNetworkImage(
                fit: BoxFit.fill,
                imageUrl: 'http://img.youtube.com/vi/' +
                    _videoDataList[index].youtubeVideoId +
                    '/0.jpg',
                // Constants.VIDEO_THUMBNAIL + _videoDataList[index].thumbnail,
                placeholder: (context, url) => Image.asset(
                    'images/ic_placeholder.png',
                    height: 220,
                    fit: BoxFit.fill,
                    width: MediaQuery.of(context).size.width),
                errorWidget: (context, url, error) => Image.asset(
                    'images/ic_placeholder.png',
                    height: 220,
                    fit: BoxFit.fill,
                    width: MediaQuery.of(context).size.width),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0, left: 8.0),
              child: Text(
                _videoDataList[index].title,
                maxLines: 8,
                style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 1),
              ),
            ),
            SizedBox(
              height: 8,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 8.0, bottom: 8.0),
              child: Text(
                _videoDataList[index].createdAt,
                style: TextStyle(fontSize: 12.0),
              ),
            ),
          ],
        ),
        onTap: () {
          //Navigator.push(context, MaterialPageRoute(builder: (context) =>Test()));
          playYoutubeVideo(_videoDataList[index].youtubeVideoId);
        },
      ),
    );
  }

  void playYoutubeVideo(String youtubevideoid) {
    FlutterYoutube.playYoutubeVideoByUrl(
      apiKey: Constants.YOUTUBE_APP_KEY,
      videoUrl: "https://www.youtube.com/watch?v=" + youtubevideoid,
    );
  }

  @override
  onFailed(Exception onError) {
    Utility.dismissProgress(context);
    Utility.showToast(Constants.SOMETHING_WENT, context);
  }

  @override
  onSuccess(String data, PageName pageName) {
    Utility.dismissProgress(context);
    setState(() {
      try {
        _videoDataList = videoDataFromJson(data);
      } on TypeError {
        Utility.showToast('No Data Found!', context);
      }
    });
  }

  String getTitle() {
    if (_pageName == PageName.WORD_EXPLOSION) {
      pageNumber = 1;
      return 'Word Explosion';
    } else if (_pageName == PageName.MIRACLE) {
      pageNumber = 3;
      return 'Miracle Tranformation';
    } else if (_pageName == PageName.EYE_TO_EYE) {
      pageNumber = 4;
      return 'Eye To Eye';
    } else if (_pageName == PageName.MELODIES) {
      pageNumber = 5;
      return 'Melodies For the Master';
    } else {
      pageNumber = 2;
      return 'Rock Of Life';
    }
  }
}
