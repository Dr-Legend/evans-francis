import 'package:evans/src/old_dev_non_bloc/utils/enum_pages.dart';

///Abstract class that define all the methods used in
///Login widget
abstract class View {
  /// this method is called on Login Success
  onSuccess(String data, PageName pageName);

  ///This method is called on Login failed
  onFailed(Exception onError);
}
