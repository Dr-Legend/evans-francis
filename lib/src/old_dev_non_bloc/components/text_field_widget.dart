import 'package:flutter/material.dart';

///This class create a edit text with text input layout
class EditTextWidget extends StatelessWidget {
  ///TextEditingController
  final TextEditingController controller;

  /// Label Text
  final String labelText;

  ///Error Text
  final String errorText;

  ///Is obscureText
  final bool isObscureText;

  ///Validate Variable
  final bool errorValidation;

  ///Keyboard type
  final TextInputType keyboardType;
  final int maxLines;
  const EditTextWidget(
      {Key key,
      this.controller,
      this.labelText,
      this.errorText,
      this.isObscureText,
      this.errorValidation,
      this.keyboardType, this.maxLines})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: new TextField(
        keyboardType: keyboardType,
        maxLines: maxLines,
        controller: controller,
        decoration: new InputDecoration(
            labelText: labelText,
            errorText: !errorValidation ? errorText : null),
        obscureText: isObscureText,
      ),
    );
  }
}
