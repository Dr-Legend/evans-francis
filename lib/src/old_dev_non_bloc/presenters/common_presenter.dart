import 'dart:convert';

import 'package:evans/src/api/api.dart';
import 'package:evans/src/old_dev_non_bloc/utils/enum_pages.dart';
import 'package:evans/src/old_dev_non_bloc/views/common_view.dart';

///Presenter class for Login Widget
///All the logic part for widget will goes here
class Presenter {
  View _view;

  ///Pass view object in presenter
  Presenter(this._view);

  ApiMethods api = new ApiMethods();

  ///This method will make the ApI call in GET Method
  makeGetCall(String url, PageName pageName) {
    //  _view.showProgressDialog();
    if (url != null) {
      api.requestInGet(url).then((onValue) {
        _view.onSuccess(onValue.toString(), pageName);
      }).catchError((onError) {
        print("Error is " + onError.toString());
        _view.onFailed(onError);
      });
    }
  }
}
