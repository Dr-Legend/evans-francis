//import 'dart:async';
//
//import 'package:evans/src/models/music_album_data.dart';
//import 'package:evans/src/old_dev_non_bloc/music_list_screen.dart';
//import 'package:evans/src/old_dev_non_bloc/presenters/common_presenter.dart';
//import 'package:evans/src/old_dev_non_bloc/utils/constants.dart';
//import 'package:evans/src/old_dev_non_bloc/utils/enum_pages.dart';
//import 'package:evans/src/old_dev_non_bloc/utils/utility.dart';
//import 'package:evans/src/old_dev_non_bloc/views/common_view.dart';
//import 'package:flutter/material.dart';
//import 'package:cached_network_image/cached_network_image.dart';
//
//class MusicAlbumPage extends StatefulWidget {
//  @override
//  State<StatefulWidget> createState() {
//    // TODO: implement createState
//    return _MusicAlbumPageState();
//  }
//}
//
/////State for VideoShowCase Class
//
//class _MusicAlbumPageState extends State<MusicAlbumPage> implements View {
//  Presenter _presenter;
//  List<MusicAlbumData> _dataList;
//
//  _MusicAlbumPageState() {
//    _presenter = new Presenter(this);
//    _dataList = new List<MusicAlbumData>();
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return GridView.count(
//      crossAxisCount: 3,
//      children: List.generate(_dataList.length, (index) {
//        return _gridItems(index);
//      }),
//    );
//  }
//
//  Widget _gridItems(int index) {
//    var size = MediaQuery.of(context).size;
//    final double itemHeight = (size.height - kToolbarHeight - 24) / 3;
//    final double itemWidth = size.width / 3;
//    return InkWell(
//      child: Padding(
//        padding: const EdgeInsets.only(left: 4.0, right: 4.0),
//        child: Center(
//          child: Card(
//            elevation: 5,
//            child: Column(
//              crossAxisAlignment: CrossAxisAlignment.end,
//              mainAxisAlignment: MainAxisAlignment.end,
//              children: <Widget>[
//                Expanded(
//                    child: Stack(
//                  children: <Widget>[
//                    Padding(
//                      padding: const EdgeInsets.only(top: 4.0),
//                      child: Container(
//                        height: itemHeight,
//                        width: itemWidth,
//                        child: CachedNetworkImage(
//                          fit: BoxFit.fill,
//                          imageUrl: Constants.MUSIC_ALBUM_THUMBNAIL +
//                              _dataList[index].image,
//                          placeholder: (context, url) => Image.asset(
//                            'images/ic_placeholder.png',
//                            fit: BoxFit.fill,
//                          ),
//                          errorWidget: (context, url, error) => Image.asset(
//                              'images/ic_placeholder.png',
//                              fit: BoxFit.fill,
//                              width: itemWidth),
//                        ),
//                      ),
//                    ),
//                  ],
//                )),
//                Padding(
//                  padding: const EdgeInsets.all(4.0),
//                  child: Center(
//                      child: Text(
//                    _dataList[index].name,
//                    maxLines: 1,
//                  )),
//                ),
//              ],
//            ),
//          ),
//        ),
//      ),
//      onTap: () {
//        Navigator.push(
//            context,
//            MaterialPageRoute(
//                builder: (context) => MusicList(data: _dataList[index])));
//      },
//    );
//  }
//
//  ///Call the API here
//  @override
//  void initState() {
//    super.initState();
//    setState(() {
//      Utility.check().then((internet) {
//        if (internet != null && internet) {
//          Future.delayed(Duration.zero,
//              () => Utility.showProgressDialog(Constants.PLEASE_WAIT, context));
//          _presenter.makeGetCall(
//              Constants.MUSIC_ALBUM_API, PageName.LIVE_VIDEO);
//        } else {
//          Utility.showToast(Constants.NO_INTERNET_FOUND, context);
//        }
//      });
//    });
//  }
//
//  ///ListView Item layout
//  Widget _listViewItem(int index) {
//    return InkWell(
//      child: Padding(
//        padding: const EdgeInsets.only(bottom: 2.0),
//        child: Stack(
//          children: <Widget>[
//            Container(
//              padding: EdgeInsets.only(left: 10.0),
//              height: 220,
//              width: MediaQuery.of(context).size.width,
//              child: CachedNetworkImage(
//                fit: BoxFit.fill,
//                imageUrl:
//                    Constants.MUSIC_ALBUM_THUMBNAIL + _dataList[index].image,
//                placeholder: (context, url) => Image.asset(
//                    'images/ic_placeholder.png',
//                    height: 220,
//                    fit: BoxFit.fill,
//                    width: MediaQuery.of(context).size.width),
//                errorWidget: (context, url, error) => Image.asset(
//                    'images/ic_placeholder.png',
//                    height: 220,
//                    fit: BoxFit.fill,
//                    width: MediaQuery.of(context).size.width),
//              ),
//            ),
//            Container(
//              height: 220,
//              padding: EdgeInsets.all(20.0),
//              width: MediaQuery.of(context).size.width,
//              decoration: BoxDecoration(color: Color.fromRGBO(0, 0, 0, .7)),
//              child: Column(
//                crossAxisAlignment: CrossAxisAlignment.center,
//                mainAxisAlignment: MainAxisAlignment.center,
//                children: <Widget>[
//                  Text(
//                    _dataList[index].name,
//                    style: TextStyle(
//                        fontSize: 25,
//                        fontWeight: FontWeight.bold,
//                        color: Colors.white),
//                  ),
//                  Padding(
//                    padding: const EdgeInsets.all(8.0),
//                    child: Text(
//                      "(" + _dataList[index].totalRecord.toString() + " Audio)",
//                      style: TextStyle(color: Colors.white),
//                    ),
//                  )
//                ],
//              ),
//            ),
//          ],
//        ),
//      ),
//      onTap: () {
//        Navigator.push(
//            context,
//            MaterialPageRoute(
//                builder: (context) => MusicList(data: _dataList[index])));
//      },
//    );
//  }
//
//  @override
//  onFailed(Exception onError) {
//    Utility.dismissProgress(context);
//    Utility.showToast(Constants.SOMETHING_WENT, context);
//  }
//
//  @override
//  onSuccess(String data, PageName pageName) {
//    Utility.dismissProgress(context);
//    setState(() {
//      try {
//        _dataList = musicAlbumDataFromJson(data);
//      } on TypeError {
//        Utility.showToast(Constants.NO_DATA, context);
//      }
//    });
//  }
//}
