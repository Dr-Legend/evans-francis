import 'package:cached_network_image/cached_network_image.dart';
import 'package:evans/src/models/blog_data.dart';
import 'package:evans/src/old_dev_non_bloc/utils/constants.dart';

import 'package:flutter/material.dart';

class BlogDetails extends StatelessWidget {
  final BlogData blogData;

  const BlogDetails({Key key, this.blogData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(blogData.title),
      ),
      body: ListView(
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width,
                height: 280,
                child: CachedNetworkImage(
                  fit: BoxFit.fill,
                  imageUrl: Constants.BLOG_THUMBNAIL + blogData.image,
                  placeholder: (context, url) => Image.asset(
                      'images/ic_placeholder.png',
                      height: 200,
                      fit: BoxFit.fill,
                      width: MediaQuery.of(context).size.width),
                  errorWidget: (context, url, error) => Image.asset(
                      'images/ic_placeholder.png',
                      height: 200,
                      fit: BoxFit.fill,
                      width: MediaQuery.of(context).size.width),
                ),
              ),
              Text(blogData.description),
            ],
          )
        ],
      ),
    );
  }
}
