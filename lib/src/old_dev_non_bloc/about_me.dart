import 'dart:async';

import 'package:evans/src/models/about_data.dart';
import 'package:evans/src/old_dev_non_bloc/presenters/common_presenter.dart';
import 'package:evans/src/old_dev_non_bloc/utils/constants.dart';
import 'package:evans/src/old_dev_non_bloc/utils/enum_pages.dart';
import 'package:evans/src/old_dev_non_bloc/utils/utility.dart';
import 'package:evans/src/old_dev_non_bloc/views/common_view.dart';
import 'package:flutter/material.dart';

class AboutMePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _AboutMeState();
  }
}

class _AboutMeState extends State<AboutMePage> implements View {
  Presenter _presenter;
  List<AboutData> _listData;
  String text = '';

  _AboutMeState() {
    _presenter = new Presenter(this);
    _listData = new List<AboutData>();
  }

  ///Call the API here
  @override
  void initState() {
    super.initState();
    setState(() {
      Utility.check().then((internet) {
        if (internet != null && internet) {
          Future.delayed(Duration.zero,
              () => Utility.showProgressDialog(Constants.PLEASE_WAIT, context));
          _presenter.makeGetCall(Constants.ABOUT_API, PageName.EYE_TO_EYE);
        } else {
          Utility.showToast(Constants.NO_INTERNET_FOUND, context);
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('About Us'),
      ),
      body: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 8.0, left: 8.0, right: 8.0),
            child: Container(
              height: 350,
              width: MediaQuery.of(context).size.width,
              child: Center(
                child: Image.asset(
                  'images/rbn_logo.png',
                  fit: BoxFit.contain,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 12.0, right: 12.0),
            child: Text(
              text,
              textAlign: TextAlign.justify,
              style: TextStyle(fontSize: 18.0, letterSpacing: 1),
            ),
          ),
        ],
      ),
    );
  }

  @override
  onFailed(Exception onError) {
    Utility.showToast(Constants.SOMETHING_WENT, context);
  }

  @override
  onSuccess(String data, PageName pageName) {
    Utility.dismissProgress(context);
    setState(() {
      _listData = aboutDataFromJson(data);
      if (_listData.length > 0) {
        text = _listData[0].content.replaceAll('&nbsp;', '');
        text = text.replaceAll('&ldquo;', '');
        text = text.replaceAll('&rdquo;', '');
        text = text.replaceAll('&rsquo;', '');
        text = text.replaceAll('&#39;', '');
        text = text.replaceAll('&quot;', '');
      }
    });
  }
}
