import 'dart:async';

import 'package:evans/src/old_dev_non_bloc/presenters/common_presenter.dart';
import 'package:evans/src/old_dev_non_bloc/utils/constants.dart';
import 'package:evans/src/old_dev_non_bloc/utils/enum_pages.dart';
import 'package:evans/src/old_dev_non_bloc/utils/utility.dart';
import 'package:evans/src/old_dev_non_bloc/video_page.dart';
import 'package:evans/src/old_dev_non_bloc/views/common_view.dart';
import 'package:flutter/material.dart';
import 'package:banner_view/banner_view.dart';

///This is the home page
class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _HomePageState();
  }
}

///THis class represent to Home page State
class _HomePageState extends State<HomePage> implements View {
  Presenter _presenter;
  var imagesName = [
    'images/icon_live_video.png',
    'images/icon_word_explosion.png',
    'images/icon_miracle.png',
    'images/icon_eye.png',
    'images/icon_melodies.png',
    'images/icon_rock.png',
  ];
  var serviceName = [
    'Live',
    'Word Explosion',
    'Miracle Transformation',
    'Eye To Eye',
    'Melodies For the Master',
    'Rock Of Life',
  ];

  _HomePageState() {
    _presenter = new Presenter(this);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: <Widget>[
        Container(
            color: Colors.white,
            height: 220.0,
            child: BannerView(
              [
                Container(
                    child: Image.asset('images/slider1.png', fit: BoxFit.fill)),
                Container(
                    child: Image.asset('images/slider2.png', fit: BoxFit.fill)),
                Container(
                    child: Image.asset('images/slider3.png', fit: BoxFit.fill)),
                Container(
                    child: Image.asset('images/slider4.png', fit: BoxFit.fill)),
                Container(
                    child: Image.asset('images/slider5.png', fit: BoxFit.fill)),
                Container(
                    child: Image.asset('images/slider6.png', fit: BoxFit.fill)),
                Container(
                    child: Image.asset('images/slider7.png', fit: BoxFit.fill)),
              ],
              intervalDuration: Duration(
                  days: 0,
                  hours: 0,
                  microseconds: 0,
                  milliseconds: 0,
                  minutes: 0,
                  seconds: 3),
              log: false,
            )),
        Expanded(
          child: GridView.builder(
              itemCount: 6,
              gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  mainAxisSpacing: 0.0,
                  crossAxisSpacing: 0.0,
                  childAspectRatio: 1.0),
              itemBuilder: (BuildContext context, int index) {
                return _gridItems(index);
              }),
        ),
      ],
    );
  }

  Widget _gridItems(int index) {
    return new InkWell(
      child: Container(
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: SizedBox(
                height: 90,
                width: 90,
                child: Image.asset(
                  imagesName[index],
                  fit: BoxFit.fill,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: SizedBox(
                child: Text(
                  serviceName[index],
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontWeight: FontWeight.w300,
                      color: Colors.black,
                      fontSize: 15),
                ),
              ),
            ),
          ],
        ),
      ),
      onTap: () {
        _clickEvents(index);
      },
    );
  }

  ///Handle the click events here accordingly to item position
  void _clickEvents(int index) {
    switch (index) {
      case 0:
        {
          Utility.showProgressDialog(Constants.PLEASE_WAIT, context);
          _presenter.makeGetCall(Constants.VIDEO_API, PageName.LIVE_VIDEO);
          break;
        }
      case 1:
        {
          ///Move to Blog Page
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => VideoShowCase(
                        pageName: PageName.WORD_EXPLOSION,
                      )));
          break;
        }
      case 2:
        {
          ///Move to Music Album Page
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      VideoShowCase(pageName: PageName.MIRACLE)));
          break;
        }
      case 3:
        {
          ///Move to Mission Album Page
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      VideoShowCase(pageName: PageName.EYE_TO_EYE)));
          break;
        }
      case 4:
        {
          ///Move to Upcoming  Page
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      VideoShowCase(pageName: PageName.MELODIES)));
          break;
        }
      case 5:
        {
          ///Move to Statement of  Page
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      VideoShowCase(pageName: PageName.ROCK_IF_LIFE)));
          break;
        }
      case 6:
        {
          ///Move to Statement of  Page
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      VideoShowCase(pageName: PageName.WORD_EXPLOSION)));
          break;
        }
    }
  }

  @override
  onFailed(Exception onError) {
    Utility.dismissProgress(context);
    Utility.showAlertDialog(
        context, 'No Live streaming found', 'Alert!', 'Dismiss');
  }

  @override
  onSuccess(String data, PageName pageName) {
    Utility.dismissProgress(context);
    Utility.showAlertDialog(
        context, 'No Live streaming found', 'Alert!', 'Dismiss');
  }
}
