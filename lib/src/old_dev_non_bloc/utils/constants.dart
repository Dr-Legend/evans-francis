///This class represents all the constants methods and variables
class Constants {
  static const String ABOUT_EVAN =
      "An evangelist, author, teacher and lyricist with uncompromising faithfulness to the Holy Scriptures, Evans brings clarity and a message of necessity to Christians to uncover the unseen riches in God’s word." +
          "Born in a village called Mukerian, Punjab, India in 1988, Evans began his evangelistic career at a very young age of nineteen. In spite of many hurdles, challenges and obstacles Evans did not deviate from his work but continued to follow the path, which God gave him through vision and calling. Belonging to a family of meagre means Evans, at a very young age of twenty-two established an NGO called TWCO [Together We Can Organization], which is registered under the government of India. \n" +
          "Over the years, Evans has established churches, taught, preached the word of God and brought many souls to the kingdom of God. Evans has written and composed a number of Biblical songs and authored a few books, which are yet to be published and he provides daily devotion for a solid spiritual foundation to many people as well." +
          "Evans uncompromisingly stresses on correct interpretation, understanding and application of the Word of God as it should be applied by each individual being a New Testament believer.";
  static const String BASE_URL =
      'http://webinnovativetechnology.com/rapture/api/';
  static const String BLOG_API = BASE_URL + 'blog.php';
  static const String UPCOMING_EVENT_API = BASE_URL + 'event.php';
  static const String MUSIC_ALBUM_API = BASE_URL + 'audio_album_list.php';
  static const String ABOUT_API = BASE_URL + 'about_us.php';
  static const String REGISTRATION = BASE_URL + 'deviceId.php?deviceId=';
  static const String LETTER_OF_CEO = BASE_URL + 'letter_of_ceo.php';
  static const String MISSION = BASE_URL + 'our_mission.php';
  static const String VISION = BASE_URL + 'our_vision.php';
  static const String STATEMENT_OF_FAITH = BASE_URL + 'statement_of_faith.php';
  static const String NO_DATA = 'No Data Found!';
  static const String MISSIONS_API = BASE_URL + 'mission.php';
  static const String GALLERY_LIST_API = BASE_URL + "gallery_images.php?id=";
  static const String MUSIC_LIST_API =
      BASE_URL + 'audio_album_track_list.php?id=';
  static const String VIDEO_API = BASE_URL + 'youtube_video.php?category_id=';
  static const String TRACK_URL =
      "http://webinnovativetechnology.com/rapture/uploaded_files/track_list/";
  static const String GALLERY_ALBUM_API = BASE_URL + 'gallery_list.php';
  static const String FAITH_API = BASE_URL + 'faith.php';
  static const String INVITE_API = BASE_URL + 'invite_form_request.php?';
  static const String PILLAR_CLOUD_API = BASE_URL + 'pillar-of-cloud';
  static const String PILLAR_FIRE_API = BASE_URL + 'pillar-of-fire';
  static const String MESSAGE_API = BASE_URL + 'message.php';
  static const String PRAYER_API = BASE_URL + 'prayer_form_request.php?';
  static const String NO_INTERNET_FOUND = 'No internet connection found';
  static const String SOMETHING_WENT = 'Something went wrong';
  static const String EMPTY_TEXT_ERROR = "This field can't be empty";
  static const String PLEASE_WAIT = 'Please Wait...';
  static const String MESSAGE_THUMBNAIL =
      'http://webinnovativetechnology.com/rapture/uploaded_files/message/';
  static const String BASE_THUMBNAIL =
      'http://webinnovativetechnology.com/rapture/uploaded_files/';
  static const String VIDEO_THUMBNAIL =
      'http://webinnovativetechnology.com/rapture/uploaded_files/youtube_video/thumb/';
  static const String MUSIC_TRACK =
      'http://webinnovativetechnology.com/rapture/uploaded_files/track_list/';
  static const String BLOG_THUMBNAIL = BASE_THUMBNAIL + 'blog/thumb/';
  static const String UPCOMING_THUMBNAIL = BASE_THUMBNAIL + 'event/thumb/';
  static const String MUSIC_ALBUM_THUMBNAIL =
      'http://webinnovativetechnology.com/rapture/uploaded_files/audio_album/';
  static const String MUSIC_LIST_THUMBNAIL =
      'http://webinnovativetechnology.com/rapture/uploaded_files/track_list/thumb/';
  static const String MISSION_THUMBNAIL = BASE_THUMBNAIL + 'mission/';
  static const String FAITH_THUMBNAIL = BASE_THUMBNAIL + 'faith/';
  static const String YOUTUBE_APP_KEY =
      "AIzaSyD7ioDAOXF3XeU3MZIYYBMllaanH2WqQ4E";
  static const String GALLERY_ALBUM_THUMBNAIL =
      'http://webinnovativetechnology.com/rapture/uploaded_files/gallery/';
  static const String THANK_YOU_TEXT =
      "Thank you for taking the time to fill out the form. We deeply appericiate the invitation to co-labor with you in your area for a great spiritual breakthrough!\n\nWe will prayfully consider your request and respond as quickly as possible.\n\nThank you and God bless";
  static const String PDF_File_BASE_URL =
      'http://webinnovativetechnology.com/rapture/uploaded_files/pdf_notes/';
}
