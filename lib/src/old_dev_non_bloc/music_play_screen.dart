//import 'dart:async';
//
//import 'package:audioplayer/audioplayer.dart';
//import 'package:cached_network_image/cached_network_image.dart';
//import 'package:evans/src/models/music_list_data.dart';
//import 'package:evans/src/old_dev_non_bloc/utils/constants.dart';
//
//import 'package:flutter/material.dart';
//
//import 'package:flutter/services.dart';
//
//class MusicPlayScreen extends StatefulWidget {
//  final MusicListData musicListData;
//
//  const MusicPlayScreen({Key key, this.musicListData}) : super(key: key);
//
//  @override
//  State<StatefulWidget> createState() {
//    return _MusicState(musicListData);
//  }
//}
//
//class _MusicState extends State<MusicPlayScreen> {
//  MusicListData _musicListData;
//  bool isPlay = false;
//  AudioPlayer audioPlayer = new AudioPlayer();
//
//  /*String url =
//      "https://ia802708.us.archive.org/3/items/count_monte_cristo_0711_librivox/count_of_monte_cristo_001_dumas.mp3";
//*/
//  Duration duration;
//  Duration position;
//
//  PlayerState playerState = PlayerState.STOPPED;
//
//  get isPlaying => playerState == PlayerState.PLAYING;
//
//  get isPaused => playerState == PlayerState.PAUSED;
//
//  get durationText =>
//      duration != null ? duration.toString().split('.').first : '';
//
//  get positionText =>
//      position != null ? position.toString().split('.').first : '';
//
//  bool isMuted = false;
//
//  StreamSubscription _positionSubscription;
//  StreamSubscription _audioPlayerStateSubscription;
//
//  _MusicState(MusicListData musicData) {
//    _musicListData = musicData;
//  }
//
//  @override
//  void initState() {
//    super.initState();
//    initAudioPlayer();
//  }
//
//  @override
//  void dispose() {
//    super.dispose();
//  }
//
//  void initAudioPlayer() {
//    audioPlayer = new AudioPlayer();
//    _positionSubscription = audioPlayer.onAudioPositionChanged
//        .listen((p) => setState(() => position = p));
//    _audioPlayerStateSubscription =
//        audioPlayer.onPlayerStateChanged.listen((s) {
//      if (s == AudioPlayerState.PLAYING) {
//        setState(() => duration = audioPlayer.duration);
//      } else if (s == AudioPlayerState.STOPPED) {
//        onComplete();
//        setState(() {
//          position = duration;
//        });
//      }
//    }, onError: (msg) {
//      setState(() {
//        playerState = PlayerState.STOPPED;
//        duration = new Duration(seconds: 0);
//        position = new Duration(seconds: 0);
//      });
//    });
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      appBar: AppBar(
//        title: Text('Audio Player'),
//      ),
//      body: Column(
//        crossAxisAlignment: CrossAxisAlignment.end,
//        mainAxisAlignment: MainAxisAlignment.end,
//        children: <Widget>[
//          Expanded(
//            child: Container(
//              width: MediaQuery.of(context).size.width,
//              decoration: BoxDecoration(
//                // Box decoration takes a gradient
//                gradient: new LinearGradient(
//                  begin: Alignment.bottomCenter,
//                  end: Alignment.topCenter,
//                  colors: [Colors.white12, Colors.blue, Colors.blue],
//                ),
//              ),
//              child: Center(
//                child: Container(
//                  height: 250,
//                  width: 250,
//                  child: Card(
//                    elevation: 20,
//                    child: CachedNetworkImage(
//                      fit: BoxFit.fill,
//                      imageUrl: Constants.MUSIC_LIST_THUMBNAIL +
//                          _musicListData.poster,
//                      placeholder: (context, url) => Image.asset(
//                        'images/ic_placeholder.png',
//                        fit: BoxFit.fill,
//                      ),
//                      errorWidget: (context, url, error) => Image.asset(
//                        'images/ic_placeholder.png',
//                        fit: BoxFit.fill,
//                      ),
//                    ),
//                  ),
//                ),
//              ),
//            ),
//          ),
//          Container(
//            height: 250,
//            width: MediaQuery.of(context).size.width,
//            color: Colors.blue,
//            child: Center(
//              child: _buildPlayer(),
//            ),
//          )
//        ],
//      ),
//    );
//  }
//
//  Future<void> play() async {
//    await audioPlayer.play(Constants.MUSIC_TRACK + _musicListData.track);
//    setState(() => playerState = PlayerState.PLAYING);
//  }
//
//  Future<void> pause() async {
//    await audioPlayer.pause();
//    setState(() => playerState = PlayerState.PAUSED);
//  }
//
//  Future<void> stop() async {
//    await audioPlayer.stop();
//    setState(() {
//      playerState = PlayerState.STOPPED;
//      position = new Duration();
//    });
//  }
//
//  void onComplete() {
//    setState(() => playerState = PlayerState.STOPPED);
//  }
//
//  Widget _buildPlayer() => Container(
//          child: Column(mainAxisSize: MainAxisSize.min, children: [
//        Center(
//          child: Padding(
//            padding: const EdgeInsets.all(8.0),
//            child: Text(
//              _musicListData.name,
//              style: TextStyle(
//                  color: Colors.white,
//                  fontWeight: FontWeight.bold,
//                  letterSpacing: 2,
//                  fontSize: 18),
//            ),
//          ),
//        ),
//        Container(
//            width: MediaQuery.of(context).size.width,
//            child: duration == null
//                ? Container()
//                : Slider(
//                    inactiveColor: Colors.white,
//                    activeColor: Colors.green,
//                    value: position == null
//                        ? 0
//                        : position?.inMilliseconds?.toDouble(),
//                    onChanged: (double value) =>
//                        audioPlayer.seek((value / 1000).roundToDouble()),
//                    min: 0.0,
//                    max: position == null
//                        ? 1000000.0
//                        : duration.inMilliseconds.toDouble(),
//                    onChangeEnd: (double value) => {isPlay = false},
//                  )),
//        Padding(
//          padding: const EdgeInsets.only(left: 16.0, right: 16.0, bottom: 16.0),
//          child: Row(
//            children: <Widget>[
//              Expanded(
//                  child: Text(
//                positionText,
//                style: TextStyle(color: Colors.white),
//              )),
//              Expanded(
//                  child: Text(
//                durationText,
//                textAlign: TextAlign.right,
//                style: TextStyle(color: Colors.white),
//              ))
//            ],
//          ),
//        ),
//        Center(
//          child: SizedBox(
//            height: 60,
//            width: 60,
//            child: new FloatingActionButton(
//              elevation: 20,
//              backgroundColor: Colors.green,
//              onPressed: () {
//                setState(() {
//                  if (isPlay) {
//                    isPlay = false;
//                    pause();
//                  } else {
//                    play();
//                    isPlay = true;
//                  }
//                });
//              },
//              child: new ConstrainedBox(
//                  constraints: new BoxConstraints.expand(),
//                  child: Icon(
//                    !isPlay ? Icons.play_arrow : Icons.pause,
//                    size: 40,
//                  )),
//            ),
//          ),
//        ),
//      ]));
//
//  Future mute(bool muted) async {
//    await audioPlayer.mute(muted);
//    setState(() {
//      isMuted = muted;
//    });
//  }
//}
//
//enum PlayerState { PLAYING, PAUSED, STOPPED }
