part of 'prayer_bloc.dart';

@immutable
abstract class PrayerState {}

class InitialPrayerState extends PrayerState {}

class LoadingPrayerState extends PrayerState {}

class SuccessPrayerState extends PrayerState {}

class ErrorPrayerState extends PrayerState {
  final DioError error;

  ErrorPrayerState(this.error);
}
