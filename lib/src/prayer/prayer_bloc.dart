import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:evans/src/api/api.dart';
import 'package:evans/src/models/prayer_data.dart';
import 'package:evans/src/utils/constants.dart';
import 'package:meta/meta.dart';

part 'prayer_event.dart';

part 'prayer_state.dart';

class PrayerBloc extends Bloc<PrayerEvent, PrayerState> {
  ApiMethods apiMethods = ApiMethods();
  @override
  PrayerState get initialState => InitialPrayerState();

  @override
  Stream<PrayerState> mapEventToState(PrayerEvent event) async* {
    if (event is SendPrayerEvent) {
      try {
        yield LoadingPrayerState();
        await apiMethods.requestInPost(
            Constants.PRAYER_API, event.data.toJson());
        yield SuccessPrayerState();
      } catch (e) {
        yield ErrorPrayerState(e);
      }
    }
  }
}
