import 'package:evans/src/models/invite.dart';
import 'package:evans/src/models/prayer_data.dart';
import 'package:evans/src/prayer/prayer_bloc.dart';
import 'package:evans/src/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class Prayer extends StatefulWidget {
  @override
  _PrayerState createState() => _PrayerState();
}

class _PrayerState extends State<Prayer> {
  List<String> _listPrayerFor;
  final GlobalKey<FormBuilderState> _fbKey3 = GlobalKey<FormBuilderState>();

  PrayerData data = PrayerData();

  @override
  void initState() {
    super.initState();
    _listPrayerFor = new List<String>();
    _addPrayerFor();
  }

  void _addPrayerFor() {
    _listPrayerFor.add('Prayer For*');
    _listPrayerFor.add('A Neighbour/Co-Worker');
    _listPrayerFor.add('Abuse');
    _listPrayerFor.add('Alchol');
    _listPrayerFor.add('Cancer');
    _listPrayerFor.add('Child Custody');
    _listPrayerFor.add('Church');
    _listPrayerFor.add('Cold & Flu');
    _listPrayerFor.add('Deliverance from Addictions');
    _listPrayerFor.add('Depression');
    _listPrayerFor.add('Diabetes');
    _listPrayerFor.add('Drugs');
    _listPrayerFor.add('Emotional Distress');
    _listPrayerFor.add('Family Member');
    _listPrayerFor.add('Family Situation');
    _listPrayerFor.add('For a Brother/Sister in Christ');
    _listPrayerFor.add('For a Friend');
    _listPrayerFor.add('For a Loved One');
    _listPrayerFor.add('For Finance');
    _listPrayerFor.add('For My Business');
    _listPrayerFor.add('For My Family');
    _listPrayerFor.add('For My Home Church');
    _listPrayerFor.add('For My Job');
    _listPrayerFor.add('For My Ministry');
    _listPrayerFor.add('For Myself');
    _listPrayerFor.add('For Provision');
    _listPrayerFor.add('For Current World Situation');
    _listPrayerFor.add('For the Government');
    _listPrayerFor.add('Heart Problem');
    _listPrayerFor.add('Internal Organs');
    _listPrayerFor.add('Legal Situation');
    _listPrayerFor.add('Life Threates');
    _listPrayerFor.add('Lung Disease');
    _listPrayerFor.add('Marriage Restoration');
    _listPrayerFor.add('Mental Illness');
    _listPrayerFor.add('Military Service');
    _listPrayerFor.add('Others');
    _listPrayerFor.add('Physical Ailment');
    _listPrayerFor.add('Protection');
    _listPrayerFor.add('Rebellious Children');
    _listPrayerFor.add('Salvation');
    _listPrayerFor.add('Sexual Perversion');
    _listPrayerFor.add('Stroke');
    _listPrayerFor.add('Tabacco');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Prayer Request'),
      ),
      body: BlocListener<PrayerBloc, PrayerState>(
        listener: (BuildContext context, PrayerState state) {
          if (state is SuccessPrayerState) {
            showDialog(
                context: context,
                builder: (_) => AlertDialog(
                      actions: <Widget>[
                        FlatButton(
                            onPressed: () => Navigator.of(context).pop(),
                            child: Text('Close'))
                      ],
                      title: Row(
                        children: <Widget>[
                          Icon(
                            Icons.done,
                            color: Colors.green,
                          ),
                          SizedBox(
                            width: 12,
                          ),
                          Text(
                            'Success',
                            style: TextStyle(color: Colors.green),
                          ),
                        ],
                      ),
                      content: Text('Request Sent successfully!'),
                    ));
          }
          if (state is ErrorPrayerState) {
            showDialog(
                context: context,
                builder: (_) => AlertDialog(
                      actions: <Widget>[
                        FlatButton(
                            onPressed: () => Navigator.of(context).pop(),
                            child: Text('Close'))
                      ],
                      title: Row(
                        children: <Widget>[
                          Icon(
                            Icons.error_outline,
                            color: Colors.red,
                          ),
                          SizedBox(
                            width: 12,
                          ),
                          Text(
                            'Error',
                            style: TextStyle(color: Colors.red),
                          ),
                        ],
                      ),
                      content: Text('Unable to send request please try again!'),
                    ));
          }
        },
        child: BlocBuilder<PrayerBloc, PrayerState>(
            builder: (BuildContext context, PrayerState state) {
          return SingleChildScrollView(
            child: Container(
              color: ChurchAppColors.prayerBackground,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Image.asset('assets/images/prayer.png'),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Material(
                      color: Colors.transparent,
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              Constants.Prayer_TITLE,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: ChurchAppColors.golden,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20,
                                  letterSpacing: 4),
                            ),
                          ),
                          FormBuilder(
                            key: _fbKey3,
                            initialValue: {
                              'date': DateTime.now(),
                              'accept_terms': false,
                            },
                            child: Column(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: FormBuilderTextField(
                                    textInputAction: TextInputAction.next,
                                    style: TextStyle(color: Colors.white),
                                    attribute: "title",
                                    decoration:
                                        InputDecoration(labelText: "Full Name"),
                                    validators: [
                                      FormBuilderValidators.required(),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: FormBuilderTextField(
                                    textInputAction: TextInputAction.next,
                                    attribute: "email",
                                    keyboardType: TextInputType.emailAddress,
                                    style: TextStyle(color: Colors.white),
                                    decoration:
                                        InputDecoration(labelText: "Email"),
                                    validators: [
                                      FormBuilderValidators.email(),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: FormBuilderTextField(
                                    textInputAction: TextInputAction.next,
                                    attribute: "phone",
                                    keyboardType: TextInputType.number,
                                    style: TextStyle(color: Colors.white),
                                    decoration: InputDecoration(
                                        labelText: "Contact No"),
                                    maxLength: 10,
                                    validators: [
                                      FormBuilderValidators.numeric(),
                                      FormBuilderValidators.maxLength(10)
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: FormBuilderTextField(
                                    textInputAction: TextInputAction.newline,
                                    attribute: "country",
                                    style: TextStyle(color: Colors.white),
                                    keyboardType: TextInputType.text,
                                    decoration:
                                        InputDecoration(labelText: "Country"),
                                    validators: [
                                      FormBuilderValidators.required(),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: FormBuilderTextField(
                                    textInputAction: TextInputAction.newline,
                                    attribute: "address",
                                    keyboardType: TextInputType.text,
                                    style: TextStyle(color: Colors.white),
                                    decoration:
                                        InputDecoration(labelText: "Address"),
                                    validators: [
                                      FormBuilderValidators.required(),
                                      FormBuilderValidators.minLength(5,
                                          errorText:
                                              'Address must be bigger than 5 characters!')
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: FormBuilderTextField(
                                    textInputAction: TextInputAction.newline,
                                    attribute: "state",
                                    keyboardType: TextInputType.text,
                                    style: TextStyle(color: Colors.white),
                                    decoration:
                                        InputDecoration(labelText: "State"),
                                    validators: [
                                      FormBuilderValidators.required(),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: FormBuilderTextField(
                                    textInputAction: TextInputAction.newline,
                                    attribute: "city",
                                    keyboardType: TextInputType.text,
                                    style: TextStyle(color: Colors.white),
                                    decoration:
                                        InputDecoration(labelText: "City"),
                                    validators: [
                                      FormBuilderValidators.required(),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: FormBuilderDropdown(
                                    attribute: 'prayer_for',
                                    style: TextStyle(
                                        color: ChurchAppColors.golden),
                                    initialValue: 'prayer for',
                                    items: _listPrayerFor.map((String value) {
                                      return new DropdownMenuItem<String>(
                                        value: value,
                                        child: Text(
                                          value,
                                          style: TextStyle(
                                              color: ChurchAppColors.golden),
                                        ),
                                      );
                                    }).toList()
                                      ..add(DropdownMenuItem(
                                          value: 'prayer for',
                                          child: Text(
                                            'Prayer for',
                                            style: TextStyle(
                                                color: ChurchAppColors.golden),
                                          ))),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: FormBuilderTextField(
                                    textInputAction: TextInputAction.newline,
                                    style: TextStyle(color: Colors.white),
                                    minLines: 1,
                                    maxLines: 2,
                                    attribute: "message",
                                    decoration:
                                        InputDecoration(labelText: "Message"),
                                    validators: [
                                      FormBuilderValidators.required(),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                MaterialButton(
                                  child: Text("Submit"),
                                  textColor: Colors.white,
                                  color: Colors.green,
                                  shape: StadiumBorder(
                                      side: BorderSide(color: Colors.green)),
                                  onPressed: () {
                                    if (_fbKey3.currentState
                                        .saveAndValidate()) {
                                      print(_fbKey3.currentState.value);
                                      data = PrayerData.fromJson(
                                          _fbKey3.currentState.value);
                                      BlocProvider.of<PrayerBloc>(context)
                                          .add(SendPrayerEvent(data));
                                      _fbKey3.currentState.reset();
                                    }
                                  },
                                ),
                                MaterialButton(
                                  elevation: 5,
                                  color: Colors.white,
                                  textColor: Colors.red,
                                  child: Text("Reset"),
                                  shape: StadiumBorder(
                                      side: BorderSide(color: Colors.white70)),
                                  onPressed: () {
                                    _fbKey3.currentState.reset();
                                  },
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        }),
      ),
    );
  }
}
