part of 'prayer_bloc.dart';

@immutable
abstract class PrayerEvent {}

@immutable
class LoadPrayerEvent extends PrayerEvent {}

@immutable
class SendPrayerEvent extends PrayerEvent {
  final PrayerData data;

  SendPrayerEvent(this.data);
}
