import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'testimony_event.dart';

part 'testimony_state.dart';

class TestimonyBloc extends Bloc<TestimonyEvent, TestimonyState> {
  @override
  TestimonyState get initialState => InitialTestimonyState();

  @override
  Stream<TestimonyState> mapEventToState(TestimonyEvent event) async* {
    // TODO: Add your event logic
  }
}
