import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:evans/src/api/api.dart';
import 'package:evans/src/models/home_images.dart';
import 'package:evans/src/utils/constants.dart';
import 'package:meta/meta.dart';

part 'home_event.dart';

part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final ApiMethods api = ApiMethods();

  @override
  HomeState get initialState => InitialHomeState();

  @override
  Stream<HomeState> mapEventToState(HomeEvent event) async* {
    if (event is ChangeBottomNavigationBarIndex) {
      yield state.copyWith(bottomNavigationBarIndex: event.index);
      return;
    }
    if (event is LoadHomeEvent) {
      yield LoadingHomeState();
      try {
        var response = await api.requestInGet(Constants.HOME_IMAGES_API);
        List<HomeData> homeImages = homeDataFromJson(response);

        yield LoadedHomeState(homeData: homeImages);
      } catch (e) {
        yield ErrorHomeState(e);
      }
    }
  }
}
