part of 'home_bloc.dart';

@immutable
abstract class HomeEvent {}

class ChangeBottomNavigationBarIndex extends HomeEvent {
  final int index;

  ChangeBottomNavigationBarIndex(this.index);
}

@immutable
class LoadHomeEvent extends HomeEvent {}
