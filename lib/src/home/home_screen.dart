import 'dart:async';
import 'dart:io';

import 'package:banner_view/banner_view.dart';
import 'package:circular_bottom_navigation/circular_bottom_navigation.dart';
import 'package:circular_bottom_navigation/tab_item.dart';
import 'package:device_info/device_info.dart';
import 'package:dio/dio.dart';
import 'package:easy_dialog/easy_dialog.dart';
import 'package:evans/src/api/api.dart';
import 'package:evans/src/blog/blog_bloc.dart';
import 'package:evans/src/cloud/cloud_screen.dart';
import 'package:evans/src/events/events_bloc.dart';
import 'package:evans/src/fire/fire_screen.dart';
import 'package:evans/src/gallery/gallery_bloc.dart';
import 'package:evans/src/gallery/gallery_screen.dart';
import 'package:evans/src/home/home_bloc.dart';
import 'package:evans/src/love/love_offering.dart';
import 'package:evans/src/messages/messages_bloc.dart';
import 'package:evans/src/mission/mission_bloc.dart';
import 'package:evans/src/models/fcm_data.dart';
import 'package:evans/src/models/message_data.dart';
import 'package:evans/src/music/audio_album.dart';
import 'package:evans/src/music/music_bloc.dart';
import 'package:evans/src/music/music_screen.dart';
import 'package:evans/src/old_dev_non_bloc/gallery_show_case.dart';
import 'package:evans/src/utils/constants.dart';
import 'package:evans/src/utils/router.dart';
import 'package:evans/src/utils/services/locator.dart';
import 'package:evans/src/utils/services/navigation_service.dart';
import 'package:evans/src/utils/services/notification_handler.dart';
import 'package:evans/src/utils/widgets/menu_bottomsheet.dart';
import 'package:evans/src/utils/widgets/network_image.dart';
import 'package:evans/src/video/video_bloc.dart';
import 'package:evans/src/video/video_screen.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_splash_screen/flutter_splash_screen.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:groovin_material_icons/groovin_material_icons.dart';
import 'package:groovin_widgets/groovin_widgets.dart';
import 'package:intl/intl.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:share/share.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:solid_bottom_sheet/solid_bottom_sheet.dart';
import 'dart:convert';
import 'package:flutter_plugin_playlist/flutter_plugin_playlist.dart';
import 'package:url_launcher/url_launcher.dart';

RmxAudioPlayer rmxAudioPlayer = new RmxAudioPlayer();
Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) async {
  print("_backgroundMessageHandler");
  if (message.containsKey('data')) {
    // Handle data message
    final dynamic data = message['data'];
    print("_backgroundMessageHandler data: ${data}");
  }

  if (message.containsKey('notification')) {
    // Handle notification message
    final dynamic notification = message['notification'];
    print("_backgroundMessageHandler notification: ${notification}");
//        Fimber.d("=====>myBackgroundMessageHandler $message");
  }
  return Future<void>.value();
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with TickerProviderStateMixin {
//  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey(debugLabel: 'ScaffoldKey');

  PanelController _pc = new PanelController();
  int selectedPos = 0;
  int previousPos;
  PersistentBottomSheetController controller;
  double bottomNavBarHeight = 60;
  List<TabItem> tabItems = List.of([
    TabItem(
      Icons.home,
      "Home",
      Colors.blue,
      labelStyle: TextStyle(
        fontWeight: FontWeight.bold,
        color: Colors.blue,
        fontSize: 12,
      ),
    ),
    TabItem(
      FontAwesomeIcons.gripfire,
      "Pillar of Fire",
      Colors.red,
      labelStyle: TextStyle(
        fontWeight: FontWeight.bold,
        color: Colors.red,
        fontSize: 12,
      ),
    ),
    TabItem(
      Icons.cloud,
      "Pillar of Cloud",
      Colors.blue,
      labelStyle: TextStyle(
          color: Colors.blue, fontWeight: FontWeight.bold, fontSize: 12),
    ),
    TabItem(
      GroovinMaterialIcons.heart,
      "💜 Offerings",
      Colors.pink,
      labelStyle: TextStyle(
        color: Colors.pink,
        fontWeight: FontWeight.bold,
      ),
    ),
    TabItem(
      Icons.more,
      "More",
      Colors.green,
      labelStyle: TextStyle(
        color: Colors.green,
        fontWeight: FontWeight.bold,
      ),
    ),
  ]);
  CircularBottomNavigationController _navigationController;
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  ApiMethods apiMethods = ApiMethods();
  SharedPreferences prefs;
  StreamSubscription iosSubscription;
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  AndroidDeviceInfo androidInfo;
  IosDeviceInfo iosInfo;
  PermissionStatus permission;
  @override
  void initState() {
    super.initState();
    NotificationHandler().initializeFcmNotification(context);
    //firebaseCloudMessagingListeners();
    rmxAudioPlayer.initialize();
    hideScreen();
    BlocProvider.of<HomeBloc>(context).add(LoadHomeEvent());
    _navigationController = new CircularBottomNavigationController(selectedPos);
  }

  ///hide your splash screen
  Future<void> hideScreen() async {
    Future.delayed(Duration(milliseconds: 3600), () {
      FlutterSplashScreen.hide();
    });
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(
        url,
        forceSafariVC: false,
      );
    } else {
      throw 'Could not launch $url';
    }
  }

  PermissionStatus _permissionStatus = PermissionStatus.unknown;
  PermissionGroup _permissionGroup = PermissionGroup.storage;
  void checkServiceStatus(BuildContext context, PermissionGroup permission) {
    PermissionHandler()
        .checkServiceStatus(permission)
        .then((ServiceStatus serviceStatus) {
      final SnackBar snackBar =
          SnackBar(content: Text(serviceStatus.toString()));

      Scaffold.of(context).showSnackBar(snackBar);
    });
  }

  Future<void> requestPermission(PermissionGroup permission) async {
    final List<PermissionGroup> permissions = <PermissionGroup>[permission];
    final Map<PermissionGroup, PermissionStatus> permissionRequestResult =
        await PermissionHandler().requestPermissions(permissions);
    print(permissionRequestResult);
//      _permissionStatus = permissionRequestResult[permission];
    print(_permissionStatus);
  }

  void _listenForPermissionStatus() {
    final Future<PermissionStatus> statusFuture =
        PermissionHandler().checkPermissionStatus(_permissionGroup);

    statusFuture.then((PermissionStatus status) {
      if (status.value != 2 && status.value != 1) {
        requestPermission(_permissionGroup);
      }
//      setState(() {
//        _permissionStatus = status;
//      });
    });
  }

  Future registerDevice(Map<String, dynamic> body) async {
    String fcmId = body['fcm_id'];
    String deviceId = body['device_id'];
    String platform = body['platform'];
    try {
      Map<String, dynamic> rows = await apiMethods
          .requestInGet(Constants.REGISTER_DEVICE_API + deviceId);
      if (rows != null) {
        Map<String, dynamic> responseJson = rows;
        if (responseJson.length == 0) {
          print('Registring new device');

          await apiMethods.requestInPost(Constants.REGISTER_DEVICE_API, body);
        } else {
          print('Device is already registered updating token..');
          await apiMethods.requestInPut(Constants.REGISTER_DEVICE_API, body);
        }
      } else {
        print('Registring new device');
        await apiMethods.requestInPost(Constants.REGISTER_DEVICE_API, body);
      }

      bool operationStatus = await prefs.setString('clientSecret', fcmId);
      print('Operation status: $operationStatus');
      prefs.setString('deviceId', deviceId);
      prefs.setString('platform', platform);
    } catch (e) {
      print(e);
    }
  }

//  Future onSelectNotification(String payload) async {
//    if (payload != null) {
//      debugPrint('notification payload: ' + payload);
//    }
//    await Navigator.push(
//      context,
//      new MaterialPageRoute(
//          builder: (context) => Container(
//                child: Center(
//                  child: Text('You cliked on Notification'),
//                ),
//              )),
//    );
//  }

  Future getDeviceId() async {
    try {
      androidInfo = await deviceInfo.androidInfo;
    } catch (e) {
      print(e);
    }
  }

  Future getIosDeviceId() async {
    try {
      iosInfo = await deviceInfo.iosInfo;
    } catch (e) {
      print(e);
    }
  }

  initSharedPreference() async {
    prefs = await SharedPreferences.getInstance();
  }

  checkStoragePermission() async {
    permission = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.contacts);
    if (permission.value != 2 && permission.value != 1) {
      requestPermission(_permissionGroup);
    }
    if (permission.value == 1) {}
  }

  void firebaseCloudMessagingListeners() async {
//    initSharedPreference();
    _listenForPermissionStatus();
    prefs = await SharedPreferences.getInstance();
    var deviceId;
    var platform;
    if (Platform.isIOS) {
      iOSPermission();
      getIosDeviceId();
      platform = "IOS";
    } else {
      getDeviceId();
      platform = "ANDROID";
    }
    String clientSecret = prefs.get('clientSecret');
    if (clientSecret == null) {
      _firebaseMessaging.getToken().then((token) async {
        var deviceName;
        if (Platform.isIOS) {
          deviceId = iosInfo.identifierForVendor;
        } else {
          deviceId = androidInfo.androidId;
        }
//          var url = deviceName + "&fcmid=" + token;
        print('Token $token');
        Map<String, dynamic> body = {
          "device_id": deviceId,
          "fcm_id": token,
          "platform": platform
        };
        await registerDevice(body);
//          _presenter.makeGetCall(Constants.REGISTRATION + url, PageName.MUSIC);
      });
    } else {
      print('ALERT!,Device is Already registred');
    }

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        final snackBar = SnackBar(
            content: Text(
                '${message['notification']['title']}! \n${message['notification']['body'] != null ? message['notification']['body'] : ''}'));
        Constants.scaffoldKey.currentState.showSnackBar(snackBar);
      },
//      onBackgroundMessage: Theme.of(context).platform == TargetPlatform.iOS
//          ? null
//          : myBackgroundMessageHandler,
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        switch (message['data']['screen']) {
          case 'blogs':
            BlocProvider.of<BlogBloc>(context).add(LoadBlogEvent());
            Navigator.pushNamed(context, Routes.blog);
            break;

          case 'messages':
            BlocProvider.of<MessagesBloc>(context).add(LoadMessagesEvent());
            Navigator.pushNamed(context, Routes.messages);
            break;
          case 'events':
            BlocProvider.of<EventsBloc>(context).add(LoadEventsEvent());
            Navigator.pushNamed(context, Routes.events);
            break;
        }

//        await locator<NavigationService>()
//            .navigateTo(message['data']['screen']);
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        switch (message['data']['screen']) {
          case 'blogs':
            BlocProvider.of<BlogBloc>(context).add(LoadBlogEvent());
            Navigator.pushNamed(context, Routes.blog);
            break;

          case 'messages':
            BlocProvider.of<MessagesBloc>(context).add(LoadMessagesEvent());
            Navigator.pushNamed(context, Routes.messages);
            break;
          case 'events':
            BlocProvider.of<EventsBloc>(context).add(LoadEventsEvent());
            Navigator.pushNamed(context, Routes.events);
            break;
        }
//        await locator<NavigationService>()
//            .navigateTo(message['data']['screen']);
      },
    );
  }

  void iOSPermission() {
    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }

  bool getAllowsNotifications() {
    return prefs.getBool('IS_SAVED') ?? true;
  }

  /// ----------------------------------------------------------
  /// Method that saves the user decision to allow notifications
  /// ----------------------------------------------------------
  Future<bool> setAllowsNotifications(bool value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool('IS_SAVED', value);
  }

  @override
  Widget build(BuildContext context) {
    var bottomNavWidth = MediaQuery.of(context).size.width - 40;
    return Scaffold(
        key: Constants.scaffoldKey,
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          leading: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Image.asset('assets/icons/logo.png'),
          ),
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.email),
                onPressed: () {
                  BlocProvider.of<MessagesBloc>(context)
                      .add(LoadMessagesEvent());
                  Navigator.pushNamed(context, Routes.messages);
                }),
            IconButton(
                icon: Icon(FontAwesomeIcons.donate),
                onPressed: () {
                  _launchURL(Constants.payPal);
                }),
          ],
          title: Text(
            "Evans Francis",
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
        ),
        body: buildSlidingUpPanel());
  }

  Widget buildSlidingUpPanel() {
    return Stack(
      children: <Widget>[
        SlidingUpPanel(
          controller: _pc,
          maxHeight: 600,
          minHeight: 0,
          panelSnapping: true,
          backdropEnabled: true,
          margin: EdgeInsets.only(left: 20, right: 20),
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(15), topLeft: Radius.circular(15)),
          defaultPanelState: PanelState.CLOSED,
          panel: SingleChildScrollView(
            child: buildPanel(),
          ),
          body: IndexedStack(
            index: _navigationController.value,
            children: <Widget>[
              HomeBody(),
              Fire(
                onSubmit: () {
                  setState(() {
                    _navigationController.value = 0;
                  });
                },
              ),
              Cloud(
                onSubmit: () {
                  setState(() {
                    _navigationController.value = 0;
                  });
                },
              ),
              LoveOffering(),
              HomeBody(),
            ],
          ),
        ),
        Positioned(
            left: 20, right: 20, bottom: 0, child: FittedBox(child: bottomNav))
      ],
    );
  }

  Column buildPanel() {
    return Column(
      children: <Widget>[
        SizedBox(
          height: 2,
        ),
        ModalDrawerHandle(
          handleColor: Colors.green,
        ),
        SizedBox(
          height: 2,
        ),
        ModalDrawerHandle(
          handleColor: Colors.green,
        ),
        ListTile(
          title: Text('Missions'),
          leading: Icon(FontAwesomeIcons.solidCheckCircle),
          onTap: () {
            _pc.close();
            BlocProvider.of<MissionBloc>(context).add(LoadMissionsEvent());
            Navigator.of(context).pushNamed(Routes.missions);
          },
        ),
        ListTile(
          title: Text('Upcoming Events'),
          leading: Icon(Icons.event),
          onTap: () {
            _pc.close();
//            var date = dateFormat.format(DateTime.now());
            BlocProvider.of<EventsBloc>(context).add(LoadEventsEvent());
            Navigator.of(context).pushNamed(Routes.events);
          },
        ),
        ListTile(
          leading: Icon(FontAwesomeIcons.mailBulk),
          title: Text('Invite Evans'),
          onTap: () {
            _pc.close();
            Navigator.of(context).pushNamed(Routes.invite);
          },
        ),
        ListTile(
          leading: Icon(FontAwesomeIcons.cross),
          title: Text('Statement of Faith'),
          onTap: () {
            _pc.close();
            Navigator.pushNamed(context, Routes.statement);
          },
        ),
        ListTile(
          leading: Icon(FontAwesomeIcons.share),
          title: Text('Share'),
          onTap: () {
            _pc.close();
            Share.share(
                "Install Evans Francis App \nPlay store\n ${Constants.RateUs}\nApp Store \n ${Constants.RateUsIOS}");
          },
        ),
        ListTile(
          leading: Icon(FontAwesomeIcons.locationArrow),
          title: Text('Reach Us'),
          onTap: () {
            _pc.close();
            Navigator.pushNamed(context, Routes.reachUs);
          },
        ),
        ListTile(
          leading: Icon(FontAwesomeIcons.dev),
          title: Text('Created By'),
          onTap: () {
//            _launchURL(Constants.CreatedBy);
            _pc.close();
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (_) => WebviewScaffold(
                          url: Constants.CreatedBy,
                          appBar: AppBar(
                            title: Text("Christian App Developers"),
                          ),
                        )));
          },
        ),
        ListTile(
          leading: Icon(FontAwesomeIcons.solidGrinStars),
          title: Text('Rate Us'),
          onTap: () {
            _pc.close();

            Platform.isAndroid
                ? _launchURL(Constants.RateUs)
                : _launchURL(Constants.RateUsIOS);
//            Navigator.push(
//                context,
//                MaterialPageRoute(
//                    builder: (_) => WebviewScaffold(
//                          url: Constants.RateUs,
//                          appBar: AppBar(
//                            title: Text("Christian App Developers"),
//                          ),
//                        )));
          },
        ),
        ListTile(
          leading: Icon(FontAwesomeIcons.infoCircle),
          title: Text('About Evans'),
          onTap: () {
            _pc.close();
            Navigator.pushNamed(context, Routes.about);
          },
        )
      ],
    );
  }

  Widget get bottomNav {
    return CircularBottomNavigation(
      tabItems,

      controller: _navigationController,
      barHeight: bottomNavBarHeight,
      barBackgroundColor: ChurchAppColors.golden,
      normalIconColor: Colors.white,
      animationDuration: Duration(milliseconds: 500),
      selectedCallback: (int) {
        if (int == 4) {
          _pc.isPanelOpen() ? _pc.close() : _pc.open();
        }
        setState(() {});
      },
//      selectedCallback: (int selectedPos) => BlocProvider.of<HomeBloc>(context)
//          .add(ChangeBottomNavigationBarIndex(selectedPos)),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _navigationController.dispose();
  }
}

class HomeBody extends StatelessWidget {
  DateFormat dateFormat = DateFormat('MMM yyyy');
  sendFcm(BuildContext context) async {
    var prefs = await SharedPreferences.getInstance();
    String token = prefs.get('clientSecret');
    String deviceId = prefs.get('deviceId');
    EasyDialog(title: Text('Send this on whatsapp'), contentList: [
      ListTile(
        title: Text(deviceId),
        subtitle: Text(
          deviceId,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
        trailing: Icon(Icons.share),
        onTap: () => Share.share('device id: ' + deviceId + ' token: ' + token),
      )
    ]).show(context);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(12.0),
          child: Material(
            color: ChurchAppColors.golden,
            borderRadius: BorderRadius.circular(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: IconButton(
                      icon: Icon(
                        FontAwesomeIcons.blog,
                        color: Colors.white,
                      ),
                      onPressed: () {
                        var date = dateFormat.format(DateTime.now());
                        BlocProvider.of<BlogBloc>(context).add(LoadBlogEvent());
                        Navigator.of(context).pushNamed(Routes.blog);
                      }),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: IconButton(
                      icon: Icon(
                        FontAwesomeIcons.music,
                        color: Colors.white,
                      ),
                      onPressed: () {
                        BlocProvider.of<MusicBloc>(context)
                            .add(LoadMusicEvent());
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (_) => AudioAlbums(
                                      rmxAudioPlayer: rmxAudioPlayer,
                                    )));
                      }),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: IconButton(
                      icon: Icon(
                        FontAwesomeIcons.video,
                        color: Colors.white,
                      ),
                      onPressed: () {
                        var date = dateFormat.format(DateTime.now());
                        BlocProvider.of<VideoBloc>(context)
                            .add(LoadVideosEvent());
                        Navigator.push(context,
                            MaterialPageRoute(builder: (_) => Video()));
                      }),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: IconButton(
                      icon: Icon(
                        Icons.photo,
                        color: Colors.white,
                      ),
                      onPressed: () {
                        BlocProvider.of<GalleryBloc>(context)
                            .add(LoadGalleryEvent());
                        Navigator.pushNamed(context, Routes.gallery);
                      }),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: IconButton(
                      icon: Icon(
                        FontAwesomeIcons.prayingHands,
                        color: Colors.white,
                      ),
                      onPressed: () {
                        //sendFcm(context);
                        Navigator.of(context).pushNamed(Routes.Prayer);
                      }),
                ),
              ],
            ),
          ),
        ),
        Expanded(
            flex: 3,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: BlocBuilder<HomeBloc, HomeState>(
                  builder: (BuildContext context, HomeState state) {
                if (state is LoadingHomeState) {
                  return Center(child: CircularProgressIndicator());
                }
                if (state is LoadedHomeState) {
                  return Container(
                    child: BannerView(
                      state.homeData
                          .map((image) => PNetworkImage(
                              Constants.BASE_THUMBNAIL + image.images))
                          .toList(),
                      log: false,
                      autoRolling: state.homeData.isNotEmpty,
                      intervalDuration: Duration(seconds: 10),
                    ),
                  );
                }
                return Container(
                  child: Center(
                    child: Text(
                        "Servers are under maintainance. Please try again later.."),
                  ),
                );
              }),
            )),
        Spacer()
      ],
    );
  }
}
