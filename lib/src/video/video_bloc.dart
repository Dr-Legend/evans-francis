import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:evans/src/api/api.dart';
import 'package:evans/src/models/blog_archives_dates.dart';
import 'package:evans/src/models/video_model.dart';
import 'package:evans/src/utils/constants.dart';
import 'package:meta/meta.dart';

part 'video_event.dart';

part 'video_state.dart';

class VideoBloc extends Bloc<VideoEvent, VideoState> {
  ApiMethods apiMethods = ApiMethods();
  @override
  VideoState get initialState => InitialVideoState();

  @override
  Stream<VideoState> mapEventToState(VideoEvent event) async* {
    if (event is LoadVideosEvent) {
      try {
        var response;
        yield LoadingVideoState();
        var dateRespose =
            await apiMethods.requestInGet(Constants.VIDEO_DATES_API);
        List<ArchiveDates> dates = archiveDatesFromJson(dateRespose[0]);
        if (event.monthString == null)
          response = await apiMethods
              .requestInGet(Constants.VIDEO_API + dates.first.substrDate);
        if (event.monthString != null)
          response = await apiMethods
              .requestInGet(Constants.VIDEO_API + event.monthString);
        List<VideoData> videos = videoDataFromJson(response);

        yield LoadedVideoState(videos: videos, dates: dates);
      } catch (e) {
        yield ErrorVideoState(e);
        print(e);
      }
    }
  }
}
