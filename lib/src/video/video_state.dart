part of 'video_bloc.dart';

@immutable
abstract class VideoState {}

class InitialVideoState extends VideoState {}

@immutable
class ErrorVideoState extends VideoState {
  final DioError error;

  ErrorVideoState(this.error);
}

@immutable
class LoadedVideoState extends VideoState {
  final List<VideoData> videos;
  final List<ArchiveDates> dates;

  LoadedVideoState({this.videos, @required this.dates});
}

@immutable
class LoadingVideoState extends VideoState {}
