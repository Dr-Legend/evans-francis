import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:evans/src/api/api.dart';
import 'package:evans/src/models/blog_archives_dates.dart';
import 'package:evans/src/models/blog_data.dart';
import 'package:evans/src/utils/constants.dart';
import 'package:http/http.dart';
import 'package:meta/meta.dart';

part 'blog_event.dart';

part 'blog_state.dart';

class BlogBloc extends Bloc<BlogEvent, BlogState> {
  final ApiMethods api = ApiMethods();
  @override
  BlogState get initialState => InitialBlogState();

  @override
  Stream<BlogState> mapEventToState(BlogEvent event) async* {
    if (event is LoadBlogEvent) {
      yield LoadingBlogState();
      try {
        var response;
        var dateRespose = await api.requestInGet(Constants.BLOGS_DATES_API);
        List<ArchiveDates> dates = archiveDatesFromJson(dateRespose[0]);
        if (event.monthString == null)
          response = await api
              .requestInGet(Constants.BLOG_API + dates.first.substrDate);
        if (event.monthString != null)
          response = await api.requestInGet(
              Constants.BLOG_API + event.monthString ?? dates.first.substrDate);

        List<BlogData> blogs = blogDataFromJson(response);

        yield LoadedBlogState(blogData: blogs, dates: dates);
      } catch (e) {
        yield ErrorBlogState(e);
      }
    }
  }
}
