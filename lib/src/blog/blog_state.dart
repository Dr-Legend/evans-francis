part of 'blog_bloc.dart';

@immutable
abstract class BlogState {}

class InitialBlogState extends BlogState {}

class LoadingBlogState extends BlogState {}

class LoadedBlogState extends BlogState {
  final List<BlogData> blogData;
  final List<ArchiveDates> dates;

  LoadedBlogState({this.blogData, @required this.dates});
}

class ErrorBlogState extends BlogState {
  final DioError error;

  ErrorBlogState(this.error);
}
