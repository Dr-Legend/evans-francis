part of 'blog_bloc.dart';

@immutable
abstract class BlogEvent {}

@immutable
class LoadBlogEvent extends BlogEvent {
  final String monthString;

  LoadBlogEvent({@required this.monthString});
}
