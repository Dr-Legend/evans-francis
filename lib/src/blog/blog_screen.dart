import 'package:cached_network_image/cached_network_image.dart';
import 'package:evans/src/blog/blog_bloc.dart';
import 'package:evans/src/models/blog_data.dart';
import 'package:evans/src/utils/constants.dart';
import 'package:evans/src/utils/widgets/html_content_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class Blog extends StatefulWidget {
  @override
  _BlogState createState() => _BlogState();
}

class _BlogState extends State<Blog> {
  DateFormat dateFormat = DateFormat('MMM yyyy');

  String monthString;

  @override
  void initState() {
    super.initState();
//    monthString = dateFormat.format(DateTime.now());
  }

  @override
  Widget build(BuildContext context) {
    final double _radius = 25.0;
    final double _screenHeight = MediaQuery.of(context).size.height;
    final double _carouselSize = _screenHeight / 2.1;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).canvasColor,
        title: Text("Blog"),
      ),
      body: BlocBuilder<BlogBloc, BlogState>(
        builder: (BuildContext context, BlogState state) {
          if (state is LoadingBlogState) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          if (state is ErrorBlogState) {
            return Center(
              child: Text("Unable to fetch blogs please try again later.."),
            );
          }
          if (state is LoadedBlogState) {
            if (monthString == null) {
              monthString = state.dates[0].substrDate;
            }
            return Container(
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: SizedBox(
                      height: 50,
                      child: DropdownButton(
                        elevation: 5,
                        icon: Icon(Icons.sort),
                        items: state.dates
                            .map((date) => DropdownMenuItem(
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(date.substrDate),
                                  ),
                                  value: date.substrDate,
                                ))
                            .toList(),
                        onChanged: (value) {
                          setState(() {
                            monthString = value;
                          });
                          BlocProvider.of<BlogBloc>(context)
                              .add(LoadBlogEvent(monthString: value));
                        },
                        value: monthString,
                      ),
                    ),
                  ),
                  Expanded(
                    child: ListView.builder(
                      shrinkWrap: false,
                      itemCount: state.blogData.length ?? 0,
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => HtmlContentScreenBasic(
                                      date: state.blogData[index].createdAt,
                                      title: 'Blog',
                                      name: state.blogData[index].title,
                                      image: Constants.BASE_THUMBNAIL +
                                          state.blogData[index].image,
                                      content:
                                          state.blogData[index].description,
                                      createdAt:
                                          state.blogData[index].createdAt,
                                    )));
                          },
                          child: BlogCard(
                            blogData: state.blogData[index],
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            );
          }
          return Container(
            child: Center(
              child: Text(
                  "Servers are under maintainance. Please try again later.."),
            ),
          );
        },
      ),
    );
  }
}

class BlogCard extends StatelessWidget {
  final BlogData blogData;
  final double radius;
  final double carouselSize;

  const BlogCard({Key key, this.blogData, this.radius, this.carouselSize})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 8,
        child: Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Material(
                elevation: 7,
                borderRadius: BorderRadius.circular(50),
                child: SizedBox(
                    height: 70,
                    width: 70,
                    child: Padding(
                        padding: const EdgeInsets.all(0.0),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(8),
                          child: Hero(
                            tag: this.blogData.createdAt,
                            child: CachedNetworkImage(
                              imageUrl:
                                  "${Constants.BLOG_THUMBNAIL}${this.blogData.image}",
                              fit: BoxFit.fill,
                            ),
                          ),
                        ))),
              ),
            ),
            Flexible(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  SizedBox(
                    width: 8,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0, bottom: 8.0),
                    child: Text(
                      this.blogData.title,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
//                  Padding(
//                    padding: const EdgeInsets.only(
//                        left: 8.0, bottom: 8.0, right: 8.0),
//                    child: Row(
//                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                      children: <Widget>[
//                        Text(
//                          DateFormat('dd/MM/yyyy')
//                              .format(DateTime.parse(this.blogData.createdAt)),
//                          style: Theme.of(context).textTheme.caption,
//                        ),
//                        Text(
//                          DateFormat('hh:mm a')
//                              .format(DateTime.parse(this.blogData.createdAt)),
//                          style: Theme.of(context).textTheme.caption,
//                        ),
//                      ],
//                    ),
//                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
