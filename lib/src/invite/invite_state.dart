part of 'invite_bloc.dart';

@immutable
abstract class InviteState {}

class InitialInviteState extends InviteState {}

class LoadingInviteState extends InviteState {}

class SuccessInviteState extends InviteState {}

class ErrorInviteState extends InviteState {
  final DioError error;

  ErrorInviteState(this.error);
}
