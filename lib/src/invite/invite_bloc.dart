import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:evans/src/api/api.dart';
import 'package:evans/src/models/invite.dart';
import 'package:evans/src/utils/constants.dart';
import 'package:meta/meta.dart';

part 'invite_event.dart';

part 'invite_state.dart';

class InviteBloc extends Bloc<InviteEvent, InviteState> {
  ApiMethods apiMethods = ApiMethods();
  @override
  InviteState get initialState => InitialInviteState();

  @override
  Stream<InviteState> mapEventToState(InviteEvent event) async* {
    if (event is SendInviteEvent) {
      try {
        yield LoadingInviteState();
        await apiMethods.requestInPost(
            Constants.INVITE_API, event.data.toJson());
        yield SuccessInviteState();
      } catch (e) {
        yield ErrorInviteState(e);
      }
    }
  }
}
