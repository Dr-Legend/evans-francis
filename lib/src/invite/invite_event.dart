part of 'invite_bloc.dart';

@immutable
abstract class InviteEvent {}

@immutable
class LoadInviteEvent extends InviteEvent {}

@immutable
class SendInviteEvent extends InviteEvent {
  final InviteData data;

  SendInviteEvent(this.data);
}
