import 'package:evans/src/invite/invite_bloc.dart';
import 'package:evans/src/models/invite.dart';
import 'package:evans/src/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_rounded_date_picker/rounded_picker.dart';
import 'package:intl/intl.dart';

class Invite extends StatefulWidget {
  @override
  _InviteState createState() => _InviteState();
}

class _InviteState extends State<Invite> {
  InviteData data = InviteData();
  final GlobalKey<FormBuilderState> _fbKey10 = GlobalKey<FormBuilderState>();

  TextEditingController dateController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    dateController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: ChurchAppColors.brandBackground,
        title: Text(
          'Invite',
          style: TextStyle(fontSize: 20),
        ),
      ),
//      resizeToAvoidBottomInset: false,
      body: BlocListener<InviteBloc, InviteState>(
        listener: (BuildContext context, InviteState state) {
          if (state is SuccessInviteState) {
            showDialog(
                context: context,
                builder: (_) => AlertDialog(
                      actions: <Widget>[
                        FlatButton(
                            onPressed: () {
                              _fbKey10.currentState.reset();
                              dateController.clear();
                              Navigator.of(context).pop();
                            },
                            child: Text('Close'))
                      ],
                      title: Row(
                        children: <Widget>[
                          Icon(
                            Icons.done,
                            color: Colors.green,
                          ),
                          SizedBox(
                            width: 12,
                          ),
                          Text(
                            'Success',
                            style: TextStyle(color: Colors.green),
                          ),
                        ],
                      ),
                      content: Text(
                        'Thank you for taking the time to fill out this form. we deeply appriciate the invitation to  co-labor with you in  your area  for a  great  spiritual breakthrough!\n\nEvans Francis will prayerfully consider  your invitation and respond as quickly as possible. We pray great blessing on you and your church/ministry.\n\nThank You and God Bless\n\nEvans Francis',
                        style: TextStyle(color: Colors.grey),
                      ),
                    ));
          }
          if (state is ErrorInviteState) {
            showDialog(
                context: context,
                builder: (_) => AlertDialog(
                      actions: <Widget>[
                        FlatButton(
                            onPressed: () => Navigator.of(context).pop(),
                            child: Text('Close'))
                      ],
                      title: Row(
                        children: <Widget>[
                          Icon(
                            Icons.error_outline,
                            color: Colors.red,
                          ),
                          SizedBox(
                            width: 12,
                          ),
                          Text(
                            'Error',
                            style: TextStyle(color: Colors.red),
                          ),
                        ],
                      ),
                      content: Text('Unable to send request please try again!'),
                    ));
          }
        },
        child: BlocBuilder<InviteBloc, InviteState>(
            builder: (BuildContext context, InviteState state) {
          return SingleChildScrollView(
            child: Container(
//          color: ChurchAppColors.brandBackground,
              decoration: BoxDecoration(
                  gradient: LinearGradient(colors: [
                ChurchAppColors.brandBackground,
                ChurchAppColors.brandBackgroundLight
              ])),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Material(
                    child: Image.asset('assets/images/brand.png'),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Material(
                      color: Colors.transparent,
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              Constants.INVITE,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: ChurchAppColors.golden,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20,
                                  letterSpacing: 4),
                            ),
                          ),
                          FormBuilder(
                            key: _fbKey10,
                            initialValue: {
                              'date': DateTime.now(),
                              'accept_terms': false,
                            },
                            child: Column(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: FormBuilderTextField(
                                    initialValue: null,
                                    textInputAction: TextInputAction.next,
                                    style: TextStyle(color: Colors.white),
                                    attribute: "organization",
                                    decoration: InputDecoration(
                                        labelText:
                                            "Name of Organization(Church/Ministry)"),
                                    validators: [
                                      FormBuilderValidators.required(),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: FormBuilderTextField(
                                    initialValue: null,
                                    textInputAction: TextInputAction.next,
                                    style: TextStyle(color: Colors.white),
                                    attribute: "title",
                                    decoration:
                                        InputDecoration(labelText: "Full Name"),
                                    validators: [
                                      FormBuilderValidators.required(),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: FormBuilderTextField(
                                    initialValue: null,
                                    textInputAction: TextInputAction.next,
                                    attribute: "email",
                                    keyboardType: TextInputType.emailAddress,
                                    style: TextStyle(color: Colors.white),
                                    decoration:
                                        InputDecoration(labelText: "Email"),
                                    validators: [
                                      FormBuilderValidators.email(),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: FormBuilderTextField(
                                    initialValue: null,
                                    textInputAction: TextInputAction.next,
                                    attribute: "phone",
                                    keyboardType: TextInputType.phone,
                                    style: TextStyle(color: Colors.white),
                                    decoration: InputDecoration(
                                        labelText: "Contact No"),
                                    maxLength: 10,
                                    validators: [
                                      FormBuilderValidators.numeric(),
                                      FormBuilderValidators.maxLength(10)
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: FormBuilderTextField(
                                    initialValue: null,
                                    textInputAction: TextInputAction.newline,
                                    attribute: "address",
                                    style: TextStyle(color: Colors.white),
                                    keyboardType: TextInputType.text,
                                    decoration:
                                        InputDecoration(labelText: "Address"),
                                    validators: [
                                      FormBuilderValidators.required(),
                                      FormBuilderValidators.minLength(5,
                                          errorText:
                                              'Address must be bigger than 5 characters!')
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: FormBuilderTextField(
                                    initialValue: null,
                                    textInputAction: TextInputAction.newline,
                                    attribute: "country",
                                    style: TextStyle(color: Colors.white),
                                    keyboardType: TextInputType.text,
                                    decoration:
                                        InputDecoration(labelText: "Country"),
                                    validators: [
                                      FormBuilderValidators.required(),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: FormBuilderTextField(
                                    initialValue: null,
                                    textInputAction: TextInputAction.newline,
                                    attribute: "state",
                                    keyboardType: TextInputType.text,
                                    style: TextStyle(color: Colors.white),
                                    decoration:
                                        InputDecoration(labelText: "State"),
                                    validators: [
                                      FormBuilderValidators.required(),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: FormBuilderTextField(
                                    initialValue: null,
                                    textInputAction: TextInputAction.newline,
                                    attribute: "city",
                                    keyboardType: TextInputType.text,
                                    style: TextStyle(color: Colors.white),
                                    decoration:
                                        InputDecoration(labelText: "City"),
                                    validators: [
                                      FormBuilderValidators.required(),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: FormBuilderTextField(
                                    initialValue: null,
                                    textInputAction: TextInputAction.done,
                                    attribute: "zip_code",
                                    keyboardType: TextInputType.phone,
                                    style: TextStyle(color: Colors.white),
                                    decoration: InputDecoration(
                                        labelText: "Zip/Postal code"),
                                    validators: [
                                      FormBuilderValidators.required(),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: FormBuilderTextField(
                                    initialValue: null,
                                    textInputAction: TextInputAction.newline,
                                    attribute: "website",
                                    keyboardType: TextInputType.text,
                                    style: TextStyle(color: Colors.white),
                                    decoration:
                                        InputDecoration(labelText: "Website"),
                                    validators: [
                                      FormBuilderValidators.url(),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: FormBuilderTextField(
                                    initialValue: null,
                                    textInputAction: TextInputAction.newline,
                                    attribute: "contact_person",
                                    keyboardType: TextInputType.text,
                                    style: TextStyle(color: Colors.white),
                                    decoration: InputDecoration(
                                        labelText: "Contact Person"),
                                    validators: [
                                      FormBuilderValidators.required(),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: FormBuilderTextField(
                                    initialValue: null,
                                    textInputAction: TextInputAction.newline,
                                    style: TextStyle(color: Colors.white),
                                    attribute: "contact_person_phone",
                                    keyboardType: TextInputType.phone,
                                    decoration: InputDecoration(
                                        labelText: "Contact Person Phone"),
                                    validators: [
                                      FormBuilderValidators.required(),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: FormBuilderTextField(
                                    initialValue: null,
                                    textInputAction: TextInputAction.newline,
                                    style: TextStyle(color: Colors.white),
                                    attribute: "pastor",
                                    keyboardType: TextInputType.text,
                                    decoration: InputDecoration(
                                        labelText: "Pastor/overseer"),
                                    validators: [
                                      FormBuilderValidators.required(),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Container(
                                    padding: EdgeInsets.all(8),
                                    color: ChurchAppColors.brandBackgroundLight,
                                    width: double.infinity,
                                    child: Center(
                                      child: Text(
                                        "Event Information",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color:
                                                ChurchAppColors.brandBackground,
                                            fontSize: 17,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: FormBuilderTextField(
                                    initialValue: null,
                                    textInputAction: TextInputAction.newline,
                                    style: TextStyle(color: Colors.white),
                                    attribute: "event_title",
                                    keyboardType: TextInputType.text,
                                    decoration: InputDecoration(
                                        labelText: "Event Title"),
                                    validators: [
                                      FormBuilderValidators.required(),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: FormBuilderTextField(
                                    initialValue: null,
                                    textInputAction: TextInputAction.newline,
                                    style: TextStyle(color: Colors.white),
                                    attribute: "event_theme",
                                    keyboardType: TextInputType.text,
                                    decoration: InputDecoration(
                                        labelText: "Event Theme"),
                                    validators: [
                                      FormBuilderValidators.required(),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: TextFormField(
                                    readOnly: true,
                                    controller: dateController,
                                    validator: (value) {
                                      if (value == null) {
                                        return "Date is required!";
                                      }
                                      return null;
                                    },
                                    onTap: () async {
                                      DateTime newDateTime =
                                          await showRoundedDatePicker(
                                        context: context,
                                        initialDate: DateTime.now(),
                                        firstDate:
                                            DateTime(DateTime.now().year - 1),
                                        lastDate:
                                            DateTime(DateTime.now().year + 1),
                                        theme: ThemeData.dark(),
                                        borderRadius: 16,
                                      );
                                      if (newDateTime != null) {
                                        setState(() {
                                          dateController.text =
                                              DateFormat('dd/MM/yyyy')
                                                  .format(newDateTime);
                                        });
                                      }
                                    },
                                    textInputAction: TextInputAction.newline,
                                    style: TextStyle(color: Colors.white),
                                    keyboardType: TextInputType.text,
                                    decoration: InputDecoration(
                                        labelText: "Event Date"),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: FormBuilderTextField(
                                    initialValue: null,
                                    textInputAction: TextInputAction.newline,
                                    style: TextStyle(color: Colors.white),
                                    attribute: "attendance",
                                    keyboardType: TextInputType.number,
                                    decoration: InputDecoration(
                                        labelText: "Event Attendance"),
                                    validators: [
                                      FormBuilderValidators.required(),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                MaterialButton(
                                  child: Text("Submit"),
                                  textColor: Colors.white,
                                  color: Colors.green,
                                  shape: StadiumBorder(
                                      side: BorderSide(color: Colors.green)),
                                  onPressed: () {
//                                    _fbKey10.currentState.reset();
                                    if (_fbKey10.currentState
                                        .saveAndValidate()) {
                                      print(_fbKey10.currentState.value);
                                      data = InviteData.fromJson(
                                          _fbKey10.currentState.value);
                                      data.eventDate = dateController.text;
                                      BlocProvider.of<InviteBloc>(context)
                                          .add(SendInviteEvent(data));
                                    }
                                  },
                                ),
                                MaterialButton(
                                  elevation: 5,
                                  color: Colors.white,
                                  textColor: Colors.red,
                                  child: Text("Reset"),
                                  shape: StadiumBorder(
                                      side: BorderSide(color: Colors.white70)),
                                  onPressed: () {
                                    _fbKey10.currentState.reset();
                                    dateController.clear();
                                  },
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        }),
      ),
    );
  }

  OutlineInputBorder get border {
    return OutlineInputBorder(
        borderSide: BorderSide(color: ChurchAppColors.brandBackgroundLight),
        borderRadius: BorderRadius.circular(20));
  }
}
