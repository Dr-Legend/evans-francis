import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:evans/src/api/api.dart';
import 'package:evans/src/models/mission_data.dart';
import 'package:evans/src/utils/constants.dart';
import 'package:meta/meta.dart';

part 'mission_event.dart';

part 'mission_state.dart';

class MissionBloc extends Bloc<MissionEvent, MissionState> {
  ApiMethods api = ApiMethods();
  @override
  MissionState get initialState => InitialMissionState();

  @override
  Stream<MissionState> mapEventToState(MissionEvent event) async* {
    if (event is LoadMissionsEvent) {
      yield LoadingMissionsState();
      try {
        var response = await api.requestInGet(Constants.MISSION);
//        var dateRespose = await api.requestInGet(Constants.BLOGS_DATES_API);
        List<MissionData> missions = missionDataFromJson(response);
//        List<ArchiveDates> dates = archiveDatesFromJson(dateRespose[0]);
        yield LoadedMissionsState(missionData: missions);
      } catch (e) {
        yield ErrorMissionsState(e);
      }
    }
  }
}
