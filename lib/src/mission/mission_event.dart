part of 'mission_bloc.dart';

@immutable
abstract class MissionEvent {}

@immutable
class LoadMissionsEvent extends MissionEvent {}
