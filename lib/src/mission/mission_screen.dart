import 'package:cached_network_image/cached_network_image.dart';
import 'package:evans/src/mission/mission_bloc.dart';
import 'package:evans/src/models/mission_data.dart';
import 'package:evans/src/utils/constants.dart';
import 'package:evans/src/utils/widgets/html_content_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class Missions extends StatefulWidget {
  @override
  _MissionsState createState() => _MissionsState();
}

class _MissionsState extends State<Missions> {
  DateFormat dateFormat = DateFormat('MMM yyyy');

  String monthString;

  @override
  void initState() {
    super.initState();
//    monthString = dateFormat.format(DateTime.now());
  }

  @override
  Widget build(BuildContext context) {
    final double _radius = 25.0;
    final double _screenHeight = MediaQuery.of(context).size.height;
    final double _carouselSize = _screenHeight / 2.1;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).canvasColor,
        title: Text("Missions"),
      ),
      body: BlocBuilder<MissionBloc, MissionState>(
        builder: (BuildContext context, MissionState state) {
          if (state is LoadingMissionsState) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          if (state is ErrorMissionsState) {
            return Center(
              child: Text("Unable to fetch missions please try again later.."),
            );
          }
          if (state is LoadedMissionsState) {
            return Container(
              child: Column(
                children: <Widget>[
//                  Padding(
//                    padding: const EdgeInsets.all(8.0),
//                    child: SizedBox(
//                      height: 50,
//                      child: DropdownButton(
//                        elevation: 5,
//                        icon: Icon(Icons.sort),
//                        items: state.dates
//                            .map((date) => DropdownMenuItem(
//                          child: Padding(
//                            padding: const EdgeInsets.all(8.0),
//                            child: Text(date.substrDate),
//                          ),
//                          value: date.substrDate,
//                        ))
//                            .toList(),
//                        onChanged: (value) {
//                          setState(() {
//                            monthString = value;
//                          });
//                          BlocProvider.of<MissionsBloc>(context)
//                              .add(LoadMissionsMission(monthString: value));
//                        },
//                        value: monthString,
//                      ),
//                    ),
//                  ),
                  Expanded(
                    child: ListView.builder(
                      shrinkWrap: false,
                      itemCount: state.missionData.length ?? 0,
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => HtmlContentScreenBasic(
                                      date: state.missionData[index].createdAt,
                                      title: 'Missions',
                                      createdAt:
                                          state.missionData[index].createdAt,
                                      name: state.missionData[index].title,
                                      image: Constants.BASE_THUMBNAIL +
                                          state.missionData[index].image,
                                      content:
                                          state.missionData[index].description,
                                    )));
                          },
                          child: MissionsCard(
                            missionData: state.missionData[index],
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            );
          }
          return Container(
            child: Center(
              child: Text(
                  "Servers are under maintainance. Please try again later.."),
            ),
          );
        },
      ),
    );
  }
}

class MissionsCard extends StatelessWidget {
  final MissionData missionData;
  final double radius;
  final double carouselSize;

  const MissionsCard(
      {Key key, this.missionData, this.radius, this.carouselSize})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 8,
        child: Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Material(
                elevation: 7,
                borderRadius: BorderRadius.circular(50),
                child: SizedBox(
                    height: 70,
                    width: 70,
                    child: Padding(
                        padding: const EdgeInsets.all(0.0),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(8),
                          child: Hero(
                            tag: this.missionData.createdAt,
                            child: CachedNetworkImage(
                              imageUrl:
                                  "${Constants.BASE_THUMBNAIL}${this.missionData.image}",
                              fit: BoxFit.fill,
                            ),
                          ),
                        ))),
              ),
            ),
            Flexible(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  SizedBox(
                    width: 8,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0, bottom: 8.0),
                    child: Text(
                      this.missionData.title,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
//                  Padding(
//                    padding: const EdgeInsets.only(left: 8.0, bottom: 8.0),
//                    child: Text(
//                      this.missionData.createdAt,
//                      style: TextStyle(
//                          color: ChurchAppColors.navyBlue, fontSize: 10),
//                    ),
//                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
