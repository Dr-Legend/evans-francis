part of 'mission_bloc.dart';

@immutable
abstract class MissionState {}

class InitialMissionState extends MissionState {}

class LoadingMissionsState extends MissionState {}

class LoadedMissionsState extends MissionState {
  final List<MissionData> missionData;
//  final List<ArchiveDates> dates;

  LoadedMissionsState({this.missionData});
}

class ErrorMissionsState extends MissionState {
  final String error;

  ErrorMissionsState(this.error);
}
