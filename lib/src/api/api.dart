import 'dart:async';
import 'dart:convert';
import 'package:dio/dio.dart';

///This class has API calling methods
///
class ApiMethods {
  ///This method handles request in GET
//  Future<String> requestInGet(String url) async {
//    final response = await http.get(url,
//        headers: {"Accept": "application/json"}).timeout(Duration(seconds: 10));
//    print(response.body);
//    if (response.statusCode == 200) {
//      /// If server returns an OK response, return the response
//      return response.body;
//    } else {
//      throw Exception('Failed to load data');
//    }
//  }

  Future<dynamic> requestInGet(String url) async {
    final response = await Dio()
        .get(
          url,
        )
        .timeout(Duration(seconds: 60));
    print(response.data);
    if (response.statusCode == 200) {
      /// If server returns an OK response, return the response
      return response.data;
    } else {
//      throw Exception('Failed to load data');
    }
  }

  Future<Response> requestInPost(String url, Map<String, dynamic> data) async {
    final response = await Dio().post(url, data: data);

    if (response?.statusCode == 200) {
      /// If server returns an OK response, return the response
      return response;
    }
  }

  requestInPut(String url, Map<String, dynamic> data) async {
    final response = await Dio().put(url, data: data);

    if (response?.statusCode == 200) {
      /// If server returns an OK response, return the response
      return response;
    }
  }
}
