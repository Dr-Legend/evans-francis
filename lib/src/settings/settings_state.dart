part of 'settings_bloc.dart';

@immutable
abstract class SettingsState {}

class InitialSettingsState extends SettingsState {}