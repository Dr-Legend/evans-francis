//import 'package:clipboard_manager/clipboard_manager.dart';
import 'package:easy_dialog/easy_dialog.dart';
import 'package:evans/src/utils/constants.dart';
import 'package:evans/src/utils/widgets/asset_image.dart';
import 'package:evans/src/utils/widgets/network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:groovin_material_icons/groovin_material_icons.dart';
import 'package:solid_bottom_sheet/solid_bottom_sheet.dart';
import 'package:url_launcher/url_launcher.dart';

class LoveOffering extends StatelessWidget {
  static final String path = "lib/src/pages/profile/profile5.dart";
  var gPayLogo = Image.asset('assets/images/gpay2.png');
  var sbiLogo = Image.asset('assets/images/sbi.png');
  var paytmLogo = Image.asset('assets/images/paytm.png');
  var paypalLogo = Image.asset('assets/images/paypal.png');

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(
        url,
        forceSafariVC: false,
      );
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    final Color color1 = ChurchAppColors.brandBackground;
    final Color color2 = ChurchAppColors.brandBackgroundLight;
//    final String image = avatars[0];
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            height: 360,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(50.0),
                    bottomRight: Radius.circular(50.0)),
                gradient: LinearGradient(
                    colors: [color1, color2],
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight)),
          ),
          Container(
            margin: const EdgeInsets.only(top: 80),
            child: Column(
              children: <Widget>[
                Flexible(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: Text(
                      "\"God will richly bless the people who take of his servants (LK. 6:38). Put first and he will add to your life daily what you need (Matt.6:35).\"",
                      style: TextStyle(
                          color: ChurchAppColors.golden,
                          fontSize: 18,
                          fontStyle: FontStyle.italic),
                    ),
                  ),
                ),
                SizedBox(height: 20.0),
                Flexible(
                  child: Stack(
                    children: <Widget>[
                      Container(
//                        height: double.infinity,
                        margin: const EdgeInsets.only(
                            left: 30.0, right: 30.0, top: 10.0),
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(30.0),
                            child: PNetworkAssetImage(
                              'assets/images/love.jpeg',
//                              fit: BoxFit.contain,
                              height: 300,
                              width: double.maxFinite,
                            )),
                      ),
                      Container(
                        alignment: Alignment.topCenter,
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 10.0, vertical: 5.0),
                          decoration: BoxDecoration(
                              color: Colors.yellow,
                              borderRadius: BorderRadius.circular(20.0)),
                          child: Text(
                            "Give a love offering now !",
                            style: TextStyle(
                                color: ChurchAppColors.brandBackground),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(height: 10.0),
//                Text(
//                  "Sasha - 22",
//                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
//                ),
//                Row(
//                  mainAxisAlignment: MainAxisAlignment.center,
//                  crossAxisAlignment: CrossAxisAlignment.center,
//                  children: <Widget>[
//                    Icon(
//                      Icons.location_on,
//                      size: 16.0,
//                      color: Colors.grey,
//                    ),
//                    Text(
//                      "San Diego, California, USA",
//                      style: TextStyle(color: Colors.grey.shade600),
//                    )
//                  ],
//                ),
                SizedBox(height: 5.0),
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          IconButton(
                            iconSize: 50,
                            color: Colors.grey,
                            icon: Image.asset('assets/images/paypal.png'),
                            onPressed: () {
                              EasyDialog(
                                  title: Text(
                                    'You can send offering via Paypal',
                                    style: TextStyle(
                                        color: ChurchAppColors.brandBackground,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  closeButton: true,
                                  contentList: [
                                    ListTile(
                                      title: Text(
                                        "paypal/EvansFrancis",
                                        style: TextStyle(
                                            fontSize: 12,
                                            color: Colors.blue,
                                            decoration:
                                                TextDecoration.underline),
                                      ),
                                      leading: FittedBox(
                                        child: Image.asset(
                                            'assets/images/paypal.png'),
                                      ),
                                      subtitle: Text(Constants.payPalId),
                                      onTap: () {
                                        Navigator.pop(context);
                                        _launchURL(Constants.payPal);
                                      },
                                    ),
                                  ]).show(context);
                            },
                          ),
                          Text(
                            'PayPal',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Color(0xFF253B80),
                            ),
                          )
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          IconButton(
                            iconSize: 50,
                            color: Colors.grey,
                            icon: Image.asset('assets/images/gpay2.png'),
                            onPressed: () {
                              EasyDialog(
                                  height: 300,
                                  closeButton: true,
                                  contentList: [
                                    IconButton(
                                        iconSize: 50,
                                        icon: gPayLogo,
                                        onPressed: null),
                                    Spacer(),
                                    Text(
                                      'You can send offering via GPay',
                                      style: TextStyle(
                                          color:
                                              ChurchAppColors.brandBackground,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    ListTile(
                                      title: Text(
                                        Constants.gPayUPI,
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 10),
                                      ),
                                      leading: Icon(GroovinMaterialIcons.web),
                                    ),
                                    Spacer(),
                                  ]).show(context);
                            },
                          ),
                          Text(
                            'GPay',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Color(0xFF6E7BF2)),
                          )
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          IconButton(
                            iconSize: 50,
                            color: Colors.grey.shade600,
                            icon: Image.asset('assets/images/sbi.png'),
                            onPressed: () {
                              EasyDialog(
                                  height: 500,
                                  closeButton: true,
                                  contentList: [
                                    IconButton(
                                        iconSize: 50,
                                        icon: sbiLogo,
                                        onPressed: null),
                                    Spacer(),
                                    Text(
                                      'You can send offering via SBI',
                                      style: TextStyle(
                                          color:
                                              ChurchAppColors.brandBackground,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    _BuildListTile(
                                        context,
                                        'Account Holder Name',
                                        Constants.SBIName,
                                        Icons.account_circle,
                                        Constants.SBIName),
                                    _BuildListTile(
                                        context,
                                        'Account Number',
                                        Constants.SBINumber,
                                        FontAwesomeIcons.sortNumericDown,
                                        Constants.SBINumber),
                                    _BuildListTile(
                                        context,
                                        'Bank',
                                        Constants.SBIBank,
                                        GroovinMaterialIcons.bank,
                                        Constants.SBIBank),
                                    _BuildListTile(
                                        context,
                                        'IFSC Code',
                                        Constants.SBIIFSC,
                                        Icons.code,
                                        Constants.SBIIFSC),
                                    _BuildListTile(
                                        context,
                                        'Branch',
                                        Constants.SBIBranch,
                                        GroovinMaterialIcons.bank,
                                        Constants.SBIBranch),
                                    Spacer(),
                                  ]).show(context);
                            },
                          ),
                          Text(
                            'SBI',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.lightBlueAccent),
                          )
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          IconButton(
                            iconSize: 50,
                            color: Colors.grey.shade600,
                            icon: Image.asset('assets/images/paytm.png'),
                            onPressed: () {
//                            _BuildListTile(context, Constants.payTm, '',
//                                Icons.phone, Constants.payTm);
                              EasyDialog(
                                  title: Text(
                                    'You can send offering via Paytm',
                                    style: TextStyle(
                                        color: ChurchAppColors.brandBackground,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  closeButton: true,
                                  contentList: [
                                    ListTile(
                                      title: Text(Constants.payTm),
                                      leading: FittedBox(
                                        child: Image.asset(
                                            'assets/images/paytm.png'),
                                      ),
                                    ),
                                  ]).show(context);
                            },
                          ),
                          Text(
                            'Paytm',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Color(0xFF0F3071)),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 10.0),
              ],
            ),
          ),
//          AppBar(
//            backgroundColor: Colors.transparent,
//            elevation: 0,
//            actions: <Widget>[
//              IconButton(
//                icon: Icon(Icons.notifications),
//                onPressed: () {},
//              ),
//              IconButton(
//                icon: Icon(Icons.menu),
//                onPressed: () {},
//              ),
//            ],
//          ),
        ],
      ),
    );
  }

  Widget _BuildListTile(BuildContext context, String title, String subtitle,
      IconData icon, String copy,
      {double titleSize}) {
    return ListTile(
      title: Text(
        title,
        style:
            TextStyle(fontWeight: FontWeight.bold, fontSize: titleSize ?? 10),
      ),
      subtitle: Text(
        subtitle,
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 10),
      ),
      leading: Icon(icon),
    );
  }
}
