import 'package:cached_network_image/cached_network_image.dart';
import 'package:evans/src/events/events_bloc.dart';
import 'package:evans/src/models/upcoming_events_data.dart';
import 'package:evans/src/utils/constants.dart';
import 'package:evans/src/utils/widgets/html_content_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class Events extends StatefulWidget {
  @override
  _EventsState createState() => _EventsState();
}

class _EventsState extends State<Events> {
  DateFormat dateFormat = DateFormat('MMM yyyy');

  String monthString;

  @override
  void initState() {
    super.initState();
//    monthString = dateFormat.format(DateTime.now());
  }

  @override
  Widget build(BuildContext context) {
    final double _radius = 25.0;
    final double _screenHeight = MediaQuery.of(context).size.height;
    final double _carouselSize = _screenHeight / 2.1;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).canvasColor,
        title: Text("Upcoming Events"),
      ),
      body: BlocBuilder<EventsBloc, EventsState>(
        builder: (BuildContext context, EventsState state) {
          if (state is LoadingEventsState) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          if (state is ErrorEventsState) {
            return Center(
              child: Text("Unable to fetch events please try again later.."),
            );
          }
          if (state is LoadedEventsState) {
            return Container(
              child: Column(
                children: <Widget>[
//                  Padding(
//                    padding: const EdgeInsets.all(8.0),
//                    child: SizedBox(
//                      height: 50,
//                      child: DropdownButton(
//                        elevation: 5,
//                        icon: Icon(Icons.sort),
//                        items: state.dates
//                            .map((date) => DropdownMenuItem(
//                          child: Padding(
//                            padding: const EdgeInsets.all(8.0),
//                            child: Text(date.substrDate),
//                          ),
//                          value: date.substrDate,
//                        ))
//                            .toList(),
//                        onChanged: (value) {
//                          setState(() {
//                            monthString = value;
//                          });
//                          BlocProvider.of<EventsBloc>(context)
//                              .add(LoadEventsEvent(monthString: value));
//                        },
//                        value: monthString,
//                      ),
//                    ),
//                  ),
                  Expanded(
                    child: ListView.builder(
                      shrinkWrap: false,
                      itemCount: state.eventData.length ?? 0,
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => HtmlContentScreenBasic(
                                      date: state.eventData[index].createdAt,
                                      title: 'Events',
                                      createdAt:
                                          state.eventData[index].createdAt,
                                      name: state.eventData[index].title,
                                      image: Constants.BASE_THUMBNAIL +
                                          state.eventData[index].image,
                                      content:
                                          state.eventData[index].description,
                                    )));
                          },
                          child: EventsCard(
                            eventData: state.eventData[index],
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            );
          }
          return Container(
            child: Center(
              child: Text(
                  "Servers are under maintainance. Please try again later.."),
            ),
          );
        },
      ),
    );
  }
}

class EventsCard extends StatelessWidget {
  final EventData eventData;
  final double radius;
  final double carouselSize;

  const EventsCard({Key key, this.eventData, this.radius, this.carouselSize})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 8,
        child: Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Material(
                elevation: 7,
                borderRadius: BorderRadius.circular(50),
                child: SizedBox(
                    height: 70,
                    width: 70,
                    child: Padding(
                        padding: const EdgeInsets.all(0.0),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(8),
                          child: Hero(
                            tag: this.eventData.createdAt,
                            child: CachedNetworkImage(
                              imageUrl:
                                  "${Constants.BASE_THUMBNAIL}${this.eventData.image}",
                              fit: BoxFit.fill,
                            ),
                          ),
                        ))),
              ),
            ),
            Flexible(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  SizedBox(
                    width: 8,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0, bottom: 8.0),
                    child: Text(
                      this.eventData.title,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
//                  Padding(
//                    padding: const EdgeInsets.only(left: 8.0, bottom: 8.0),
//                    child: Text(
//                      this.eventData.createdAt,
//                      style: TextStyle(
//                          color: ChurchAppColors.navyBlue, fontSize: 10),
//                    ),
//                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
