import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:evans/src/api/api.dart';
import 'package:evans/src/models/blog_archives_dates.dart';
import 'package:evans/src/models/upcoming_events_data.dart';
import 'package:evans/src/utils/constants.dart';
import 'package:meta/meta.dart';

part 'events_event.dart';

part 'events_state.dart';

class EventsBloc extends Bloc<EventsEvent, EventsState> {
  ApiMethods api = ApiMethods();
  @override
  EventsState get initialState => InitialEventsState();

  @override
  Stream<EventsState> mapEventToState(EventsEvent event) async* {
    if (event is LoadEventsEvent) {
      yield LoadingEventsState();
      try {
        var response = await api.requestInGet(Constants.UPCOMING_EVENT_API);
//        var dateRespose = await api.requestInGet(Constants.BLOGS_DATES_API);
        List<EventData> events = eventsEventDataFromJson(response);
//        List<ArchiveDates> dates = archiveDatesFromJson(dateRespose[0]);
        yield LoadedEventsState(eventData: events);
      } catch (e) {
        yield ErrorEventsState(e);
      }
    }
  }
}
