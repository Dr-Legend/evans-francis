part of 'events_bloc.dart';

@immutable
abstract class EventsState {}

class InitialEventsState extends EventsState {}

class LoadingEventsState extends EventsState {}

class LoadedEventsState extends EventsState {
  final List<EventData> eventData;
//  final List<ArchiveDates> dates;

  LoadedEventsState({this.eventData});
}

class ErrorEventsState extends EventsState {
  final String error;

  ErrorEventsState(this.error);
}
