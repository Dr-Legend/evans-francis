import 'package:bloc/bloc.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:evans/src/blog/blog_bloc.dart';
import 'package:evans/src/cloud/cloud_bloc.dart';
import 'package:evans/src/events/events_bloc.dart';
import 'package:evans/src/fire/fire_bloc.dart';
import 'package:evans/src/fire/fire_screen.dart';
import 'package:evans/src/gallery/gallery_bloc.dart';
import 'package:evans/src/home/home_bloc.dart';
import 'package:evans/src/invite/invite_bloc.dart';
import 'package:evans/src/messages/messages_bloc.dart';
import 'package:evans/src/mission/mission_bloc.dart';
import 'package:evans/src/music/music_bloc.dart';
import 'package:evans/src/pdf_screen/pdf_screen_bloc.dart';
import 'package:evans/src/prayer/prayer_bloc.dart';
import 'package:evans/src/reach_us/reach_us_bloc.dart';
import 'package:evans/src/settings/settings_bloc.dart';
import 'package:evans/src/testimony/testimony_bloc.dart';
import 'package:evans/src/utils/constants.dart';
import 'package:evans/src/utils/router.dart';
import 'package:evans/src/utils/services/locator.dart';
import 'package:evans/src/utils/services/navigation_service.dart';
import 'package:evans/src/video/video_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
//import 'package:flutter_plugin_playlist/flutter_plugin_playlist.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';

//RmxAudioPlayer rmxAudioPlayer = new RmxAudioPlayer();
void main() async {
  setupLocator();
  WidgetsFlutterBinding.ensureInitialized();
  BlocSupervisor.delegate = SimpleBlocDelegate(
    await HydratedBlocStorage.getInstance(),
  );
  runApp(MultiBlocProvider(
    providers: [
      BlocProvider<SettingsBloc>(
        create: (BuildContext context) => SettingsBloc(),
      ),
      BlocProvider<HomeBloc>(
        create: (BuildContext context) => HomeBloc(),
      ),
      BlocProvider<BlogBloc>(
        create: (BuildContext context) => BlogBloc(),
      ),
      BlocProvider<TestimonyBloc>(
        create: (BuildContext context) => TestimonyBloc(),
      ),
      BlocProvider<PdfScreenBloc>(
        create: (BuildContext context) => PdfScreenBloc(),
      ),
      BlocProvider<FireBloc>(
        create: (BuildContext context) => FireBloc(),
      ),
      BlocProvider<CloudBloc>(
        create: (BuildContext context) => CloudBloc(),
      ),
      BlocProvider<InviteBloc>(
        create: (BuildContext context) => InviteBloc(),
      ),
      BlocProvider<PrayerBloc>(
        create: (BuildContext context) => PrayerBloc(),
      ),
      BlocProvider<GalleryBloc>(
        create: (BuildContext context) => GalleryBloc(),
      ),
      BlocProvider<MusicBloc>(
        create: (BuildContext context) => MusicBloc(),
      ),
      BlocProvider<VideoBloc>(
        create: (BuildContext context) => VideoBloc(),
      ),
      BlocProvider<EventsBloc>(
        create: (BuildContext context) => EventsBloc(),
      ),
      BlocProvider<MissionBloc>(
        create: (BuildContext context) => MissionBloc(),
      ),
      BlocProvider<MessagesBloc>(
        create: (BuildContext context) => MessagesBloc(),
      ),
      BlocProvider<ReachUsBloc>(
        create: (BuildContext context) => ReachUsBloc(),
      ),
    ],
    child: ChurchApp(),
  ));
}

class ChurchApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return DynamicTheme(
      defaultBrightness: Brightness.light,
      data: (brightness) => ThemeData(
        indicatorColor: ChurchAppColors.golden,
        accentColor: ChurchAppColors.golden,
        brightness: brightness,
        primaryColorDark: Colors.red,
        primaryColorBrightness: brightness,
        backgroundColor: ChurchAppColors.scaffoldBackground,
        inputDecorationTheme: InputDecorationTheme(
            helperStyle: TextStyle(color: Colors.white30),
            labelStyle: TextStyle(color: ChurchAppColors.golden),
            border: OutlineInputBorder(
              gapPadding: 12,
              borderRadius: BorderRadius.circular(20),
              borderSide:
                  BorderSide(color: ChurchAppColors.brandBackgroundLight),
            ),
            focusedBorder: OutlineInputBorder(
                gapPadding: 12,
                borderRadius: BorderRadius.circular(20),
                borderSide: BorderSide(color: ChurchAppColors.golden))),
        iconTheme: IconThemeData(color: ChurchAppColors.golden),
        appBarTheme: AppBarTheme(
          brightness: brightness,
          iconTheme: IconThemeData(color: ChurchAppColors.golden),
          textTheme: TextTheme(title: TextStyle(color: ChurchAppColors.golden)),
          color: brightness == Brightness.dark
              ? ChurchAppColors.darkAccent
              : Colors.white,
        ),
      ),
      themedWidgetBuilder: (context, theme) {
        return MaterialApp(
          title: 'Evans Francis',
          debugShowCheckedModeBanner: false,
          theme: theme,
          onGenerateRoute: Router.generateRoute,
          initialRoute: Routes.root,
          navigatorKey: locator<NavigationService>().navigatorKey,
        );
      },
    );
  }
}

class SimpleBlocDelegate extends HydratedBlocDelegate {
  SimpleBlocDelegate(HydratedStorage storage) : super(storage);

  @override
  void onEvent(Bloc bloc, Object event) {
    super.onEvent(bloc, event);
    print(event);
  }

  @override
  onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    print(transition);
  }

  @override
  void onError(Bloc bloc, Object error, StackTrace stacktrace) {
    super.onError(bloc, error, stacktrace);
    print(error);
  }
}
